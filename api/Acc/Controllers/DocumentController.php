<?php
namespace Api\Acc\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Requests\DocumentRequest as REQUEST;
use Api\Acc\Services\DocumentService as SERVICE;

class DocumentController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
