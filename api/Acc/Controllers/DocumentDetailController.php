<?php
namespace Api\Acc\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Requests\DocumentDetailRequest as REQUEST;
use Api\Acc\Services\DocumentDetailService as SERVICE;

class DocumentDetailController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
