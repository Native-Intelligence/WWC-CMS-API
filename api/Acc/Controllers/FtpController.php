<?php
namespace Api\Acc\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Requests\FtpRequest as REQUEST;
use Api\Acc\Services\FtpService as SERVICE;

class FtpController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
