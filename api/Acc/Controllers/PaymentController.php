<?php
namespace Api\Acc\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Requests\PaymentRequest as REQUEST;
use Api\Acc\Services\PaymentService as SERVICE;

class PaymentController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
