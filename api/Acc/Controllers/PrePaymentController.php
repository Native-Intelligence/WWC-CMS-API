<?php
namespace Api\Acc\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Requests\PrePaymentRequest as REQUEST;
use Api\Acc\Services\PrePaymentService as SERVICE;

class PrePaymentController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
