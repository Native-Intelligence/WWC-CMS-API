<?php
namespace Api\Acc\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Requests\TransactionRequest as REQUEST;
use Api\Acc\Services\TransactionService as SERVICE;

class TransactionController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
