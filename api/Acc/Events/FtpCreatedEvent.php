<?php

namespace Api\Acc\Events;

use Infrastructure\Events\Event;
use Api\Acc\Models\Ftp;

class FtpCreatedEvent extends Event
{

    public $ftp;

    public function __construct(Ftp $ftp)
    {
        // dd("Ss");
        $this->ftp = $ftp;
    }
}
