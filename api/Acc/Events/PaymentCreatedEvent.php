<?php

namespace Api\Acc\Events;

use Infrastructure\Events\Event;
use Api\Acc\Models\Payment;

class PaymentCreatedEvent extends Event
{

    public $payment;

    public function __construct(Payment $payment)
    {
        // dd("Ssaa");
        $this->payment = $payment;
    }
}
