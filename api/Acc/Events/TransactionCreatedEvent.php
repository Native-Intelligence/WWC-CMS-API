<?php

namespace Api\Acc\Events;

use Infrastructure\Events\Event;
use Api\Acc\Models\Transaction;

class TransactionCreatedEvent extends Event
{

    public $transaction;

    public function __construct(Transaction $transaction)
    {
        // dd();
        $this->transaction = $transaction;
    }
}
