<?php

namespace Api\Acc\Listeners;

use Api\Sales\Services\CalculationService;
use Api\Sales\Services\SurveyService;
use Api\Sales\Services\FileService;
use Api\Acc\Services\FtpService;
use Api\Acc\Events\FtpCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FtpCreatedListener implements ShouldQueue
{

    // use InteractsWithQueue;
    private $_surveyService;

    private $_calculationService;

    private $_fileService;
    
    private $_ftpService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(CalculationService $calculationService, SurveyService $surveyService, FtpService $ftpService, FileService $fileService)
    {
        // dd("Ss");
        $this->_calculationService = $calculationService;
        $this->_surveyService = $surveyService;
        $this->_fileService = $fileService;
        $this->_ftpService = $ftpService;
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function handle(FtpCreatedEvent $ftp)
    {
        $calculation = $this->_calculationService->getByField(['file_number' => $ftp->ftp->file_number]);

        if (empty($calculation)) {
            $this->_ftpService->update(['status' => 1, 'id' => $ftp->ftp->id]);
        } else if (floor(($calculation->billing_details->total_price - $calculation->billing_details->total_price_with_discount) / 1000) == ($ftp->ftp->amount) / 1000) {
            $survey = $this->_surveyService->update(['status' => 2 , 'id' => $calculation->survey_id]);
            $this->_ftpService->update(['status' => 2, 'id' => $ftp->ftp->id]);
            $this->_calculationService->update(['status' => 2, 'id' => $calculation->id]);
            // $survey['survey_id'] = $survey['id'];
            // unset($survey['id']);
            $file = $this->_fileService->getByField(['file_number' => $ftp->ftp->file_number]);
            $fileData = collect($survey)->toArray();
            
            
            $fileData['last_survey_id'] = $fileData['id'];
            $fileData['last_calculation_id'] = $calculation->id;
            // dd($survey->request->person->full_name);
            if (is_null($file)) {
                $fileData['first_request_id'] = $survey->request->id;
                $fileData['branch_type'] = $survey->request->branch_type;
                $fileData['land_id'] = $survey->request->land->id;
                $fileData['full_name'] = $survey->request->person->full_name;
                unset($fileData['id']);
                $this->_fileService->create($fileData);
                // dd($file);
            } else {
                // $survey = (array) $survey;
                $fileData['id'] = $file->id;
                $this->_fileService->update($fileData);
            }
        } else {
            // dd($calculation->total_price, $ftp->ftp->amount);
            $survey = $this->_surveyService->update(['status' => 1 , 'id' => $calculation->survey_id]);
            $this->_ftpService->update(['status' => 1, 'id' => $ftp->ftp->id]);
            $this->_calculationService->update(['status' => 1, 'id' => $calculation->id]);
            
            $file = $this->_fileService->getByField(['file_number' => $ftp->ftp->file_number]);
            $fileData = collect($survey)->toArray();
            $fileData['last_survey_id'] = $fileData['id'];
            $fileData['last_calculation_id'] = $calculation->id;
            if (is_null($file)) {
                $fileData['first_request_id'] = $survey->request->id;
                $fileData['branch_type'] = $survey->request->branch_type;
                $fileData['land_id'] = $survey->request->land->id;
                $fileData['full_name'] = $survey->request->person->full_name;
                
                unset($fileData['id']);
                $this->_fileService->create((array) $fileData);
            } else {
                // $survey = (array) $survey;
                $fileData['id'] = $file->id;
                $this->_fileService->update($fileData);
            }
        }
    }
}
