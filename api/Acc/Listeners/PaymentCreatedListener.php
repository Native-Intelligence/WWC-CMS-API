<?php

namespace Api\Acc\Listeners;

use Api\Acc\Services\FtpService;
use Api\Acc\Events\PaymentCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentCreatedListener implements ShouldQueue
{

    // use InteractsWithQueue;
    private $_ftpService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(FtpService $ftpService)
    {
        // dd("Ss");
        $this->_ftpService = $ftpService;
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function handle(PaymentCreatedEvent $payment)
    {
        // dd($payment->payment->payment_details);
        $data = (array)$payment->payment->payment_details;
        $data['id'] = $payment->payment->id;
        // dd($data);
        $this->_ftpService->create($data);
    }
}
