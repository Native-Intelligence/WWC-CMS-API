<?php

namespace Api\Acc\Listeners;

// use Api\Sales\Services\CalculationService;
// use Api\Sales\Services\SurveyService;
// use Api\Acc\Services\TransactionService;
use Api\Acc\Services\DocumentService;
use Api\Acc\Services\DocumentDetailService;
use Api\Acc\Settings\Services\ConfigService;
use Api\Acc\Events\TransactionCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransactionCreatedListener implements ShouldQueue
{
    // use InteractsWithQueue;
    
    private $_documentService;
    private $_documentDetailService;
    private $_configService;

    public function __construct(
        DocumentService $documentService,
        DocumentDetailService $documentDetailService,
        ConfigService $configService
    ) {
        $this->_documentService = $documentService;
        $this->_documentDetailService = $documentDetailService;
        $this->_configService = $configService;
    }

    public function handle(TransactionCreatedEvent $transaction)
    {
        $data = json_decode($transaction->transaction->details);
        $documentData = [];
        
        if($transaction->transaction->type == '537' or $transaction->transaction->type == '551') {
            $config = $this->_configService->getByField(
                ['g_document_type_id' => $transaction->transaction->type]
            );
            $documentData['scope'] = $config->scope;
            $documentCount = [$data->company_code => $data->amount];
        }elseif($transaction->transaction->type == '571' or $transaction->transaction->type == '194') {
            $config = $this->_configService->getByField(
                ['g_service_type_id' => $transaction->transaction->type]
            );

            $documentData['scope'] = $data->survey->scope;
            $documentCount = (array)$data->invoice_item_price;
        }
        
        
        // dd($data, $config);
        $documentData += [
            //TODO:  add file_number with check digit
        'title' => $config->title.' - '.$data->file_number,
        'g_document_type_id' => $config->g_document_type_id,
        'document_date' => date('Y-m-d H:i:s'),
        'document_number' => time()
        ];

        // dd($config->details);
        $document = $this->_documentService->create($documentData);
        // dd($documentCount);

        foreach ($documentCount as $slug => $amount) {
            $documentDetailConfigs = (array)$config->details->$slug;
            $documentDetailConfigs = array_filter(
                $documentDetailConfigs,
                function ($k) {
                    return $k != 'invoice_item' and $k != 'bank_account';
                },
                ARRAY_FILTER_USE_KEY
            );

            foreach ((array) $documentDetailConfigs as $documentDetailConfig) {
                $documentDetailData = [
                'title' => $documentDetailConfig->title.' - '.$data->file_number,
                'document_id' => $document->id,
                'acc_id' => $documentDetailConfig->account->id,
                'level_4' => (isset($documentDetailConfig->level_4) ? $documentDetailConfig->level_4->id : null),
                'level_5' => (isset($documentDetailConfig->level_5) ? $documentDetailConfig->level_5->id : null),
                'level_6' => (isset($documentDetailConfig->level_6) ? $documentDetailConfig->level_6->id : null),
                'type' => $documentDetailConfig->type,
                'amount' => intval($amount),
                'scope' => $documentData['scope']
                ];
                
                if(isset($data->invoice_item_price_with_discount->{$slug})
                    and isset($documentDetailConfig->discount)
                ) {
                    $documentDetailData['amount'] = intval($amount) - intval($data->invoice_item_price_with_discount->{$slug});
                    $documentDetail = $this->_documentDetailService->create($documentDetailData);
                            
                    $documentDetailData = [
                    'title' => $documentDetailConfig->discount->title.' - '.$data->file_number,
                    'document_id' => $document->id,
                    'acc_id' => $documentDetailConfig->discount->account->id,
                    'level_4' => (isset($documentDetailConfig->discount->level_4) ? $documentDetailConfig->discount->level_4->id : null),
                    'level_5' => (isset($documentDetailConfig->discount->level_5) ? $documentDetailConfig->discount->level_5->id : null),
                    'level_6' => (isset($documentDetailConfig->discount->level_6) ? $documentDetailConfig->discount->level_6->id : null),
                    'type' => $documentDetailConfig->type,
                    'amount' => intval($data->invoice_item_price_with_discount->{$slug}),
                    'scope' => $data->survey->scope
                    ];
                    
                    $documentDetail = $this->_documentDetailService->create($documentDetailData);
                }
                else{
                    $documentDetail = $this->_documentDetailService->create($documentDetailData);
                }
            }
        }
        
        
        // switch ($transaction->transaction->type) {
        // case '537':
        // case '551':
        //     $documentDetailConfigs = (array)$config->details->{$data->company_code};
        //         $documentDetailConfigs = array_filter(
        //             $documentDetailConfigs,
        //             function ($k) {
        //                 return $k != 'invoice_item' and $k != 'bank_account';
        //             },
        //             ARRAY_FILTER_USE_KEY
        //         );
        //
        //     foreach ((array) $documentDetailConfigs as $documentDetailConfig) {
        //         $documentDetailData = [
        //         'title' => $documentDetailConfig->title,
        //         'document_id' => $document->id,
        //         'acc_id' => $documentDetailConfig->account->id,
        //         'level_4' => (isset($documentDetailConfig->level_4) ? $documentDetailConfig->level_4->id : null),
        //         'level_5' => (isset($documentDetailConfig->level_5) ? $documentDetailConfig->level_5->id : null),
        //         'level_6' => (isset($documentDetailConfig->level_6) ? $documentDetailConfig->level_6->id : null),
        //         'type' => $documentDetailConfig->type,
        //         'amount' => intval($data->amount),
        //         'scope' => []
        //         ];
        //
        //         $documentDetail = $this->_documentDetailService->create($documentDetailData);
        //     }
        //     break;
        // case '500':
        // case '501':
        //     foreach ((array)$data->invoice_item_price as $slug => $amount) {
        //         $documentDetailConfigs = (array)$config->details->$slug;
        //         $documentDetailConfigs = array_filter(
        //             $documentDetailConfigs,
        //             function ($k) {
        //                 return $k != 'invoice_item' and $k != 'bank_account';
        //             },
        //             ARRAY_FILTER_USE_KEY
        //         );
        //
        //         foreach ((array) $documentDetailConfigs as $documentDetailConfig) {
        //             $documentDetailData = [
        //             'title' => $documentDetailConfig->title.' - '.$data->file_number,
        //             'document_id' => $document->id,
        //             'acc_id' => $documentDetailConfig->account->id,
        //             'level_4' => (isset($documentDetailConfig->level_4) ? $documentDetailConfig->level_4->id : null),
        //             'level_5' => (isset($documentDetailConfig->level_5) ? $documentDetailConfig->level_5->id : null),
        //             'level_6' => (isset($documentDetailConfig->level_6) ? $documentDetailConfig->level_6->id : null),
        //             'type' => $documentDetailConfig->type,
        //             'amount' => intval($amount),
        //             'scope' => $data->survey->scope
        //             ];
        //
        //             if(isset($data->invoice_item_price_with_discount->{$slug})
        //                 and isset($documentDetailConfig->discount)
        //             ) {
        //                 $documentDetailData['amount'] = intval($amount) - intval($data->invoice_item_price_with_discount->{$slug});
        //                 $documentDetail = $this->_documentDetailService->create($documentDetailData);
        //
        //                 $documentDetailData = [
        //                 'title' => $documentDetailConfig->discount->title.' - '.$data->file_number,
        //                 'document_id' => $document->id,
        //                 'acc_id' => $documentDetailConfig->discount->account->id,
        //                 'level_4' => (isset($documentDetailConfig->discount->level_4) ? $documentDetailConfig->discount->level_4->id : null),
        //                 'level_5' => (isset($documentDetailConfig->discount->level_5) ? $documentDetailConfig->discount->level_5->id : null),
        //                 'level_6' => (isset($documentDetailConfig->discount->level_6) ? $documentDetailConfig->discount->level_6->id : null),
        //                 'type' => $documentDetailConfig->type,
        //                 'amount' => intval($data->invoice_item_price_with_discount->{$slug}),
        //                 'scope' => $data->survey->scope
        //                 ];
        //
        //                 $documentDetail = $this->_documentDetailService->create($documentDetailData);
        //             }
        //             else{
        //                 $documentDetail = $this->_documentDetailService->create($documentDetailData);
        //             }
        //         }
        //     }
        //     break;
        // }
    }
}
