<?php
namespace Api\Acc\Models;

use Infrastructure\Database\Eloquent\Model;

class Document extends Model
{

    protected $table = 'acc_docs';

    protected $fillable = [
                            'title',
                            'document_date',
                            'document_number',
                            'g_document_type_id',
                            'is_manual',
                            'status',
                            'scope'
                           ];
}
