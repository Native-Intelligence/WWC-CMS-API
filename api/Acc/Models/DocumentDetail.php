<?php
namespace Api\Acc\Models;

use Infrastructure\Database\Eloquent\Model;

class DocumentDetail extends Model
{

    protected $table = 'acc_doc_dets';
    
    protected $view = 'acc_doc_dets_view';

    protected $fillable = [
                            'title',
                            'document_id',
                            'acc_id',
                            'type',
                            'level_4',
                            'level_5',
                            'level_6',
                            'amount',
                            'status',
                            'scope'
                           ];
}
