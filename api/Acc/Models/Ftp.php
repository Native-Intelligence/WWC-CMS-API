<?php
namespace Api\Acc\Models;

use Infrastructure\Database\Eloquent\Model;

class Ftp extends Model
{

    protected $table = 'accounting_ftp';

    protected $fillable = [
                            'bill_number',
                            'pay_number',
                            'company_code',
                            'amount',
                            'payment_id',
                            'file_number',
                            'g_bank_id',
                            'paid_at',
                            'status'
                           ];
    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'id');
    }

    public function request()
    {
        return $this->belongsTo(SaleReq::class, 'id');
    }

    public function invoiceItem()
    {
        return $this->belongsTo(InvoiceItem::class, 'id');
    }
}
