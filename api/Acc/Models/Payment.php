<?php
namespace Api\Acc\Models;

use Infrastructure\Database\Eloquent\Model;

class Payment extends Model
{

    protected $table = 'accounting_payments';

    protected $fillable = [
                            'g_payment_type_id',
                            'payment_details',
                            'user_id',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'payment_details' => 'object'
    ];
}
