<?php
namespace Api\Acc\Models;

use Infrastructure\Database\Eloquent\Model;

class PrePayment extends Model
{

    protected $table = 'accounting_pre_payments';

    protected $fillable = [
                            'amount',
                            'pre_payment_date',
                            'pre_payment_number',
                            'document_id',
                            'payment_id',
                            'recipt_id',
                            'payment_type',
                            'pre_payment_detail',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'pre_payment_detail' => 'object'
    ];
}
