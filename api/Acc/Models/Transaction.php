<?php
namespace Api\Acc\Models;

use Infrastructure\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = 'acc_transactions';

    protected $fillable = [
                            'details',
                            'type',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'details'  => 'object'
    ];
}
