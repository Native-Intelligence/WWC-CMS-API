<?php
namespace Api\Acc\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Models\DocumentDetail;

class DocumentDetailRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Models\\DocumentDetail';
    }
}
