<?php
namespace Api\Acc\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Models\Document;

class DocumentRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Models\\Document';
    }
}
