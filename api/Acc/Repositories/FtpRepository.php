<?php
namespace Api\Acc\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Models\Ftp;

class FtpRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Models\\Ftp';
    }
}
