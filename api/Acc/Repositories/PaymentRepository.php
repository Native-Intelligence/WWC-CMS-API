<?php
namespace Api\Acc\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Models\Payment;

class PaymentRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Models\\Payment';
    }
}
