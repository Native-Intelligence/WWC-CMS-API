<?php
namespace Api\Acc\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Models\PrePayment;

class PrePaymentRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Models\\PrePayment';
    }
}
