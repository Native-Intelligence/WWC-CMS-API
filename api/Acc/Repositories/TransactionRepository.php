<?php
namespace Api\Acc\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Models\Transaction;

class TransactionRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Models\\Transaction';
    }
}
