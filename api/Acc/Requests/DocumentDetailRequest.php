<?php
namespace Api\Acc\Requests;

use Infrastructure\Requests\Request;

class DocumentDetailRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('document_detail');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            // 'type' => 'required|integer',
            'document_id' => 'required|integer',
            'type' => 'required|integer',
            // 'acc_grp_id' => 'integer',
            'acc_id' => 'required|integer',
            // 'acc_det_id' => 'string',
            'level_4' => 'nullable|integer',
            'level_5' => 'nullable|integer',
            'level_6' => 'nullable|integer',
            // 'det_acc_ids' => 'nullable|array',
            'amount' => 'required|integer',
            // 'details' => 'array',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            // 'type' => 'integer',
            'title' => 'sometimes|required|string',
            'type' => 'sometimes|required|integer',
            'level_4' => 'nullable|integer',
            'level_5' => 'nullable|integer',
            'level_6' => 'nullable|integer',
            // 'acc_grp_id' => 'string',
            'acc_id' => 'sometimes|required|integer',
            // 'acc_det_id' => 'string',
            // 'det_acc_ids' => 'nullable|array',
            'amount' => 'sometimes|required|integer',
            // 'details' => 'array',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
