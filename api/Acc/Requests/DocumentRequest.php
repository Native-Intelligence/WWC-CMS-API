<?php
namespace Api\Acc\Requests;

use Infrastructure\Requests\Request;

class DocumentRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('document');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'document_date' => 'required|date',
            'document_number' => 'required|integer',
            'g_document_type_id' => 'required|integer',
            'is_manual' => 'boolean',
            // 'details' => 'array',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'document_date' => 'sometimes|required|date',
            'document_number' => 'sometimes|required|integer',
            'is_manual' => 'boolean',
            'g_document_type_id' => 'sometimes|required|integer',
            // 'details' => 'array',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
