<?php
namespace Api\Acc\Requests;

use Infrastructure\Requests\Request;

class FtpRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('ftp');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'bill_number' => 'integer',
            'pay_number' => 'date',
            'amount' => 'integer',
            'payment_id' => 'integer',
            'file_number' => 'string',
            'paid_at' => 'integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'bill_number' => 'integer',
            'pay_number' => 'date',
            'amount' => 'integer',
            'payment_id' => 'integer',
            'file_number' => 'string',
            'paid_at' => 'integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
