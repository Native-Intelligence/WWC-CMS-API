<?php
namespace Api\Acc\Requests;

use Infrastructure\Requests\Request;

class PaymentRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('payment');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'g_payment_type_id' => 'integer',
            'user_id' => 'intere',
            'payment_details' => 'array',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'g_payment_type_id' => 'integer',
            'user_id' => 'intere',
            'payment_details' => 'array',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
