<?php
namespace Api\Acc\Requests;

use Infrastructure\Requests\Request;

class PrePaymentRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('prePayment');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'amount' => 'required|integer',
            'pre_payment_date' => 'required|date',
            'pre_payment_number' => 'required|integer',
            'document_id' => 'required|integer',
            'payment_id' => 'required|integer',
            'recipt_id' => 'required|integer',
            'payment_type' => 'required|integer',
            'pre_payment_detail' => 'required|array',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'amount' => 'sometimes|required|integer',
            'pre_payment_date' => 'sometimes|required|date',
            'pre_payment_number' => 'sometimes|required|integer',
            'document_id' => 'sometimes|required|integer',
            'payment_id' => 'sometimes|required|integer',
            'recipt_id' => 'sometimes|required|integer',
            'payment_type' => 'sometimes|required|integer',
            'pre_payment_detail' => 'sometimes|required|array',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
