<?php
namespace Api\Acc\Requests;

use Infrastructure\Requests\Request;

class TransactionRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('transaction');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'string',
            'parent_id' => 'integer',
            'accounting_import' => 'nullable|array',
            'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'string',
            'parent_id' => 'integer',
            'accounting_import' => 'nullable|array',
            'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
}
