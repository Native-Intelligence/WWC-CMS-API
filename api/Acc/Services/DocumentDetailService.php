<?php
namespace Api\Acc\Services;

use Infrastructure\Services\Service;
use Api\Acc\Repositories\DocumentDetailRepository;

class DocumentDetailService extends Service
{

    public function __construct(DocumentDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'document_detail';
        $this->uniqueKey = '';
    }
}
