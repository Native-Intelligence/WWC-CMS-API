<?php
namespace Api\Acc\Services;

use Infrastructure\Services\Service;
use Api\Acc\Repositories\DocumentRepository;

class DocumentService extends Service
{

    public function __construct(DocumentRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'document';
        $this->uniqueKey = 'document_number';
    }
}
