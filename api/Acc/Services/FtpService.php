<?php
namespace Api\Acc\Services;

use Api\Sales\Services\CalculationService;
use Api\Sales\Services\SurveyService;
use Api\Sales\Services\FileService;
use Api\Sales\Services\InstallmentService;
// use Api\Acc\Services\FtpService;
// use Api\Acc\Events\FtpCreatedEvent;

use Infrastructure\Services\Service;
use Api\Acc\Repositories\FtpRepository;
use Api\Acc\Services\TransactionService;

class FtpService extends Service
{
    private $_transactionService;
    private $_surveyService;
    private $_calculationService;
    private $_fileService;
    private $_installmentService;

    public function __construct(
        FtpRepository $repository,
        TransactionService $TransactionService,
        CalculationService $calculationService,
        SurveyService $surveyService,
        FileService $fileService,
        InstallmentService $installmentService
    ) {
        $this->_calculationService = $calculationService;
        $this->_surveyService = $surveyService;
        $this->_fileService = $fileService;
        $this->_installmentService = $installmentService;
        $this->_transactionService = $TransactionService;
        $this->repository = $repository;
        $this->key = 'ftp';
        $this->uniqueKey = 'ftp';
    }

    public function ftpCreatedEvent($ftp, $installment)
    {
        
        // dd($installment);
        if (empty($installment)) {
            $this->update(['status' => 1, 'id' => $ftp->id]);
            return 'faild';
        } else{
            if ($installment->amount == $ftp->amount) {
                $resultStatus = 2;
                $result = 'correct';
            } else {
                $resultStatus = 1;
                $result = 'faild';
            }
            
            $this->_installmentService->update(['status' => 1, 'id' => $installment->id]);
            $survey = $this->_surveyService->update(['status' => $resultStatus , 'id' => $installment->survey_id]);
            $this->update(['status' => $resultStatus, 'id' => $ftp->id]);
            $this->_calculationService->update(['status' => $resultStatus, 'id' => $installment->calculation_id]);
            // dd($survey->request->id);
            
            $file = $this->_fileService->getByField(['file_number' => $ftp->file_number]);
            $fileData = collect($survey)->toArray();
            $fileData['last_survey_id'] = $fileData['id'];
            $fileData['last_calculation_id'] = $installment->calculation_id;
            if (is_null($file)) {
                $fileData['first_request_id'] = $survey->request->id;
                $fileData['branch_type'] = $survey->request->branch_type;
                $fileData['land_id'] = $survey->land_id;
                $fileData['full_name'] = $survey->request->person->full_name;
                unset($fileData['id']);
                // dd($fileData);
                $this->_fileService->create((array) $fileData);
            } else {
                unset($fileData['branch_type']);
                $fileData['id'] = $file->id;
                $this->_fileService->update($fileData);
            }
            
            return $result;
        }
    }
    
    public function create($data)
    {
        $data = (object)$data;
        $resultArray['correct']['amount'] =0;
        $resultArray['faild']['amount'] =0;
        foreach ((array)$data->contents as $key => $value) {
            // $this->data = $this->getData($data);
            //TODO: $this->exist = $this->repository->findByField($this->uniqueKey, $this->data->{$this->uniqueKey})->first();

            // if (!is_null($this->exist)) {
            //     throw new Exception(40640);
            // }
            $installment = $this->_installmentService->getByField(
                [
                'bill_number' => $value->bill_number,
                'pay_number' => $value->pay_number,
                'status' => 0
                ]
            );
            
            if(!empty($installment)) {
                $value->file_number = $installment->file_number;
            }
            $value->payment_id = $data->id;
            $value->g_bank_id = $data->g_bank_id;
            $value->amount = $data->amount;
            $result = $this->repository->create((array) $value);
            // dd($data->content);

            $this->archiveEvent('create', $result);

            // if (isset($this->event['created'])) {
            //     // dd("SSS");
            //     event(new $this->event['created']($result));
            // }
            $res = $this->ftpCreatedEvent($result, $installment);
            switch ($res) {
            case 'correct':
                $resultArray['correct']['amount'] += $result->amount;
                break;
            case 'faild':
                $resultArray['faild']['amount'] += $result->amount;
                break;
            }
            // $resultArray['amount'] += $result->amount;
            // $resultArray['amount'] = $result->amount;
        }

        foreach ($resultArray as $key => $value) {
            $value['payment_id'] = $result->payment_id;
            $value['file_number'] = $result->file_number;
            $value['company_code'] = $result->company_code;
            switch ($key) {
            case 'correct':
                if($resultArray['correct']['amount'] > 0 ) {
                    $this->_transactionService->create(['details' => json_encode($value), 'type' => 537, 'scope' => 1]);
                }
                break;
            case 'faild':
                if($resultArray['faild']['amount'] > 0 ) {
                    $this->_transactionService->create(['details' => json_encode($value), 'type' => 551, 'scope' => 1]);
                }
                break;
            }
        }

        return $result;
    }
}
