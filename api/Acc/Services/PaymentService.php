<?php
namespace Api\Acc\Services;

use Infrastructure\Services\Service;
use Api\Acc\Repositories\PaymentRepository;

class PaymentService extends Service
{

    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'payment';
        $this->uniqueKey = '';
        $this->event = [
            'created' => ['Api\Acc\Events\PaymentCreatedEvent']
        ];
    }
}
