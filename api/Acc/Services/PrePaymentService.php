<?php
namespace Api\Acc\Services;

use Infrastructure\Services\Service;
use Api\Acc\Repositories\PrePaymentRepository;

class PrePaymentService extends Service
{

    public function __construct(PrePaymentRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'prePayment';
        $this->uniqueKey = 'pre_payment_number';
    }
}
