<?php
namespace Api\Acc\Services;

use Infrastructure\Services\Service;
use Api\Acc\Repositories\TransactionRepository;

class TransactionService extends Service
{

    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'transaction';
        $this->uniqueKey = '';
        $this->event = [
            'created' => ['Api\Acc\Events\TransactionCreatedEvent']
        ];
    }
}
