<?php
namespace Api\Acc\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Settings\Requests\AccountRequest as REQUEST;
use Api\Acc\Settings\Services\AccountService as SERVICE;

class AccountController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
