<?php
namespace Api\Acc\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Settings\Requests\AccountDetailRequest as REQUEST;
use Api\Acc\Settings\Services\AccountDetailService as SERVICE;

class AccountDetailController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
