<?php
namespace Api\Acc\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Settings\Requests\AccountGroupRequest as REQUEST;
use Api\Acc\Settings\Services\AccountGroupService as SERVICE;

class AccountGroupController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
