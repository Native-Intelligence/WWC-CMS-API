<?php
namespace Api\Acc\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Settings\Requests\ConfigRequest as REQUEST;
use Api\Acc\Settings\Services\ConfigService as SERVICE;

class ConfigController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
