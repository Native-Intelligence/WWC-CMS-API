<?php
namespace Api\Acc\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Settings\Requests\DetailedAccountRequest as REQUEST;
use Api\Acc\Settings\Services\DetailedAccountService as SERVICE;

class DetailedAccountController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
