<?php
namespace Api\Acc\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Acc\Settings\Requests\DetailedAccountGroupRequest as REQUEST;
use Api\Acc\Settings\Services\DetailedAccountGroupService as SERVICE;

class DetailedAccountGroupController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
