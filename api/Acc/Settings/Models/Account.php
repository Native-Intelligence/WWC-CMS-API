<?php
namespace Api\Acc\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Account extends Model
{

    protected $table = 'acc_set_accs';
    
    protected $view = 'acc_set_accs_view';
    
    protected $fillable = [
                            'title',
                            'code',
                            'type',
                            'g_account_type_id',
                            'parent_id',
                            'level_4',
                            'level_5',
                            'level_6',
                            'status',
                            'scope'
                           ];
}
