<?php
namespace Api\Acc\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class AccountDetail extends Model
{

    protected $table = 'acc_set_acc_dets';
    
    protected $view = 'acc_set_acc_dets_view';

    protected $fillable = [
                            'level',
                            'account_id',
                            'account_det_id',
                            'status',
                            'scope'
                           ];
    public function account()
    {
        return $this->belongsTo('Api\Acc\Settings\Models\Account', 'account_id');
    }
}
