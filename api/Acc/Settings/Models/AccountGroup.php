<?php
namespace Api\Acc\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class AccountGroup extends Model
{

    protected $table = 'acc_set_acc_grps';

    protected $fillable = [
                            'title',
                            'code',
                            'account_id',
                            'type',
                            'status',
                            'scope'
                           ];

    protected $appends = [
        'acc_title'
    ];

    public function getAccTitleAttribute()
    {
        return $this->acc->title;
    }
    
    public function acc()
    {
        return $this->belongsTo('Api\Acc\Settings\Models\Account', 'account_id');
    }
}
