<?php
namespace Api\Acc\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Config extends Model
{

    protected $table = 'acc_set_configs';

    protected $fillable = [
                            'title',
                            'g_document_type_id',
                            'g_service_type_id',
                            'details',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'details' => 'object'
    ];
}
