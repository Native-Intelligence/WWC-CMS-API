<?php
namespace Api\Acc\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class DetailedAccount extends Model
{

    protected $table = 'acc_set_det_accs';

    protected $fillable = [
                            'title',
                            'group_id',
                            'code',
                            'status',
                            'scope'
                           ];

    protected $appends = [
        'group_title'
    ];
    
    public function getGroupTitleAttribute()
    {
        return $this->acc->title;
    }
    
    public function acc()
    {
        return $this->belongsTo('Api\Acc\Settings\Models\DetailedAccountGroup', 'group_id');
    }
}
