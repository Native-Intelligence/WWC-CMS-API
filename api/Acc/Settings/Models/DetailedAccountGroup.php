<?php
namespace Api\Acc\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class DetailedAccountGroup extends Model
{

    protected $table = 'acc_set_det_acc_grps';

    protected $fillable = [
                            'title',
                            'code',
                            'status',
                            'scope'
                           ];
}
