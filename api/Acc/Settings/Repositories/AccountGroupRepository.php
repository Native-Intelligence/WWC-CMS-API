<?php
namespace Api\Acc\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Settings\Models\AccountGroup;

class AccountGroupRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Settings\\Models\\AccountGroup';
    }
}
