<?php
namespace Api\Acc\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Acc\Settings\Models\Account;

class AccountRepository extends Repository
{

    public function model()
    {
        return 'Api\\Acc\\Settings\\Models\\Account';
    }
}
