<?php
namespace Api\Acc\Settings\Requests;

use Infrastructure\Requests\Request;

class AccountDetailRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('account_detail');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'level' => 'required|integer',
            'account_id' => 'required|integer',
            'account_det_id' => 'required|integer',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'level' => 'sometimes|required|integer',
            'account_id' => 'sometimes|required|integre',
            'account_det_id' => 'sometimes|required|integer',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
}
