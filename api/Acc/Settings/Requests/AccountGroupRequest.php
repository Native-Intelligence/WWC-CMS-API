<?php
namespace Api\Acc\Settings\Requests;

use Infrastructure\Requests\Request;

class AccountGroupRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('account_group');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'string',
            'code' => 'required|string',
            'account_id' => 'integer',
            'type' => 'required|integer',
            // 'accounting_import' => 'nullable|array',
            // 'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'string',
            'code' => 'sometimes|required|string',
            'type' => 'sometimes|required|integer',
            'account_id' => 'integer',
            // 'parent_id' => 'sometimes|required|integer',
            // 'accounting_import' => 'nullable|array',
            // 'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
}
