<?php
namespace Api\Acc\Settings\Requests;

use Infrastructure\Requests\Request;

class AccountRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('account');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'string',
            'code' => 'required|string',
            'type' => 'required|integer',
            'g_account_type_id' => 'required|integer',
            'parent_id' => 'nullable|integer',
            // 'details' => 'nullable|array',
            'level_4' => 'nullable|integer',
            'level_5' => 'nullable|integer',
            'level_6' => 'nullable|integer',
            // 'group_id' => 'integer',
            // 'accounting_import' => 'nullable|array',
            // 'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'type' => 'sometimes|required|integer',
            'code' => 'sometimes|required|string',
            'g_account_type_id' => 'sometimes|required|integer',
            'parent_id' => 'nullable|integer',
            // 'details' => 'nullable|array',
            'level_4' => 'nullable|integer',
            'level_5' => 'nullable|integer',
            'level_6' => 'nullable|integer',
            // 'group_id' => 'integer',
            // 'accounting_import' => 'nullable|array',
            // 'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
}
