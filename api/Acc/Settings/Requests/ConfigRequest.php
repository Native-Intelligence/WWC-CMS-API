<?php
namespace Api\Acc\Settings\Requests;

use Infrastructure\Requests\Request;

class ConfigRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('config');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'g_document_type_id' => 'required|integer',
            'g_service_type_id' => 'integer|nullable',
            'details' => 'required|array',
            'status' => 'integer',
            'scope' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'g_document_type_id' => 'sometimes|required|integer',
            'g_service_type_id' => 'integer|nullable',
            'details' => 'sometimes|required|array',
            'status' => 'integer',
            'scope' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
