<?php
namespace Api\Acc\Settings\Requests;

use Infrastructure\Requests\Request;

class DetailedAccountGroupRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('detailed_account_group');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'string',
            'code' => 'required|string',
            // 'parent_id' => 'integer',
            // 'accounting_import' => 'nullable|array',
            // 'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'string',
            'code' => 'sometimes|required|string',
            // 'parent_id' => 'integer',
            // 'accounting_import' => 'nullable|array',
            // 'key' => 'string',
            'status' => 'integer',
            'scope' => 'scope'
        ];
        $this->validate($this->data, $this->rules);
    }
}
