<?php
namespace Api\Acc\Settings\Services;

use Infrastructure\Services\Service;
use Api\Acc\Settings\Repositories\AccountDetailRepository;

// use Api\Settings\Services\GdataService;

class AccountDetailService extends Service
{
    public function __construct(AccountDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'account_detail';
        $this->uniqueKey = 'account_id';
    }
}
