<?php
namespace Api\Acc\Settings\Services;

use Infrastructure\Services\Service;
use Api\Acc\Settings\Repositories\AccountGroupRepository;

class AccountGroupService extends Service
{

    public function __construct(AccountGroupRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'account_group';
        $this->uniqueKey = 'title';
    }
}
