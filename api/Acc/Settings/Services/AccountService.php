<?php
namespace Api\Acc\Settings\Services;

use Infrastructure\Services\Service;
use Api\Acc\Settings\Repositories\AccountRepository;

// use Api\Settings\Services\GdataService;

class AccountService extends Service
{
    public function __construct(AccountRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'account';
        $this->uniqueKey = 'title';
    }
}
