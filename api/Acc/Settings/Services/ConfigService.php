<?php
namespace Api\Acc\Settings\Services;

use Infrastructure\Services\Service;
use Api\Acc\Settings\Repositories\ConfigRepository;

class ConfigService extends Service
{

    public function __construct(ConfigRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'config';
        $this->uniqueKey = 'g_document_type_id&g_service_type_id';
    }
}
