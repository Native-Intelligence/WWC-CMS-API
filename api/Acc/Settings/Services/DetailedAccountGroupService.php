<?php
namespace Api\Acc\Settings\Services;

use Infrastructure\Services\Service;
use Api\Acc\Settings\Repositories\DetailedAccountGroupRepository;

class DetailedAccountGroupService extends Service
{

    public function __construct(DetailedAccountGroupRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'detailed_account_group';
        $this->uniqueKey = 'title';
    }
}
