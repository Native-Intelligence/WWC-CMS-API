<?php
namespace Api\Acc\Settings\Services;

use Infrastructure\Services\Service;
use Api\Acc\Settings\Repositories\DetailedAccountRepository;

class DetailedAccountService extends Service
{

    public function __construct(DetailedAccountRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'detailed_account';
        $this->uniqueKey = 'title';
    }
}
