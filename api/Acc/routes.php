<?php
$router->group(
    ['namespace' => 'Controllers', 'prefix' => 'accountings', 'middleware' => ['cors', 'auth']],
    function () use ($router) {
        $router->group(
            ['prefix' => 'payments'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getPayments',
                    'uses' => 'PaymentController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getPayment',
                    'uses' => 'PaymentController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createPayment',
                    'uses' => 'PaymentController@create'
                    ]
                );
            
                $router->post(
                    '/update',
                    [
                    'as' => 'updatePayment',
                    'uses' => 'PaymentController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyPayment',
                    'uses' => 'PaymentController@destroy'
                    ]
                );
            
                $router->group(
                    ['prefix' => 'ftp'],
                    function () use ($router) {
                        $router->get(
                            '/',
                            [
                            'as' => 'getFtps',
                            'uses' => 'FtpController@getAll'
                            ]
                        );
                    
                        $router->put(
                            '/get',
                            [
                            'as' => 'getFtp',
                            'uses' => 'FtpController@getByParams'
                            ]
                        );

                        $router->post(
                            '/create',
                            [
                            'as' => 'createFtp',
                            'uses' => 'FtpController@create'
                            ]
                        );
                    
                        $router->post(
                            '/update',
                            [
                            'as' => 'updateFtp',
                            'uses' => 'FtpController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy',
                            [
                            'as' => 'destroyFtp',
                            'uses' => 'FtpController@destroy'
                            ]
                        );
                    }
                );
            }
        );

        // $router->group(['prefix' => 'accounts'], function () use ($router) {
        //         $router->get('/', [
        //             'as' => 'getAccounts',
        //             'uses' => 'AccountController@getAll'
        //         ]);
        //
        //         $router->put('/get', [
        //             'as' => 'getAccount',
        //             'uses' => 'AccountController@getByParams'
        //         ]);
        //
        //         $router->post('/create', [
        //             'as' => 'createAccount',
        //             'uses' => 'AccountController@create'
        //         ]);
        //
        //         $router->post('/update', [
        //             'as' => 'updateAccount',
        //             'uses' => 'AccountController@update'
        //         ]);
        //
        //         $router->delete('/destroy', [
        //             'as' => 'destroyAccount',
        //             'uses' => 'AccountController@destroy'
        //         ]);
        // });

        $router->group(
            ['prefix' => 'documents'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getDocuments',
                    'uses' => 'DocumentController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getDocument',
                    'uses' => 'DocumentController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createDocument',
                    'uses' => 'DocumentController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateDocument',
                    'uses' => 'DocumentController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyDocument',
                    'uses' => 'DocumentController@destroy'
                    ]
                );
            
                $router->group(
                    ['prefix' => 'details'],
                    function () use ($router) {
                        $router->get(
                            '/',
                            [
                            'as' => 'getDocumentDetails',
                            'uses' => 'DocumentDetailController@getAll'
                            ]
                        );
            
                        $router->put(
                            '/get',
                            [
                            'as' => 'getDocumentDetail',
                            'uses' => 'DocumentDetailController@getByParams'
                            ]
                        );
            
                        $router->post(
                            '/create',
                            [
                            'as' => 'createDocumentDetail',
                            'uses' => 'DocumentDetailController@create'
                            ]
                        );
            
                        $router->post(
                            '/update',
                            [
                            'as' => 'updateDocumentDetail',
                            'uses' => 'DocumentDetailController@update'
                            ]
                        );
            
                        $router->delete(
                            '/destroy',
                            [
                            'as' => 'destroyDocumentDetail',
                            'uses' => 'DocumentDetailController@destroy'
                            ]
                        );
                    }
                );
            }
        );

        $router->group(
            ['prefix' => 'transactions'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getTransactions',
                    'uses' => 'TransactionController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getTransaction',
                    'uses' => 'TransactionController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createTransaction',
                    'uses' => 'TransactionController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateTransaction',
                    'uses' => 'TransactionController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyTransaction',
                    'uses' => 'TransactionController@destroy'
                    ]
                );
            }
        );
    }
);


$router->group(
    ['namespace' => 'Settings\Controllers', 'prefix' => 'accountings', 'middleware' => ['cors', 'auth']],
    function () use ($router) {
        $router->group(
            ['prefix' => 'configs'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getConfigs',
                    'uses' => 'ConfigController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getConfig',
                    'uses' => 'ConfigController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createConfig',
                    'uses' => 'ConfigController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateConfig',
                    'uses' => 'ConfigController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyConfig',
                    'uses' => 'ConfigController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'accounts'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getAccounts',
                    'uses' => 'AccountController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getAccount',
                    'uses' => 'AccountController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createAccount',
                    'uses' => 'AccountController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateAccount',
                    'uses' => 'AccountController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyAccount',
                    'uses' => 'AccountController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'account_details'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getAccountDetails',
                    'uses' => 'AccountDetailController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getAccountDetail',
                    'uses' => 'AccountDetailController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createAccountDetail',
                    'uses' => 'AccountDetailController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateAccountDetail',
                    'uses' => 'AccountDetailController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyAccountDetail',
                    'uses' => 'AccountDetailController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'account_groups'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getAccountGroups',
                    'uses' => 'AccountGroupController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getAccountGroup',
                    'uses' => 'AccountGroupController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createAccountGroup',
                    'uses' => 'AccountGroupController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateAccountGroup',
                    'uses' => 'AccountGroupController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyAccountGroup',
                    'uses' => 'AccountGroupController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'detailed_accounts'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getDetailedAccount',
                    'uses' => 'DetailedAccountController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getDetailedAccount',
                    'uses' => 'DetailedAccountController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createDetailedAccount',
                    'uses' => 'DetailedAccountController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateDetailedAccount',
                    'uses' => 'DetailedAccountController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyDetailedAccount',
                    'uses' => 'DetailedAccountController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'detailed_account_groups'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getDetailedAccountGroups',
                    'uses' => 'DetailedAccountGroupController@getAll'
                    ]
                );
    
                $router->put(
                    '/get',
                    [
                    'as' => 'getDetailedAccountGroup',
                    'uses' => 'DetailedAccountGroupController@getByParams'
                    ]
                );
    
                $router->post(
                    '/create',
                    [
                    'as' => 'createDetailedAccountGroup',
                    'uses' => 'DetailedAccountGroupController@create'
                    ]
                );
    
                $router->post(
                    '/update',
                    [
                    'as' => 'updateDetailedAccountGroup',
                    'uses' => 'DetailedAccountGroupController@update'
                    ]
                );
    
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyDetailedAccountGroup',
                    'uses' => 'DetailedAccountGroupController@destroy'
                    ]
                );
            }
        );
    }
);
