<?php
namespace Api\Cartables\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Cartables\Requests\ActionRequest as REQUEST;
use Api\Cartables\Services\ActionService as SERVICE;

class ActionController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
