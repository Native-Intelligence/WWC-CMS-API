<?php
namespace Api\Cartables\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Cartables\Requests\CartableRequest as REQUEST;
use Api\Cartables\Services\CartableService as SERVICE;

class CartableController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
