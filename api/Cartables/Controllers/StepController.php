<?php
namespace Api\Cartables\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Cartables\Requests\StepRequest as REQUEST;
use Api\Cartables\Services\StepService as SERVICE;

class StepController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
