<?php
namespace Api\Cartables\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Cartables\Requests\WorkflowRequest as REQUEST;
use Api\Cartables\Services\WorkflowService as SERVICE;

class WorkflowController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
