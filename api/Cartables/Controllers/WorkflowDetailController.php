<?php
namespace Api\Cartables\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Cartables\Requests\WorkflowDetailRequest as REQUEST;
use Api\Cartables\Services\WorkflowDetailService as SERVICE;

class WorkflowDetailController extends Controller
{
    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
