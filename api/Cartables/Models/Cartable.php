<?php

namespace Api\Cartables\Models;

use Infrastructure\Database\Eloquent\Model;

class Cartable extends Model
{
    protected $table = 'mid_request_workflow';
    
    protected $view = 'cartables_view';
    
    protected $fillable = [
        'request_id',
        'workflow_detail_id',
        'status',
        'user_id',
        'scope'
    ];
    
    protected $casts = [
        'request_details' => 'object',
    ];
    
    public function getRequestScopeAttribute($value)
    {
        return (int)$value;
    }
    
    public function getRequestScopeCityAttribute($value)
    {
        return (int)$value;
    }
    
}
