<?php
namespace Api\Cartables\Models;

use Infrastructure\Database\Eloquent\Model;

class Step extends Model
{
    protected $table = 'cartable_steps';
    
    protected $fillable = [
                           'title',
                           'model',
                        //    'description',
                           'status',
                           'scope'
                          ];

}
