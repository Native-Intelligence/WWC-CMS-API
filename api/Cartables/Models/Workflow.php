<?php
namespace Api\Cartables\Models;

use Infrastructure\Database\Eloquent\Model;

class Workflow extends Model
{
    protected $table = 'cartable_workflows';
    
    protected $fillable = [
                           'g_service_type_id',
                           'title',
                           'scope',
                           'status'
                          ];
                          
    public function workflowDetails()
    {
        return $this->hasMany('Api\Cartables\Models\WorkflowDetail');
    }
}
