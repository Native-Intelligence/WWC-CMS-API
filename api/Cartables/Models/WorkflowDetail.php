<?php

namespace Api\Cartables\Models;

use Infrastructure\Database\Eloquent\Model;

class WorkflowDetail extends Model
{
    protected $table = 'cartable_workflow_details';
    
    protected $view = 'cartable_workflow_details_view';
    
    protected $fillable = [
        'workflow_id',
        'step_id',
        'prev_id',
        'next_id',
        'next_false_id',
        'g_cartable_condition_id',
        'status',
        'scope'
    ];
}
