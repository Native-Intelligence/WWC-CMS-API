<?php
namespace Api\Cartables\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Cartables\Models\Action;

class ActionRepository extends Repository
{
    public function model()
    {
        return 'Api\\Cartables\\Models\\Action';
    }
}
