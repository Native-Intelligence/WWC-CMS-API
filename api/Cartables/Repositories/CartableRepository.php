<?php
namespace Api\Cartables\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Cartables\Models\Cartable;

class CartableRepository extends Repository
{
    public function model()
    {
        return 'Api\\Cartables\\Models\\Cartable';
    }
}
