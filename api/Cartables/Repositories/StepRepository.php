<?php
namespace Api\Cartables\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Cartables\Models\Step;

class StepRepository extends Repository
{
    public function model()
    {
        return 'Api\\Cartables\\Models\\Step';
    }
}
