<?php
namespace Api\Cartables\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Cartables\Models\WorkflowDetail;

class WorkflowDetailRepository extends Repository
{

    public function model()
    {
        return 'Api\\Cartables\\Models\\WorkflowDetail';
    }
}
