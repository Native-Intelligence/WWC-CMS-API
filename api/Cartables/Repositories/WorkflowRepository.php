<?php
namespace Api\Cartables\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Cartables\Models\Workflow;

class WorkflowRepository extends Repository
{
    public function model()
    {
        return 'Api\\Cartables\\Models\\Workflow';
    }
}
