<?php
namespace Api\Cartables\Requests;

use Infrastructure\Requests\Request;

class ActionRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('action');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'request_id' => 'required|integer',
            'success' => 'required|boolean'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'request_id' => 'required|integer',
            'success' => 'required|boolean'
        ];
        $this->validate($this->data, $this->rules);
    }
}
