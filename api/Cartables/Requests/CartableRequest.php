<?php
namespace Api\Cartables\Requests;

use Infrastructure\Requests\Request;

class CartableRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('cartable');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'slug' => 'required|string',
            'name' => 'required|string',
            'user_id' => 'integer',
            'permissions' => 'array',
            'scope' => 'required|integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'slug' => 'string',
            'name' => 'string',
            'user_id' => 'integer',
            'permissions' => 'array',
            'scope' => 'integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
