<?php

namespace Api\Cartables\Requests;

use Infrastructure\Requests\Request;

class StepRequest extends Request
{
    
    public function __construct(Request $request)
    {
        $this->data = $request->json('step');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title'       => 'required|string',
            'model'       => 'required|string',
            // 'description' => 'string',
            'scope'       => 'required|integer',
            'status'      => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id'          => 'required|integer',
            'title'       => 'string',
            'model'       => 'sometimes|required|string',
            // 'description' => 'string',
            'scope'       => 'integer',
            'status'      => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
