<?php

namespace Api\Cartables\Requests;

use Infrastructure\Requests\Request;

class WorkflowDetailRequest extends Request
{
    
    public function __construct(Request $request)
    {
        $this->data = $request->json('workflow_detail');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'workflow_id'             => 'required|integer',
            'step_id'                 => 'required|integer',
            'prev_id'                 => 'integer',
            'next_id'                 => 'integer',
            'next_false_id'           => 'integer',
            'g_cartable_condition_id' => 'integer',
            'status'                  => 'integer',
            'scope'                   => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id'             => 'required|integer',
            'step_id'                 => 'integer',
            'prev_id'                 => 'integer',
            'next_id'                 => 'integer',
            'next_false_id'           => 'integer',
            'g_cartable_condition_id' => 'integer',
            'status'                  => 'integer',
            'scope'                   => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
