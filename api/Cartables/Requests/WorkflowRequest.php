<?php

namespace Api\Cartables\Requests;

use Infrastructure\Requests\Request;

class WorkflowRequest extends Request
{
    
    public function __construct(Request $request)
    {
        $this->data = $request->json('workflow');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title'             => 'required|string',
            'g_service_type_id' => 'required|integer',
            'scope'             => 'required|integer',
            'status'            => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id'                => 'required|integer',
            'title'             => 'string',
            'g_service_type_id' => 'integer',
            'scope'             => 'integer',
            'status'            => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
