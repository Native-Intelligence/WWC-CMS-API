<?php
namespace Api\Cartables\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Cartables\Repositories\ActionRepository;

class ActionService extends Service
{

    public function __construct(ActionRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'action';
        $this->uniqueKey = '';
    }

    public function create($data)
    {
        $this->checkUniqueKeyExist($data);
        
        $result = $this->data;
        // $result = $this->repository->create((array) $this->data);

        $this->archiveEvent('create', $result);

        if (isset($this->event['created'])) {
            foreach ($this->event['created'] as $val) {
                event(new $val($result));
            }
        }
        
        return $result;
    }
}
