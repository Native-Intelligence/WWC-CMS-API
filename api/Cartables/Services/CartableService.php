<?php
namespace Api\Cartables\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Cartables\Repositories\CartableRepository;

class CartableService extends Service
{

    public function __construct(CartableRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'cartable';
        $this->uniqueKey = '';
        $this->event = [
            'created' => [],
            'updated' => [],
            'deleted' => []
        ];
        // $this->userCheck = true;
    }
    
    public function scopeCheckQuery($query)
    {
        
        
        if(!in_array(
            '1',
            array_map(
                function ($v) {
                            return $v->id;
                },
                app()->userHelper::$currentUser->access->roles
            )
        )
        ) {
            if(!isset(app()->userHelper::$currentUser->access->workflow)) {
                throw new Exception(40324);
            }
            
            $query = $query->where(
                function ($query) {
                    $query->orWhereNull('user_id')->orWhere(['user_id'=>app()->userHelper::$currentUser->id]);
                }
            )
            ->whereIn('workflow_details_id', (array) app()->userHelper::$currentUser->access->workflow);
        }
        
        $query = $query->where('status', '!=', '9')
            ->where(
                function ($query) {
                    $query->orWhere(['scope' => app()->helper::$scopeDefault]);
                    foreach ((array) app()->userHelper::$currentUser->access->scope as $v) {
                        $query->orWhereBetween('scope', $v);
                        $query->orWhereBetween('scope', [1, 1]);
                    }
                }
            );
        
        // if(!in_array(
        //     '1',
        //     array_map(
        //         function ($v) {
        //                     return $v->id;
        //         },
        //         app()->userHelper::$currentUser->access->roles
        //     )
        // )
        // ) {
        //     $query = $query->where(
        //         function ($query) {
        //             $query->orWhereNull('user_id')->orWhere(['user_id'=>app()->userHelper::$currentUser->id]);
        //         }
        //     );
        // }
        
        // if($this->userCheck) {
        //     $query = $query->where('user_id', '=', app()->userHelper::$currentUser->id);
        // }
        
        return $query;
    }
}
