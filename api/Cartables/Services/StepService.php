<?php
namespace Api\Cartables\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Cartables\Repositories\StepRepository;

class StepService extends Service
{

    public function __construct(StepRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'step';
        $this->uniqueKey = '';
    }
}
