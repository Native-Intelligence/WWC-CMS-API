<?php
namespace Api\Cartables\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Cartables\Repositories\WorkflowDetailRepository;

class WorkflowDetailService extends Service
{

    public function __construct(WorkflowDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'workflow_detail';
        $this->uniqueKey = '';
    }


    // public function getAll()
    // {
    //     return $this->getAllWithoutPaginate()->all();
    // }

}
