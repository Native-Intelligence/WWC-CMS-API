<?php
namespace Api\Cartables\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Cartables\Repositories\WorkflowRepository;

class WorkflowService extends Service
{

    public function __construct(WorkflowRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'workflow';
        $this->uniqueKey = '';
    }
}
