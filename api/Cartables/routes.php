<?php
$router->group(
    ['namespace' => 'Controllers', 'prefix' => 'cartables', 'middleware' => ['cors', 'auth']],
    function () use ($router) {
        $router->get(
            '/', [
            'as' => 'getCartables',
            'uses' => 'CartableController@getAll'
            ]
        );

        $router->put(
            '/get', [
            'as' => 'getCartable',
            'uses' => 'CartableController@getByParams'
            ]
        );

        // $router->post(
        //     '/create', [
        //     'as' => 'createCartable',
        //     'uses' => 'CartableController@create'
        //     ]
        // );

        // $router->post(
        //     '/update', [
        //     'as' => 'updateCartable',
        //     'uses' => 'CartableController@update'
        //     ]
        // );

        // $router->delete(
        //     '/destroy', [
        //     'as' => 'destroyCartable',
        //     'uses' => 'CartableController@destroy'
        //     ]
        // );
        $router->group(
            ['prefix' => 'action'], function () use ($router) {
                $router->get(
                    '/', [
                    'as' => 'getActions',
                    'uses' => 'ActionController@getAll'
                    ]
                );
                
                $router->put(
                    '/get', [
                    'as' => 'getAction',
                    'uses' => 'ActionController@getByParams'
                    ]
                );

                $router->post(
                    '/create', [
                    'as' => 'createAction',
                    'uses' => 'ActionController@create'
                    ]
                );
        
                // $router->post(
                //     '/update', [
                //     'as' => 'updateAction',
                //     'uses' => 'ActionController@update'
                //     ]
                // );
                //
                // $router->delete(
                //     '/destroy', [
                //     'as' => 'destroyAction',
                //     'uses' => 'ActionController@destroy'
                //     ]
                // );
            }
        );
                
        $router->group(
            ['prefix' => 'workflows'], function () use ($router) {
                $router->get(
                    '/', [
                    'as' => 'getWorkflows',
                    'uses' => 'WorkflowController@getAll'
                    ]
                );
        
                $router->put(
                    '/get', [
                    'as' => 'getWorkflow',
                    'uses' => 'WorkflowController@getByParams'
                    ]
                );

                $router->post(
                    '/create', [
                    'as' => 'createWorkflow',
                    'uses' => 'WorkflowController@create'
                    ]
                );
        
                $router->post(
                    '/update', [
                    'as' => 'updateWorkflow',
                    'uses' => 'WorkflowController@update'
                    ]
                );

                $router->delete(
                    '/destroy', [
                    'as' => 'destroyWorkflow',
                    'uses' => 'WorkflowController@destroy'
                    ]
                );
        
                $router->group(
                    ['prefix' => 'details'], function () use ($router) {
                        $router->get(
                            '/', [
                            'as' => 'getWorkflowGroups',
                            'uses' => 'WorkflowDetailController@getAll'
                            ]
                        );
            
                        $router->put(
                            '/get', [
                            'as' => 'getWorkflowGroup',
                            'uses' => 'WorkflowDetailController@getByParams'
                            ]
                        );

                        $router->post(
                            '/create', [
                            'as' => 'createWorkflowGroup',
                            'uses' => 'WorkflowDetailController@create'
                            ]
                        );
            
                        $router->post(
                            '/update', [
                            'as' => 'updateWorkflowGroup',
                            'uses' => 'WorkflowDetailController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy', [
                            'as' => 'destroyWorkflowGroup',
                            'uses' => 'WorkflowDetailController@destroy'
                            ]
                        );
                    }
                );
            }
        );


        $router->group(
            ['prefix' => 'steps'], function () use ($router) {
                $router->get(
                    '/', [
                    'as' => 'getSteps',
                    'uses' => 'StepController@getAll'
                    ]
                );
        
                $router->put(
                    '/get', [
                    'as' => 'getStep',
                    'uses' => 'StepController@getByParams'
                    ]
                );

                $router->post(
                    '/create', [
                    'as' => 'createStep',
                    'uses' => 'StepController@create'
                    ]
                );
        
                $router->post(
                    '/update', [
                    'as' => 'updateStep',
                    'uses' => 'StepController@update'
                    ]
                );

                $router->delete(
                    '/destroy', [
                    'as' => 'destroyStep',
                    'uses' => 'StepController@destroy'
                    ]
                );
            }
        );
    }
);
