<?php
namespace Api\Reports\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Reports\Requests\DesignRequest as REQUEST;
use Api\Reports\Services\DesignService as SERVICE;

class DesignController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
