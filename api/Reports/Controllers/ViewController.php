<?php
namespace Api\Reports\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Reports\Requests\ViewRequest as REQUEST;
use Api\Reports\Services\ViewService as SERVICE;

class ViewController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
