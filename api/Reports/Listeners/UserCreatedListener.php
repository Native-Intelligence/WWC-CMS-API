<?php

namespace Api\Settings\Listeners;

use Api\Settings\Repositories\TokenRepository;
use Api\Settings\Events\UserCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreatedListener implements ShouldQueue
{
    use InteractsWithQueue;
    
    private $_tokenRepository;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(TokenRepository $tokenRepository)
    {
        $this->_tokenRepository = $tokenRepository;
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(UserCreatedEvent $userId)
    {
        // dd($user_id->user_id);
        $hasher = app()->make('hash');
        
        $token = [
            'access_token' => $hasher->make('1234'),
            'refresh_token' => $hasher->make('1234'),
            'user_id' => $userId->userId,
            'client_id' => 1, //select from client table with app-name and app_secret
            'status' => 1
            
        ];
        // dd($token);
        
        $this->_tokenRepository->create($token);
    }
}
