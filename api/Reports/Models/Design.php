<?php
namespace Api\Reports\Models;

use Infrastructure\Database\Eloquent\Model;

class Design extends Model
{

    protected $table = 'report_designs';

    protected $fillable = [
        'title',
        'group',
        'query',
        'description',
        'details',
        'columns',
        'filters',
        'format',
        'print_layout',
        'status',
        'scope'
    ];

    protected $casts = [
    'details' => 'object',
    'filters' => 'object',
    'format' => 'object',
    'columns' => 'object'];
}
