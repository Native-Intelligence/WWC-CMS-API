<?php
namespace Api\Reports\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Reports\Models\Design;

class DesignRepository extends Repository
{

    public function model()
    {
        return 'Api\\Reports\\Models\\Design';
    }
}
