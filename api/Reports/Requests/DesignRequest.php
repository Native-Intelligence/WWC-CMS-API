<?php
namespace Api\Reports\Requests;

use Infrastructure\Requests\Request;

class DesignRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('design');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'group' => 'required|string',
            'description' => 'string|nullable',
            'query' => 'required|string',
            'columns' => 'array|nullable',
            'details' => 'array|nullable',
            'filters' => 'array|nullable',
            'format' => 'array|nullable',
            'print_layout' => 'string|nullable',
            'status' => 'integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'group' => 'sometimes|required|string',
            'description' => 'string|nullable',
            'query' => 'sometimes|required|string',
            'columns' => 'array|nullable',
            'details' => 'array',
            'filters' => 'array|nullable',
            'format' => 'array|nullable',
            'status' => 'integer',
            'print_layout' => 'string|nullable',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
