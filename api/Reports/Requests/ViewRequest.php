<?php
namespace Api\Reports\Requests;

use Infrastructure\Requests\Request;

class ViewRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('view');
    }
    
    // public function createValidation()
    // {
    //     $this->rules = [
    //         'title' => 'required|string',
    //         'filters' => 'array',
    //         'scope' => 'required|integer'
    //     ];
    //     $this->validate($this->data, $this->rules);
    // }
    //
    // public function updateValidation()
    // {
    //     $this->rules = [
    //         'id' => 'required|integer',
    //         'title' => 'string',
    //         'group' => 'string',
    //         'description' => 'string|nullable',
    //         'query' => 'string',
    //         'details' => 'array',
    //         'scope' => 'integer'
    //     ];
    //     $this->validate($this->data, $this->rules);
    // }
}
