<?php
namespace Api\Reports\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Reports\Repositories\DesignRepository;
use DB;

class DesignService extends Service
{

    public function __construct(DesignRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'design';
        $this->uniqueKey = '';
    }
    
    public function getColumns(string $query, array $condition = [])
    {
        foreach ($condition as $key => $value) {
            $query = str_replace($value['expr'], 'NULL', $query, $c);
        }
        
        $result = [];
        foreach (DB::select($query)[0] as $key => $value) {
            $result[] = [
                'slug' => $key,
                'type' => 'string'
                ];
        }
        return $result;
    }
    
    public function checkUniqueKeyExist($data)
    {
        parent::checkUniqueKeyExist($data);

        // $this->data = $this->getData($data);
        $this->data->columns = $this->getColumns($this->data->query, (isset($this->data->filters) ? $this->data->filters : []));
        // dd($this->repository->getQueryField());
        //TODO: $this->exist = $this->repository->findByField($this->uniqueKey, $this->data->{$this->uniqueKey})->first();

        // if (!is_null($this->exist)) {
        //     throw new Exception(40640);
        // }
    }
}
