<?php
namespace Api\Reports\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Reports\Services\DesignService;
use DB;

// use Api\Reports\Repositories\ViewRepository;

class ViewService extends Service
{

    private $_designService;

    public function __construct(DesignService $designService)
    {
        // $this->repository = $repository;
        $this->key = 'view';
        $this->uniqueKey = '';
        $this->_designService = $designService;
    }
    
    public function getAllByField(array $condition = [], array $sort = [], array $cloumns = ['*'])
    {
        // $condition = (array) $data->condition;
        
        // $query = $this->_designService->find($this->data->id)->query;
        // foreach ($condition as $key => $value) {
        //     $query = str_replace(($value == 'null') ? "'".$key."'" : $key, $value, $query, $c);
        // }
        $query = $this->_designService->find($this->data->id)->query;
        foreach ($condition as $key => $value) {
            if (is_array($value)) {
                $value = implode(',', $value);
            } else if (is_string($value)) {
                $value = '\'' . $value . '\'';
            } else if (is_null($value)) {
                $value = 'NULL';
            }

            $query = str_replace($key, $value, $query, $c);
        }
        return DB::select($query);
        // return $this->repository->scopeQuery(function ($query) {
        //     return $query->where('status', '!=', '9');
        // })->findWhere($condition)->all();
    }
}
