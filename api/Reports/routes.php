<?php
$router->group(['prefix' => 'reports', 'middleware' => ['cors', 'auth']], function () use ($router) {
    $router->group(['prefix' => 'design'], function () use ($router) {
        $router->get('/', [
            'as' => 'getDesigns',
            'uses' => 'DesignController@getAll'
        ]);
        
        $router->put('/get', [
            'as' => 'getDesign',
            'uses' => 'DesignController@getByParams'
        ]);

        $router->post('/create', [
            'as' => 'createDesign',
            'uses' => 'DesignController@create'
        ]);
        
        $router->post('/update', [
            'as' => 'updateDesign',
            'uses' => 'DesignController@update'
        ]);

        $router->delete('/destroy', [
            'as' => 'destroyDesign',
            'uses' => 'DesignController@destroy'
        ]);
    });

    $router->group(['prefix' => 'view'], function () use ($router) {
        $router->get('/', [
            'as' => 'getViews',
            'uses' => 'ViewController@getAll'
        ]);
        
        $router->put('/get', [
            'as' => 'getView',
            'uses' => 'ViewController@getByParams'
        ]);

        $router->post('/create', [
            'as' => 'createView',
            'uses' => 'ViewController@create'
        ]);
        
        $router->post('/update', [
            'as' => 'updateView',
            'uses' => 'ViewController@update'
        ]);

        $router->delete('/destroy', [
            'as' => 'destroyView',
            'uses' => 'ViewController@destroy'
        ]);
    });
});
