<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\CalculationRequest as REQUEST;
use Api\Sales\Services\CalculationService as SERVICE;

class CalculationController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
    
    // public function installment()
    // {
    //     $this->request->updateValidation();
    //
    //     return $this->response(
    //         app()->helper::jGenerate(
    //             $this->service->calcInstallments($this->request)
    //         ),
    //         201
    //     );
    // }
    
    // public function create()
    // {
    //     $this->request->createValidation();
    //
    //     return $this->response(
    //         app()->helper::jGenerate(
    //             $this->service->create($this->request)
    //         ),
    //         201
    //     );
    // }
}
