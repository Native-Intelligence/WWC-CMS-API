<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\EvaluationUserAssignmentRequest as REQUEST;
use Api\Sales\Services\EvaluationUserAssignmentService as SERVICE;

class EvaluationUserAssignmentController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
