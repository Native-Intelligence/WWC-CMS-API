<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\FileRequest as REQUEST;
use Api\Sales\Services\FileService as SERVICE;

class FileController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
