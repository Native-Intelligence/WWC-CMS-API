<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\InstallationRequest as REQUEST;
use Api\Sales\Services\InstallationService as SERVICE;

class InstallationController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
