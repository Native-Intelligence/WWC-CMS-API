<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\InstallmentRequest as REQUEST;
use Api\Sales\Services\InstallmentService as SERVICE;

class InstallmentController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
    
    public function installment()
    {
        $this->request->createValidation();
        
        return $this->response(
            app()->helper::jGenerate(
                $this->service->calcInstallments($this->request)
            ),
            201
        );
    }
}
