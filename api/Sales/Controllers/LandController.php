<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\LandRequest as REQUEST;
use Api\Sales\Services\LandService as SERVICE;

class LandController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
