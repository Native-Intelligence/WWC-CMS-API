<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\SurveyRequest as REQUEST;
use Api\Sales\Services\SurveyService as SERVICE;

class SurveyController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
