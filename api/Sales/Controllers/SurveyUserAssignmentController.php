<?php
namespace Api\Sales\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\SurveyUserAssignmentRequest as REQUEST;
use Api\Sales\Services\SurveyUserAssignmentService as SERVICE;

class SurveyUserAssignmentController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
