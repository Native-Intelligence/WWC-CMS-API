<?php

namespace Api\Sales\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Controllers\Controller;
use Api\Sales\Requests\TechnicalEvaluationRequest as REQ;
use Api\Sales\Services\TechnicalEvaluationService as SERVICE;

class TechnicalEvaluationController extends Controller {

	public function __construct( SERVICE $service, REQ $request ) {
		$this->service = $service;
		$this->request = $request;
	}


}
