<?php

namespace Api\Sales\Events;

use Infrastructure\Events\Event;
// use Api\Sales\Models\Survey;

class CartableChangeEvent extends Event
{

    public $result;

    public function __construct($result)
    {
        $this->result = $result;
    }
}
