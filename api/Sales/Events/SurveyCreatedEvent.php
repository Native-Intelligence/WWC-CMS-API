<?php

namespace Api\Sales\Events;

use Infrastructure\Events\Event;
use Api\Sales\Models\Survey;

class SurveyCreatedEvent extends Event
{

    public $survey;

    public function __construct(Survey $survey)
    {
        // dd($survey->g_service_type_id);
        $this->survey = $survey;
    }
}
