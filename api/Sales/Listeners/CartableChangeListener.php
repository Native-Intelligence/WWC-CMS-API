<?php

namespace Api\Sales\Listeners;

use Api\Cartables\Services\CartableService;
use Api\Cartables\Services\WorkflowService;
use Api\Sales\Services\SaleReqService;
use Api\Sales\Events\CartableChangeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CartableChangeListener implements ShouldQueue
{
    use InteractsWithQueue;

    private $_cartableService;
    private $_saleReqService;
    private $_workflowService;

    public function __construct(
        CartableService $cartableService,
        SaleReqService $saleReqService,
        WorkflowService $workflowService
    ) {
        $this->_cartableService = $cartableService;
        $this->_saleReqService = $saleReqService;
        $this->_workflowService = $workflowService;
    }

    public function handle(CartableChangeEvent $result)
    {
        $result = $result->result;
        if(isset($result->request_id) or isset($result->request_number)) {
            // dd($result->request_id);
            if(isset($result->request_id)) {
                $id = $result->request_id;
            }else{
                $id = $result->id;
            }
            
            if(isset($result->user_id)) {
                $userId = $result->user_id;
            }else{
                $userId = null;
            }

            // dd($result, $id);
            $cartable = $this->_cartableService->getByField(
                ['request_id' => $id, 'workflow_status' => 0]
            );
            if(is_null($cartable)) {
                // dd($cartable);
                // dd("AAAA");
                $request = $this->_saleReqService->getByField(['id' => $id]);
                // dd($request);
                $workflow = $this->_workflowService->getByField(['g_service_type_id' => $request->g_service_type_id]);
                // dd($workflow);
                $workflowDetails = $workflow->workflowDetails()->where('prev_id', null)->first();

                $this->_cartableService->create(
                    [
                    'workflow_detail_id' => $workflowDetails->id,
                    'request_id' => $id,
                    'scope' => $request->scope,
                    'status' => 1
                    ]
                );

                $workflowDetailsNext = $workflow->workflowDetails()
                    ->where('prev_id', $workflowDetails->step_id)
                    ->where('step_id', $workflowDetails->next_id)->first();

                $this->_cartableService->create(
                    [
                    'workflow_detail_id' => $workflowDetailsNext->id,
                    'request_id' => $id,
                    'scope' => $request->scope
                    ]
                );
            }else{
                $request = $this->_saleReqService->getByField(['id' => $id]);
                $workflow = $this->_workflowService->getByField(['g_service_type_id' => $request->g_service_type_id]);
                
                if(isset($result->success)) {
                    $nextId = ($result->success)? $cartable->next_id : $cartable->next_false_id;
                }else{
                    $nextId = $cartable->next_id;
                }
                
                $workflowDetailsNext = $workflow->workflowDetails()
                    ->where('prev_id', $cartable->step_id)
                    ->where('step_id', $nextId)->first();
                    
                $this->_cartableService->create(
                    [
                    'workflow_detail_id' => $workflowDetailsNext->id,
                    'request_id' => $cartable->request_id,
                    'scope' => $cartable->scope,
                    'user_id' => $userId
                    ]
                );

                $this->_cartableService->update(
                    ['id' => $cartable->id, 'status' => 1]
                );
            }
        }
    }
}
