<?php

namespace Api\Sales\Listeners;

// use Api\Acc\Services\TransactionService;
// use Api\Sales\Services\CalculationService;
use Api\Sales\Services\SaleReqService;
// use Api\Sales\Services\LandService;
use Api\Sales\Events\SurveyCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SurveyCreatedListener implements ShouldQueue
{
    use InteractsWithQueue;

    // private $_calculationService;
    // private $_landService;
    // private $_transaction;
    private $_saleReqService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        // CalculationService $calculationService,
        // LandService $landService,
        // TransactionService $transaction,
        SaleReqService $saleReqService
    ) {
        // $this->_calculationService = $calculationService;
        // $this->_landService = $landService;
        // $this->_transaction = $transaction;
        $this->_saleReqService = $saleReqService;
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function handle(SurveyCreatedEvent $survey)
    {
        // dd("Sssss");
        // dd($survey->survey);
        // if ($survey->survey->g_service_type_id == '0') {
        //     $calc = $this->_calculationService->createSurveyCalculation($survey->survey, 500);
        //     // dd($calc->billing_details);
        //     $this->_landService->update(
        //         [
        //         'id' => $survey->survey->request->land_id,
        //         'subscription_number' => $survey->survey->subscription_number,
        //         'file_number' => $survey->survey->file_number
        //         ]
        //     );
        //     //TODO:: create connection in settings for conected 2 or more app
        //     $this->_transaction->create(['details' => json_encode($calc->billing_details), 'type' => 500, 'scope' => $calc->scope]);
        // } else {
        //     $calc = $this->_calculationService->recalculate($survey->survey, 501);
        //     $this->_transaction->create(['details' => json_encode($calc->billing_details), 'type' => 501, 'scope' => $calc->scope]);
        // }
        $this->_saleReqService->update(
            [
            'id' => $survey->survey->request->id,
            'status' => 2 //surveyd
            ]
        );
    }
}
