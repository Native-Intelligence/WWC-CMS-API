<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Branch extends Model
{

    protected $table = 'sale_branches';

    protected $fillable = [
                            'use_file_number',
                            'residential_units',
                            'non_residential_units',
                            'empty_residential_units',
                            'mounted',
                            'land_id',
                            'g_branch_diameter_id',
                            'usage_id',
                            'g_tank_id',
                            'consumption_instruction',
                            'bill_name',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'scope'  => 'object'
    ];

    public function land()
    {
        return $this->belongsTo('Api\Sales\Models\land');
    }
}
