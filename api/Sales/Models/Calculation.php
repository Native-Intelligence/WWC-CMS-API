<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Calculation extends Model
{

    protected $table = 'sale_calculations';
    
    protected $view = 'sale_calculations_view';

    protected $fillable = [
                            // 'request_id',
                            'instruction_id',
                            'survey_id',
                            'file_number',
                            'bill_number',
                            'pay_number',
                            'total_price',
                            'counts_installments',
                            'customer_name',
                            'billing_details',
                            'installments_details',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'billing_details'  => 'object',
        'installments_details'  => 'object',
        'survey' => 'object'
    ];
    
    // protected $appends = [
    //                         'survey',
    //                     ];
    //
    // public function getSurveyAttribute()
    // {
    //     // return $this->survey()->with('request')->with('usage')->first();
    //     return $this->survey()->first();
    // }
    //
    //
    // public function survey()
    // {
    //     return $this->belongsTo('Api\Sales\Models\Survey', 'survey_id');
    // }
}
