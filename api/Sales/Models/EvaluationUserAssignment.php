<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class EvaluationUserAssignment extends Model
{

    protected $table = 'sale_evaluation_user';
    protected $view = 'sale_evaluation_user_view';
    

    protected $fillable = [
                            'request_id',
                            'user_id',
                            'scope',
                            'status'
                           ];
}
