<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class File extends Model
{

    protected $table = 'sale_files';
    protected $view = 'sale_files_view';

    protected $fillable = [
                            'survey_date',
                            'file_number',
                            'subscription_number',
                            'subscription_number_next',
                            'subscription_number_prev',
                            'coordination_date',
                            'request_id',
                            // 'user_id',
                            'instruction_id',
                            'usage_id',
                            // 'survey_detail',
                            'other_data',
                            'number_of_units',
                            'branch_type',
                            'land_id',
                            'full_name',
                            'last_survey_id',
                            'last_calculation_id',
                            'first_request_id',
                            'is_split',
                            'g_siphon_id',
                            'g_branch_diameter_id',
                            'g_type_installation_id',
                            'consumption_instruction',
                            'g_service_type_id',
                            'scope_city',
                            'scope',
                            'status'
                           ];

    protected $casts = [
        'other_data' => 'object',
        'number_of_units' => 'object',
        'pay' => 'float',
        'total_price' => 'float'
        
    ];
    
        // protected $hidden = [
        //                         'user'
        //                     ];
        //
        // protected $appends = [
        //                         'user_title',
        //                     ];

    // public function getUserTitleAttribute()
    // {
    //     return $this->user->full_name;
    // }
    
    // public function user()
    // {
    //     return $this->belongsTo('Api\Settings\Models\User');
    // }
    
    public function request()
    {
        return $this->belongsTo('Api\Sales\Models\SaleReq', 'request_id');
    }

    public function usage()
    {
        return $this->belongsTo('Api\Sales\Settings\Models\Usage');
    }
}
