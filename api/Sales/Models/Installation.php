<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Installation extends Model
{

    protected $table = 'sale_installations';

    protected $view = 'sale_installations_view';

    protected $fillable = [
                           'installation_figure',
                           'meter_body_number',
                           'installation_date',
                           'g_pipe_diameter_id',
                           'g_meter_installation_type_id',
                           'product_code',
                           'g_main_pipe_gender_id',
                           'g_subsidiary_pipe_gender_id',
                           'meter_sealed_number',
                           'excavation_length_added',
                           'meter_id',
                           'user_installer_id',
                           'user_supervisor_id',
                           'request_id',
                           'g_siphon_location_id',
                           'g_siphon_diameter_id',
                           'water_meter_number',
                           'land_distance',
                           'minutes_date' ,
                           'status',
                           'scope'
                          ];

    public function saleReq()
    {
        return $this->belongsTo('Api\Sales\Models\SaleReq');
    }

    public function user()
    {
        return $this->belongsTo('Api\Settings\Models\User');
    }

    public function meter()
    {
        return $this->belongsTo('Api\Sales\Settings\Models\Meter');
    }
}
