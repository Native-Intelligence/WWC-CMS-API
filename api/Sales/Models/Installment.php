<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Installment extends Model
{

    protected $table = 'sale_installments';
    protected $view = 'sale_installments_view';

    protected $fillable = [
                            'calculation_id',
                            'survey_id',
                            'end_date',
                            'start_date',
                            'account_id',
                            'file_number',
                            'bill_number',
                            'pay_number',
                            'amount',
                            'number_of_installment',
                            'count',
                            'customer_name',
                            'percent',
                            'status',
                            'scope'
                           ];
}
