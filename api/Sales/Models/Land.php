<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Land extends Model
{

    protected $table = 'sale_lands';

    protected $view = 'sale_lands_view';

    protected $fillable = [
                            'municipality_number',
                            'registration_number',
                            'postal_code',
                            'cadastre_code',
                            'file_number',
                            'subscription_number',
                            'address',
                            'coordination',
                            'customer_number',
                            'arena',
                            'foundation',
                            'usage_id',
                            'g_certificate_type_id',
                            'g_evidence_type_id',
                            'g_tank_id',
                            'g_land_type_id',
                            'persons_id',
                            'status',
                            'scope',
                            'scope_city'
                           ];

    protected $casts = [
        'coordination' => 'object',
        'persons_id' => 'object'
    ];
}
