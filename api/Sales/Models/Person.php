<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Person extends Model
{

    protected $table = 'sale_persons';

    protected $fillable = [
                            'full_name',
                            'contact_info',
                            'identity_number',
                            'person_type',
                            'status',
                            'scope',
                            'person_details'
                           ];

    protected $casts = [
        'person_details' => 'object',
        'contact_info' => 'object'
    ];

    public function land()
    {
        return $this->belongsToMany('Api\Sales\Models\Land', 'mid_land_persons', 'person_id', 'land_id')->withPivot('owner_relationship_applicant')->withTimestamps();
    }
}
