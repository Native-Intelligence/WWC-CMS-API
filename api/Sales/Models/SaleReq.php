<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class SaleReq extends Model
{

    protected $table = 'sale_requests';
    
    protected $view = 'sale_requests_view';

    protected $fillable = [
                            'request_number',
                            'request_type',
                            'customer_type',
                            'is_customer',
                            'g_service_type_id',
                            'branch_type',
                            'person_id',
                            'land_id',
                            'discount_id',
                            'file_number',
                            'request_details',
                            'status',
                            'scope'
                           ];

    protected $casts = [
        'discount_id' => 'object',
        'request_details' => 'object'
    ];

    protected $appends = [
                            'person_title'
                            // 'land_title'
                        ];

    public function getPersonTitleAttribute()
    {
        return $this->person->full_name;
    }
    
    // public function getLandTitleAttribute()
    // {
    //     return $this->land->postal_code;
    // }
    //
    // public function land()
    // {
    //     return $this->belongsTo('Api\Sales\Models\Land');
    // }

    public function discount()
    {
        return $this->belongsTo('Api\ingredients\Models\Discount');
    }
    
    public function person()
    {
        return $this->belongsTo('Api\Sales\Models\Person');
    }
}
