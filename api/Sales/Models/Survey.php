<?php

namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class Survey extends Model
{
    
    protected $table = 'sale_surveys';
    
    protected $view = 'sale_surveys_view';
    
    protected $fillable = [
        'survey_date',
        'coordination_date',
        'file_number',
        'subscription_number',
        'subscription_number_next',
        'subscription_number_prev',
        'request_id',
        'request_number',
        // 'instruction_id',
        // 'user_id',
        'usage_id',
        'other_data',
        'number_of_units',
        'is_split',
        'g_siphon_id',
        'g_branch_diameter_id',
        'g_type_installation_id',
        'consumption_instruction',
        'g_service_type_id',
        //'usage_title',
        'scope_city',
        'scope',
        'status'
    ];
    
    protected $casts = [
        'other_data'      => 'object',
        'number_of_units' => 'object',
    ];
    
    // protected $hidden = [
    //                         'user'
    //                     ];
    //
    protected $appends = [
        'survey_detail',
    ];
    
    public function getSurveyDetailAttribute()
    {
        $this->attributes['survey_detail']['other_data'] = isset($this->attributes['other_data']) ? $this->attributes['other_data'] : null;
        // var_dump($this->attributes['number_of_units']);
        $this->attributes['survey_detail']['number_of_units'] = (empty($this->attributes['number_of_units'])) ? json_decode([]) : json_decode($this->attributes['number_of_units']);
        $this->attributes['survey_detail']['g_branch_diameter_id'] = isset($this->attributes['g_branch_diameter_id']) ? $this->attributes['g_branch_diameter_id'] : null;
        $this->attributes['survey_detail']['g_siphon_id'] = isset($this->attributes['g_siphon_id']) ? $this->attributes['g_siphon_id'] : null;
        $this->attributes['survey_detail']['g_type_installation_id'] = $this->attributes['g_type_installation_id'];
        $this->attributes['survey_detail']['usage_title'] = isset($this->attributes['usage_title']) ? $this->attributes['usage_title'] : null;
        $this->attributes['survey_detail']['usage_id'] = isset($this->attributes['usage_id']) ? $this->attributes['usage_id'] : null;
        $this->attributes['survey_detail']['is_split'] = isset($this->attributes['is_split']) ? $this->attributes['is_split'] : null;
        $this->attributes['survey_detail']['usage_id'] = $this->attributes['usage_id'];
        return $this->attributes['survey_detail'];
    }
    //
    // public function user()
    // {
    //     return $this->belongsTo('Api\Settings\Models\User');
    // }
    
    public function request()
    {
        return $this->belongsTo('Api\Sales\Models\SaleReq', 'request_id');
    }
    
    // public function usage()
    // {
    //     return $this->belongsTo('Api\Sales\Settings\Models\Usage');
    // }
}
