<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class SurveyUserAssignment extends Model
{

    protected $table = 'sale_survey_user';
    protected $view = 'sale_survey_user_view';
    

    protected $fillable = [
                            'request_id',
                            'user_id',
                            'scope',
                            'status'
                           ];
}
