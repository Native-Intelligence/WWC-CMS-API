<?php
namespace Api\Sales\Models;

use Infrastructure\Database\Eloquent\Model;

class TechnicalEvaluation extends Model
{
    protected $table = 'sale_evaluations';
    protected $view = 'sale_evaluations_view';

    protected $fillable = [
    'request_id',
    'branch_info',
    'gis_info',
    'other_info',
    'review_date',
    'description',
    'status',
    'scope'
    ];
    
    protected $casts = [
    'branch_info' => 'object',
    'gis_info' => 'object',
    'other_info' => 'object',
    ];
}
