<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Branch;

class BranchRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Branch';
    }
}
