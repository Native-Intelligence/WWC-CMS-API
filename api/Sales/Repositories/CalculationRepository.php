<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Calculation;

class CalculationRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Calculation';
    }
}
