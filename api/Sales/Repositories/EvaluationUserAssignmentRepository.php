<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\EvaluationUserAssignment;

class EvaluationUserAssignmentRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\EvaluationUserAssignment';
    }
}
