<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\File;

class FileRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\File';
    }
}
