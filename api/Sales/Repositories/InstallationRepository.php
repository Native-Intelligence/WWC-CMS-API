<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Installation;

class InstallationRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Installation';
    }
}
