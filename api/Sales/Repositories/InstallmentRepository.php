<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Installment;

class InstallmentRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Installment';
    }
}
