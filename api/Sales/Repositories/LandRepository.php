<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Land;

class LandRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Land';
    }
}
