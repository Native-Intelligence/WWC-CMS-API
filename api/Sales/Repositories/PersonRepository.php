<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Person;

class PersonRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Person';
    }
}
