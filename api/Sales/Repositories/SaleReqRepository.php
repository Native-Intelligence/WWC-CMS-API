<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\SaleReq;

class SaleReqRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\SaleReq';
    }
}
