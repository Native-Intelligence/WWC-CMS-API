<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\Survey;

class SurveyRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\Survey';
    }
}
