<?php
namespace Api\Sales\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\SurveyUserAssignment;

class SurveyUserAssignmentRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\SurveyUserAssignment';
    }
}
