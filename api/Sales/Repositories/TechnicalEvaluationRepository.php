<?php
namespace Api\Sales\Repositories;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Models\TechnicalEvaluation;

class TechnicalEvaluationRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Models\\TechnicalEvaluation';
    }

}
