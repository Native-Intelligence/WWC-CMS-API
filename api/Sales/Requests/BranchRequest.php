<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class BranchRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('branch');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'use_file_number' => 'string',
            'residential_units' => 'required|integer',
            'non_residential_units' => 'required|integer',
            'empty_residential_units' => 'required|integer',
            'mounted' => 'required|integer',
            'land_id' => 'required|integer',
            'g_branch_diameter_id' => 'required|integer',
            'usage_id' => 'required|integer',
            'g_tank_id' => 'required|integer',
            'consumption_instruction' => 'required|numeric',
            'bill_name' => 'required|string',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'use_file_number' => 'string',
            'residential_units' => 'sometimes|required|integer',
            'non_residential_units' => 'sometimes|required|integer',
            'empty_residential_units' => 'sometimes|required|integer',
            'mounted' => 'sometimes|required|integer',
            'land_id' => 'sometimes|required|integer',
            'g_branch_diameter_id' => 'sometimes|required|integer',
            'usage_id' => 'sometimes|required|integer',
            'g_tank_id' => 'sometimes|required|integer',
            'consumption_instruction' => 'sometimes|required|numeric',
            'bill_name' => 'sometimes|required|string',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
