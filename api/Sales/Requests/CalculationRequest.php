<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class CalculationRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('calculation');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'survey_id' => 'required|integer',
            'instruction_id' => 'integer',
            'request_id' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'survey_id' => 'sometimes|required|inetegr',
            'instruction_id' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
