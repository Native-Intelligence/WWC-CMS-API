<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class FileRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('file');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'invoice_id' => 'integer|nullable',
            'survey_date' => 'required|date',
            'coordination_date' => 'required|date',
            'request_id' => 'required|integer',
            'user_id' => 'required|integer',
            'instruction_id' => 'integer|nullable',
            'usage_id' => 'required|integer',
            'file_number' => 'string',
            'subscription_number' => 'string',
            'subscription_number_next' => 'string',
            'subscription_number_prev' => 'string',
            'status' => 'integer',
            'number_of_units' => 'array',
            'discount_id' => 'array|nullable',
            'survey_detail' => 'array',
            'is_split' => 'boolean',
            'g_siphon_id' => 'integer',
            'residential_units_wa' => 'integer',
            'residential_units_sw' => 'integer',
            'none_residential_units_wa' => 'integer',
            'none_residential_units_sw' => 'integer',
            'none_residential_surface_units_sw' => 'integer',
            'residential_surface_units_sw' => 'integer',
            'g_branch_diameter_id' => 'integer',
            'g_type_installation_id' => 'integer',
            'consumption_instruction' => 'numeric',
            'g_service_type_id' => 'integer',
            'scope_city' => 'array|scope',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'invoice_id' => 'integer|nullable',
            'survey_date' => 'sometimes|required|date',
            'coordination_date' => 'sometimes|required|date',
            'request_id' => 'sometimes|required|integer',
            'user_id' => 'sometimes|required|integer',
            'instruction_id' => 'integer|nullable',
            'usage_id' => 'sometimes|required|integer',
            'file_number' => 'string',
            'subscription_number' => 'string',
            'subscription_number_next' => 'string',
            'subscription_number_prev' => 'string',
            'status' => 'integer',
            'number_of_units' => 'array',
            'discount_id' => 'array|nullable',
            'survey_detail' => 'array',
            'is_split' => 'boolean',
            'g_siphon_id' => 'integer',
            'residential_units_wa' => 'integer',
            'residential_units_sw' => 'integer',
            'none_residential_units_wa' => 'integer',
            'none_residential_units_sw' => 'integer',
            'none_residential_surface_units_sw' => 'integer',
            'residential_surface_units_sw' => 'integer',
            'g_branch_diameter_id' => 'integer',
            'g_type_installation_id' => 'integer',
            'consumption_instruction' => 'numeric',
            'g_service_type_id' => 'integer',
            'scope_city' => 'array|scope',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
