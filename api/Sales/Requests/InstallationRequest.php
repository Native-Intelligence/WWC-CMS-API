<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class InstallationRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('installation');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'installation_figure' => 'required|integer',
            'meter_body_number' => 'required|integer',
            'installation_date' => 'required|date',
            'g_pipe_diameter_id' => 'required|integer',
            'g_meter_installation_type_id' => 'required|integer',
            'product_code' => 'required|string',
            'g_main_pipe_gender_id' => 'required|integer',
            'g_subsidiary_pipe_gender_id' => 'required|integer',
            'meter_sealed_number' => 'required|integer',
            'excavation_length_added' => 'required|numeric',
            'meter_id' => 'required|integer',
            'user_installer_id' => 'required|integer',
            'user_supervisor_id' => 'required|integer',
            'request_id' => 'required|integer',
            'g_siphon_location_id' => 'required|integer',
            'g_siphon_diameter_id' => 'required|integer',
            'water_meter_number' => 'required|integer',
            'land_distance' => 'required|numeric',
            'minutes_date' => 'required|date',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'installation_figure' => 'sometimes|required|integer',
            'meter_body_number' => 'sometimes|required|integer',
            'installation_date' => 'sometimes|required|date',
            'g_pipe_diameter_id' => 'sometimes|required|integer',
            'g_meter_installation_type_id' => 'sometimes|required|integer',
            'product_code' => 'sometimes|required|string',
            'g_main_pipe_gender_id' => 'sometimes|required|integer',
            'g_subsidiary_pipe_gender_id' => 'sometimes|required|integer',
            'meter_sealed_number' => 'sometimes|required|integer',
            'excavation_length_added' => 'sometimes|required|numeric',
            'meter_id' => 'sometimes|required|integer',
            'user_installer_id' => 'sometimes|required|integer',
            'user_supervisor_id' => 'sometimes|required|integer',
            'request_id' => 'sometimes|required|integer',
            'g_siphon_location_id' => 'sometimes|required|integer',
            'g_siphon_diameter_id' => 'sometimes|required|integer',
            'water_meter_number' => 'sometimes|required|integer',
            'land_distance' => 'sometimes|required|numeric',
            'minutes_date' => 'sometimes|required|date',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
