<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class InstallmentRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('installment');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'calculation_id' => 'required|integer',
            'request_id' => 'integer',
            'installment_details' => 'array',
            'scope' => 'integer'
        ];

        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'calculation_id' => 'sometimes|required|integer',
            'installment_details' => 'array',
            'scope' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
