<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class LandRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('land');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'municipality_number' => 'required|string',
            'registration_number' => 'required|string',
            'postal_code' => 'required|string',
            'cadastre_code' => 'required|string',
            'file_number' => 'string',
            'address' => 'required|string',
            'coordination' => 'array',
            // 'customer_number' => 'integer',
            'arena' => 'required|numeric',
            'foundation' => 'required|numeric',
            'usage_id' => 'required|integer',
            'g_certificate_type_id' => 'integer',
            'g_evidence_type_id' => 'integer',
            'g_tank_id' => 'integer',
            'g_land_type_id' => 'integer',
            'persons_id' => 'required|array',
            'scope' => 'required|integer',
            'scope_city' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'municipality_number' => 'sometimes|required|string',
            'registration_number' => 'sometimes|required|string',
            'postal_code' => 'sometimes|required|string',
            'cadastre_code' => 'sometimes|required|string',
            'file_number' => 'string',
            'address' => 'sometimes|required|string',
            'coordination' => 'array',
            // 'customer_number' => 'integer',
            'arena' => 'sometimes|required|numeric',
            'foundation' => 'sometimes|required|numeric',
            'usage_id' => 'sometimes|required|integer',
            'g_certificate_type_id' => 'integer',
            'g_evidence_type_id' => 'integer',
            'g_tank_id' => 'integer',
            'g_land_type_id' => 'integer',
            'persons_id' => 'sometimes|required|array',
            'scope' => 'sometimes|required|integer',
            'scope_city' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
