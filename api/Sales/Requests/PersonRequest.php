<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class PersonRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('person');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'full_name' => 'required|string',
            'contact_info' => 'required|array',
            'identity_number' => 'required|numeric',
            'person_type' => 'required|integer',
            'scope' => 'required|integer',
            'person_details' => 'required|array'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'full_name' => 'sometimes|required|string',
            'contact_info' => 'sometimes|required|array',
            'identity_number' => 'sometimes|required|numeric',
            'person_type' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer',
            'person_details' => 'sometimes|required|array'
        ];
        $this->validate($this->data, $this->rules);
    }
}
