<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class SaleReqRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('request');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'request_number' => 'string',
            // 'request_date' => 'date',
            'request_type' => 'integer',
            'customer_type' => 'integer',
            'g_service_type_id' => 'required|integer',
            'land_id' => 'integer',
            //'branch_type' => 'required|integer',
            'person_id' => 'required|integer',
            //'land_id' => 'required|integer',
            'discount_id' => 'array|nullable',
            'scope' => 'required|integer',
            'file_number' => 'string',
            'description' => 'string',
            'request_details' => 'array',
            'status' => 'integer'
            
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'request_number' => 'string',
            'g_service_type_id' => 'sometimes|required|integer',
            // 'request_date' => 'date',
            'land_id' => 'integer',
            'customer_type' => 'integer',
            'request_type' => 'integer',
            'branch_type' => 'sometimes|required|integer',
            'person_id' => 'sometimes|required|integer',
            'land_id' => 'sometimes|required|integer',
            'discount_id' => 'array|nullable',
            'status' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer',
            'description' => 'string',
            'file_number' => 'string',
            'request_details' => 'array',
            'status' => 'integer'
            
            
        ];
        $this->validate($this->data, $this->rules);
    }
}
