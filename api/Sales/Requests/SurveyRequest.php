<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class SurveyRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('survey');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'request_id' => 'required|integer',
            'coordination_date' => 'date',
            'survey_date' => 'date',
            'number_of_units' => 'array',
            'g_branch_diameter_id' => 'integer|nullable',
            'g_type_installation_id' => 'integer',
            'g_siphon_id' => 'integer|nullable',
            
            //TODO: remove this
            // 'user_id' => 'required|integer',
            
            
            'invoice_id' => 'integer|nullable',
            'is_split' => 'boolean',
            'usage_id' => 'integer',
            
            
            
            'request_number' => 'string',
            // 'instruction_id' => 'integer|nullable',
            
            
            
            
            'file_number' => 'string',
            'subscription_number' => 'string',
            'subscription_number_next' => 'string',
            'subscription_number_prev' => 'string',
            'status' => 'integer',
            'discount_id' => 'array|nullable',
            'other_data' => 'array',
            'survey_detail' => 'array',
            'residential_units_wa' => 'integer',
            'residential_units_sw' => 'integer',
            'none_residential_units_wa' => 'integer',
            'none_residential_units_sw' => 'integer',
            'none_residential_surface_units_sw' => 'integer',
            'residential_surface_units_sw' => 'integer',
            'consumption_instruction' => 'numeric',
            'g_service_type_id' => 'integer',
            'scope_city' => 'integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'invoice_id' => 'integer|nullable',
            'survey_date' => 'date',
            'request_number' => 'sometimes|required|string',
            'coordination_date' => 'date',
            'request_id' => 'integer',
            // 'user_id' => 'sometimes|required|integer',
            // 'instruction_id' => 'integer|nullable',
            'subscription_number' => 'string',
            'subscription_number_next' => 'string',
            'subscription_number_prev' => 'string',
            
            'usage_id' => 'integer',
            'file_number' => 'string',
            'status' => 'integer',
            'number_of_units' => 'array',
            'discount_id' => 'array|nullable',
            'other_data' => 'array',
            'survey_detail' => 'array',
            'is_split' => 'boolean',
            'g_siphon_id' => 'integer|nullable',
            'residential_units_wa' => 'integer',
            'residential_units_sw' => 'integer',
            'none_residential_units_wa' => 'integer',
            'none_residential_units_sw' => 'integer',
            'none_residential_surface_units_sw' => 'integer',
            'residential_surface_units_sw' => 'integer',
            'g_branch_diameter_id' => 'integer|nullable',
            'g_service_type_id' => 'integer',
            'g_type_installation_id' => 'integer',
            'consumption_instruction' => 'numeric',
            'scope_city' => 'integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
