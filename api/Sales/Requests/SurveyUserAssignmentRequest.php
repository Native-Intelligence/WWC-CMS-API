<?php
namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class SurveyUserAssignmentRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('user');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'request_id' => 'required|integer',
            'user_id' => 'required|integer',
            'scope' => 'integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'request_id' => 'sometimes|required|integer',
            'user_id' => 'sometimes|required|integer',
            'scope' => 'integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
