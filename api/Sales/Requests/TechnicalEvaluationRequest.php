<?php

namespace Api\Sales\Requests;

use Infrastructure\Requests\Request;

class TechnicalEvaluationRequest extends Request
{

    public function __construct( Request $request ) 
    {
        $this->data = $request->json('evaluation');
    }

    public function createValidation() 
    {
        $this->rules = [
        'request_id'              => 'required|integer',
        'review_date'             => 'required|date',
        'branch_info'             => 'required|array',
        'gis_info'                => 'required|array',
        'other_info'              => 'required|array',
        'description'            => 'nullable|string',
        'status'                 => 'integer',
        'scope'                  => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation() 
    {
        $this->rules = [
        'id'                     => 'required|integer',
        'request_id'              => 'sometimes|required|integer',
        'review_date'             => 'sometimes|required|date',
        'branch_info'             => 'sometimes|required|array',
        'gis_info'                => 'sometimes|required|array',
        'other_info'              => 'sometimes|required|array',
        'description'            => 'nullable|string',
        'status'                 => 'integer',
        'scope'                  => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
