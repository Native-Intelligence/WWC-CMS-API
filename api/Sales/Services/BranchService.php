<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\BranchRepository;

class BranchService extends Service
{

    public function __construct(BranchRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'branch';
        $this->uniqueKey = 'use_file_number';
    }
}
