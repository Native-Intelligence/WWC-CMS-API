<?php

namespace Api\Sales\Services;

use Log;

use FormulaInterpreter\Compiler;
use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Services\InstructionService;
use Api\Sales\Settings\Repositories\InstructionDetailRepository;
use Api\Sales\Services\SurveyService;
use Api\Sales\Settings\Services\InvoiceService;
use Api\Sales\Settings\Services\InvoiceItemService;
use Api\Sales\Settings\Services\UsagePatternService;
use Api\Sales\Settings\Services\AreaService;
use Api\Sales\Settings\Services\FilterService;
use Api\Acc\Services\TransactionService;
use Api\Sales\Settings\Services\DiscountService;
use Api\Sales\Settings\Services\DiscountDetailService;
use Api\Sales\Services\FileService;
use Api\Sales\Repositories\CalculationRepository;
use Api\Acc\Settings\Services\ConfigService;

class CalculationService extends Service
{
    private $gVars;
    private $_checkPaid = [];
    private $_instructionService;
    private $_instructionDetailRepository;
    // private $_installmentService;
    private $_invoiceService;
    private $_transaction;
    private $_invoiceItemService;
    // private $_configService;
    private $_usagePatternService;
    private $_filterService;
    private $_fileService;
    private $_surveyService;
    private $_discountService;
    private $_discountDetailService;
    private $_areaService;
    private $_formulaInterpreter;
    private $_invoiceItemRepository;
    private $iiii = 0;
    
    public function __construct(
        CalculationRepository $repository,
        InstructionService $instructionService,
        InstructionDetailRepository $instructionDetailRepository,
        // InstallmentService $installmentService,
        InvoiceService $invoiceService,
        InvoiceItemService $invoiceItemService,
        UsagePatternService $usagePattern,
        AreaService $areaService,
        TransactionService $transaction,
        FilterService $filterService,
        FileService $fileService,
        SurveyService $surveyService,
        DiscountService $discountService,
        DiscountDetailService $discountDetailService,
        Compiler $formulaInterpreter,
        ConfigService $configService
    ) {
    
        $this->repository = $repository;
        $this->key = 'calculation';
        $this->uniqueKey = 'survey_id';
        
        $this->_instructionService = $instructionService;
        $this->_instructionDetailRepository = $instructionDetailRepository;
        // $this->_installmentService = $installmentService;
        $this->_invoiceService = $invoiceService;
        $this->_invoiceItemService = $invoiceItemService;
        $this->_usagePatternService = $usagePattern;
        $this->_areaService = $areaService;
        $this->_transaction = $transaction;
        $this->_filterService = $filterService;
        $this->_fileService = $fileService;
        $this->_surveyService = $surveyService;
        $this->_discountService = $discountService;
        $this->_discountDetailService = $discountDetailService;
        $this->_formulaInterpreter = $formulaInterpreter;
        
        // $this->_configService = $configService;
    }
    
    // public function getData($data)
    // {
    //     return $data;
    // }
    
    public function runMath($ast, $vars)
    {
        
        $ast = strtr(
            $ast, array_filter(
                $vars, function ($k) {
                    return is_string($k);
                }, ARRAY_FILTER_USE_KEY
            )
        );
        
        
        // foreach ($vars as $key => $value) {
        //     if (!is_string($key)) {
        //         unset($vars[$key]);
        //     }
        // }
        
        
        // $keys = array_map('strlen', array_keys($vars));
        // array_multisort($keys, SORT_DESC, $vars);
        //
        // foreach ($vars as $k => $v) {
        //     $ast = str_replace($k, $v, $ast, $c);
        // }
        // var_dump($ast);
        // dd($ast);
        
        try {
            $val = eval('return (' . $ast . ' );');
        } catch (\Error $e) {
            throw new Exception(
                50010, ['error' => [':formula' => $ast,
                                    ':vars'    => json_encode($this->gVars, JSON_FORCE_OBJECT)]]
            );
        }
        
        return $val;
    }
    
    // public function addDynamicUnits($consumption, $consumptionUnit, $residentialUnits, $unitsCount)
    public function addDynamicUnits($consumption, $usagePattern, $consumptionUnit, $residentialUnits)
    {
        $baseRate = [];
        // if (isset($numberOfUnit->consumption)) {
        if ($consumption > 0 and isset($consumptionUnit)) {
            // $baseRate['consumption'] = $consumption;
            $baseRate['consumption_unit'] = $consumptionUnit;
            $baseRate['consumption'] = $usagePattern;
            $baseRate['consumption_instruction'] = $consumption;
            
        } else {
            $baseRate['consumption'] = 0;
            $baseRate['consumption_unit'] = 0;
            $baseRate['consumption_instruction'] = 0;
        }
        // }
        $baseRate['residential_units'] = $residentialUnits;
        // $baseRate['residential_units_wa'] = $numberOfUnit->residential_units_wa;
        // $baseRate['residential_units_sw'] = $numberOfUnit->residential_units_sw - $numberOfUnit->residential_surface_units_sw;
        
        //TODO: what to o what not todo???
        // $baseRate['units_count'] = $unitsCount;
        
        return $baseRate;
    }
    
    public function calcInvoiceItem($formulaText, $baseRateVars)
    {
        if (!array_key_exists('haghab@arzeshtejariab', $baseRateVars)) {
            $baseRateVars['haghab@arzeshtejariab'] = 0;
        }
        
        return $this->runMath($formulaText, $baseRateVars);
    }
    
    // public function calcUnit($survey, $invoiceId = null, $usageId, nstruction, $type, $dynamicVars, $gTypeReq)
    public function calcUnit($survey, $usageId, $expenseId, $instructionId, $type, $dynamicVars, $numberOfUnit)
    {
        // if (empty($numberOfUnit->invoice_id)) {
        $invoice = null;
        if (!is_null($expenseId)) {
            $invoice = $this->_invoiceService->getByField(
                [
                    'instruction_id' => $instructionId,
                    'usage_id'       => $usageId,
                    'expense_id'     => $expenseId,
                    'type'           => $type
                ]
            );
        }
        if (is_null($expenseId) or is_null($invoice)) {
            $invoice = $this->_invoiceService->getByField(
                [
                    'instruction_id' => $instructionId,
                    'usage_id'       => $usageId,
                    'type'           => $type
                ]
            );
        }
        // dd($invoice);
        // } else {
        //     $invoice = $this->_invoiceService->find($numberOfUnit->invoice_id);
        // }
        
        if (empty($invoice)) {
            throw new Exception(40041);
        }
        // dd("sss");
        // dd($invoice->invoice_items);
        $this->iiii++;
        
        $baseRateVars = $this->calcBaseRateOnUnit(
            $instructionId, $survey, $invoice->invoice_items, $numberOfUnit, $type
        ) + $dynamicVars;
        
        // if($i==2) {
        //     dd($baseRateVars);
        // }
        // $baseRateVars = $baseRate + $dynamicVars;
        // Log::debug(json_encode($baseRate));
        // Log::debug(json_encode($vars));
        
        $invoiceItemSlugs = [];
        $accountSlugs = [];
        $invoiceItemNames = [];
        // $config = $this->_configService->getByField(
        //     ['g_document_type_id' => $gTypeReq]
        // );
        // dd($invoice->invoice_items_id);
        $b = 0;
        foreach ((array)$invoice->invoice_items as $v) {
            $b++;
            $invoiceItem = $this->_invoiceItemService->find($v->id);
            // var_dump($invoiceItem);
            // if($this->iiii > 2 and $b>5) {
            //     // dd($this->iiii);
            //     dd($invoiceItem->slug, $this->_checkPaid);
            // }
            if (!in_array($invoiceItem->slug, $this->_checkPaid)) {
                $invoiceItemSlugs[$invoiceItem->slug] = $this->calcInvoiceItem(
                    $invoiceItem->formula_value, $baseRateVars
                );
            }
            
            if ($invoiceItem->check_paid and !in_array($invoiceItem->slug, $this->_checkPaid)) {
                $this->_checkPaid[] = $invoiceItem->slug;
            }
            // $accountConfig = $config->details->$invoiceItem->slug;
            // $accountSlugs[$invoiceItem->slug] = $accountConfig->credit->account->code;
            $invoiceItemNames[$invoiceItem->slug] = $invoiceItem->title;
        }
        
        return [$invoiceItemSlugs, $accountSlugs, $invoiceItemNames];
    }
    
    public function calcBaseRateOnUnit($instructionId, $survey, array $invoiceItemsId, $numberOfUnit, $type)
    {
        $baseRate = [];
        $branch = isset($survey->g_branch_diameter_id) ? 'g_branch_diameter_id, ' . $survey->g_branch_diameter_id . ') = ' . $survey->g_branch_diameter_id : 'g_siphon_id, ' . $survey->g_siphon_id . ') = ' . $survey->g_siphon_id;
        // dd($survey);
        $baseRateVals = \DB::select(
            \DB::raw(
                '
                SET NOCOUNT ON;
                SELECT
                A.id, A.slug, A.value, C.value AS scope, D.value AS scope_city
                INTO #T
                FROM ing_instruction_details A
                CROSS APPLY OPENJSON(A.usage_id) WITH (value int \'$\') B
                CROSS APPLY OPENJSON(A.scopes) WITH (value bigint \'$\') C
                CROSS APPLY OPENJSON(A.scope_cities) WITH (value bigint \'$\') D
                OUTER APPLY OPENJSON(A.g_tank_id) WITH (value int \'$\') E
                WHERE A.instruction_id = ' . $instructionId . '
                AND ISNULL(A.' . $branch . '
                AND ISNULL(A.g_type_installation_id, ' . $survey->g_type_installation_id . ') = ' . $survey->g_type_installation_id . '
                AND ISNULL(E.value,  ' . $survey->g_tank_id . ') =  ' . $survey->g_tank_id . '
                AND B.value = ' . $numberOfUnit->usage->id . '
                AND C.value <= ' . $survey->scope . '
                AND D.value <= ' . $survey->scope_city . '
                AND A.invoice_item_id in (' .
                implode(
                    ',',
                    array_map(
                        function ($v) {
                            return $v->id;
                        },
                        $invoiceItemsId
                    )
                ) . ')

                SELECT T.* FROM #T T
                INNER JOIN (
                    SELECT slug, MAX(scope) AS scope, MAX(scope_city) AS scope_city FROM #T
                    GROUP BY slug
                ) AS TT
                ON T.slug = TT.slug
                AND T.scope = TT.scope
                AND T.scope_city = TT.scope_city

                DROP TABLE #T'
            )
        );
        
        $this->gVars = ['instruction_id'         => $instructionId,
                        'g_branch_diameter_id'   => $survey->g_branch_diameter_id,
                        'g_type_installation_id' => $survey->g_type_installation_id,
                        'scope_city'             => $survey->scope_city,
                        'scope'                  => $survey->scope,
                        'g_tank_id'              => $survey->g_tank_id,
                        'usage_id'               => $numberOfUnit->usage->id,
        ];
        
        foreach ($baseRateVals as $v) {
            $baseRate[$v->slug] = $v->value;
        }
        
        return $baseRate;
    }
    
    public function calcFactorsOnUnit($factors, $usageId, $scope = 1, $scopeCity = 1)
    {
        $factorVals = [];
        foreach ((array)$factors as $v) {
            
            if ($v->usage_id == $usageId
                and $v->scope <= $scope
                and $v->scope_city <= $scopeCity
            ) {
                $factorVals[$v->slug] = $v->value;
            }
            
            
        }
        // dd($factorVals, $usageId, $scope, $scopeCity);
        return $factorVals;
    }
    
    public function mergeSumValue($base, $newArr)
    {
        foreach ($newArr as $k => $v) {
            if (array_key_exists($k, $base)) {
                $base[$k] += $v;
            } else {
                $base[$k] = $v;
            }
        }
        
        return $base;
    }
    
    public function calcConsumInstruction($units)
    {
        $vals = [];
        foreach ($units as $numberOfUnit) {
            $vals[] = $numberOfUnit->consumption * $numberOfUnit->usage_pattern->value;
        }
        
        return ceil(array_sum($vals) * 1.05);
    }
    
    // public function calcDiscount($discountId, $numberOfDiscount, $vars, $i)
    public function calcDiscount($discountId, $numberOfDiscount, $vars)
    {
        $invoiceItemPriceWithDiscount = [];
        $discountDetails = $this->_discountDetailService->getAllByField([['discount_id', '=', $discountId]]);
        
        // if($i==2) {
        //     dd($discountDetails);
        // }
        
        foreach ($discountDetails as $detail) {
            $val = $this->runMath($detail->formula_value, $vars);
            $invoiceItemPriceWithDiscount[$detail->invoice_item_slug] = ceil($val * ($detail->pecuniary_percent / 100)) * $numberOfDiscount;
        }
        
        return $invoiceItemPriceWithDiscount;
    }
    
    public function billNumberGenerator($fileNumber, $code)
    {
        //TODO company code changed to account in accounts
        $billNumber = $fileNumber . $code . '1';
        // $billNumber = $fileNumber.$this->_areaService->find($id)->company_code.'1';
        $billNumber .= $this->controlDigitGenerator($billNumber);
        return $billNumber;
    }
    
    public function controlDigitGenerator($number)
    {
        $factorVar = 2;
        $result = 0;
        foreach (array_reverse(str_split($number)) as $val) {
            $result += (integer)$val * $factorVar;
            $factorVar++;
            if ($factorVar > 7) {
                $factorVar = 2;
            }
            
            // code...
        }
        $result %= 11;
        if ($result == 0 or $result == 1) {
            $controlDigit = $result;
        } else {
            $controlDigit = $result - 11;
        }
        
        return abs($controlDigit);
    }
    
    public function payNumebrGenerator($totalPrice = 0, $totalPriceWithDiscount = 0, $billNumber, $year, $period = '00')
    {
        // $totalPay = floor(($totalPrice - $totalPriceWithDiscount)/1000);
        // $length = strlen($totalPay);
        // substr_replace('00000000', $totalPay, 8-$length)
        $payNumebr = floor(($totalPrice - $totalPriceWithDiscount) / 1000) . substr($year, 1, 1) . $period;
        // dd($payNumebr);
        $payNumebr .= $this->controlDigitGenerator($payNumebr);
        $payNumebr .= $this->controlDigitGenerator($billNumber . $payNumebr);
        return $payNumebr;
    }
    
    public function calculate($survey, $instruction, $details = [])
    {
        // $survey = $this->_surveyService->find($surveyId);
        // dd($survey->id);
        if (empty($instruction)) {
            throw new Exception(40040);
        }
        
        $invoiceItemPrice = [];
        $accountSlugs = [];
        $invoiceItemNames = [];
        $invoiceItemPriceWithDiscount = [];
        $discountTypes = [];
        $consIns = [];
        
        // $unitsCount = count((array) $survey->number_of_units);
        // dd($survey->number_of_units);
        // $i=0;
        foreach ((array)$survey->number_of_units as $numberOfUnit) {
            // $i++;
            $dynamicVars = $this->calcFactorsOnUnit(
                $instruction->factors,
                $numberOfUnit->usage->id,
                $survey->scope,
                $survey->scope_city
            );
            // dd($instruction->factors);
            
            $dynamicVars += $this->addDynamicUnits(
                (isset($numberOfUnit->consumption)) ? $numberOfUnit->consumption : null,
                (isset($numberOfUnit->usage_pattern->value)) ? $numberOfUnit->usage_pattern->value : null,
                $numberOfUnit->consumption_unit,
                $numberOfUnit->residential_units
                // $unitsCount
            );
            // dd($dynamicVars);
            $consIns[] = $dynamicVars['consumption_instruction'];
            // var_dump($numberOfUnit->residential_units);
            if ($numberOfUnit->residential_units > 0 and $numberOfUnit->type != 2) {
                
                list($_invoiceItemSlugs, $_accountSlugs, $_invoiceItemNames) = $this->calcUnit(
                    $survey,
                    $numberOfUnit->usage->id,
                    (isset($numberOfUnit->expense->id)) ? $numberOfUnit->expense->id : null,
                    $instruction->id,
                    ($numberOfUnit->type == 2) ? 1 : $numberOfUnit->type,
                    $dynamicVars,
                    $numberOfUnit
                );
                
                $invoiceItemPrice = $this->mergeSumValue($invoiceItemPrice, $_invoiceItemSlugs);
                $dynamicVars = array_merge($_invoiceItemSlugs, $dynamicVars);
                $accountSlugs = array_merge($accountSlugs, $_accountSlugs);
                $invoiceItemNames = array_merge($invoiceItemNames, $_invoiceItemNames);
                
                
                // if ($numberOfUnit->residential_units_sw > 0) {
                //     $v = $this->calcUnit($survey, $numberOfUnit, $instruction, 1, $vars, $type);
                //     $invoiceItemPrice = $this->mergeSumValue($invoiceItemPrice, $v[0]);
                //     $vars = array_merge($v[0], $vars);
                //     $accountSlugs = array_merge($accountSlugs, $v[1]);
                //     $invoiceItemNames = array_merge($invoiceItemNames, $v[2]);
                // }
                
                // $discountDetails = (array) $numberOfUnit->discounts; // [{id: , count:}]
                if (isset($numberOfUnit->discounts)) {
                    foreach ((array)$numberOfUnit->discounts as $discountVal) {
                        $discount = $this->_discountService->find($discountVal->discount->id);
                        $discoutPrices = $this->calcDiscount($discount->id, $discountVal->count, $dynamicVars);
                        $discountTypes[$discount->g_discount_type_id] = $discountVal->count;
                        $invoiceItemPriceWithDiscount = $this->mergeSumValue($invoiceItemPriceWithDiscount, $discoutPrices);
                    }
                }
            }
        }
        
        $totalPrice = array_sum($invoiceItemPrice);
        $totalPriceWithDiscount = array_sum($invoiceItemPriceWithDiscount);
        // $totalPrice = $this->_formulaInterpreter->compile($invoice->formula->filter)->run($invoiceItemPrice);
        // $totalPrice = ($totalPrice * $survey->residential_units_wa) + ($totalPrice *$survey->residential_units_sw);
        
        $consumptionInstruction = ceil(array_sum($consIns) * 1.05);
        $survey->consumptionInstruction = $consumptionInstruction;
        
        $data = [
            'request_id'           => $survey->request_id,
            'instruction_id'       => $instruction->id,
            'survey_id'            => $details['survey_id'],
            'total_price'          => $totalPrice,
            'counts_installments'  => 0,
            'scope'                => $survey->scope,
            'customer_name'        => $details['customer_name'],
            'file_number'          => $survey->file_number,
            'billing_details'      => [
                'bill_number'                      => $billNumber = $this->billNumberGenerator($survey->file_number, substr($survey->scope, 3, 3)),
                'pay_number'                       => $this->payNumebrGenerator($totalPrice, $totalPriceWithDiscount, $billNumber, jdate()->format('y')),
                'survey'                           => $survey,
                'current_survey'                   => $details['current_survey'],
                'subscription_number'              => $survey->subscription_number,
                'consumption_instruction'          => $details['customer_name'],
                'file_number'                      => $survey->file_number,
                'special_type'                     => 0,
                'quota_type'                       => 0,
                'quota_count'                      => 0,
                'superstructure'                   => 120,
                'total_price'                      => $totalPrice,
                'total_price_with_discount'        => $totalPriceWithDiscount,
                'invoice_item_price'               => $invoiceItemPrice,
                'account_slugs'                    => $accountSlugs,
                'invoice_item_names'               => $invoiceItemNames,
                'invoice_item_price_with_discount' => $invoiceItemPriceWithDiscount,
                'branch_type'                      => $details['branch_type'],
                'request_number'                   => $details['request_number'],
                'g_service_type_id'                => $details['g_service_type_id'],
                'identity_number'                  => $details['identity_number'],
                'phone_number'                     => $details['phone_number'],
                'postal_code'                      => $details['postal_code'],
                'current_usage'                    => $survey->usage_title,
                'previous_usage'                   => $details['previous_usage'],
                'land'                             => $details['land'],
                'discount_types'                   => $discountTypes
            ],
            'installments_details' => []
        ];
        // dd($data);
        return $data;
    }
    
    public function create($data)
    {
        $this->data = (object)$this->getData($data);
        
        $survey = $this->_surveyService->find($this->data->survey_id);
        if (isset($this->data->instruction_id)) {
            $instruction = $this->_instructionService->find($this->data->instruction_id);
        } else {
            $instruction = $this->_instructionService->getLastItem();
        }
        
        $details['survey_id'] = $survey->id;
        
        if ($survey->g_service_type_id == 571) {
            $details['branch_type'] = $survey->branch_type;
            $details['current_survey'] = null;
            $details['land'] = $survey->land_id;
            $details['previous_usage'] = $survey->usage_title;
            
            $details['customer_name'] = $survey->request->person->full_name;
            $details['request_number'] = $survey->request->id;
            $details['g_service_type_id'] = $survey->request->g_service_type_id;
            $details['identity_number'] = $survey->request->person->identity_number;
            $details['phone_number'] = $survey->request->person->phone;
            $details['postal_code'] = $survey->request->person->identity_number;
            
            
            $data = $this->calculate($survey, $instruction, $details);
        } elseif ($survey->g_service_type_id == 194) {
            $file = $this->_fileService->getByField(['file_number' => $survey->file_number]);
            // dd($file);
            
            $details['branch_type'] = $file->branch_type;
            $details['current_survey'] = $file;
            $details['land'] = $file->land_id;
            $details['previous_usage'] = $file->usage_title;
            
            
            $details['customer_name'] = $file->full_name;
            $details['request_number'] = null;
            $details['g_service_type_id'] = $file->g_service_type_id;
            $details['identity_number'] = null;
            $details['phone_number'] = null;
            $details['postal_code'] = null;
            
            
            $previousSaleCalculate = $this->calculate($file, $instruction, $details);
            
            $details['branch_type'] = $survey->branch_type;
            $details['land'] = $survey->land_id;
            $details['previous_usage'] = $survey->usage_title;
            
            $details['customer_name'] = $survey->request->person->full_name;
            $details['request_number'] = $survey->request->id;
            $details['g_service_type_id'] = $survey->request->g_service_type_id;
            $details['identity_number'] = $survey->request->person->identity_number;
            $details['phone_number'] = $survey->request->person->phone;
            $details['postal_code'] = $survey->request->person->identity_number;
            $this->_checkPaid = [];
            $currentSaleCalculate = $this->calculate($survey, $instruction, $details);
            
            $data = $this->calcDiff($previousSaleCalculate, $currentSaleCalculate);
            $data['request_id'] = $survey['request_id'];
            
            if ($currentSaleCalculate['billing_details']['survey']['g_branch_diameter_id'] == $previousSaleCalculate['billing_details']['survey']['g_branch_diameter_id']
            ) {
                unset($data['billing_details']['invoice_item_price']['dastmozdnasbab']);
                unset($data['billing_details']['invoice_item_price']['lavazemnasbab']);
            } else {
                $data['billing_details']['invoice_item_price']['dastmozdnasbab'] = $currentSaleCalculate['billing_details']['invoice_item_price']['dastmozdnasbab'];
                $data['billing_details']['invoice_item_price']['lavazemnasbab'] = $currentSaleCalculate['billing_details']['invoice_item_price']['lavazemnasbab'];
            }
            // dd($currentSaleCalculate['billing_details']['invoice_item_price']);
            if ($currentSaleCalculate['billing_details']['survey']['g_siphon_id'] == $previousSaleCalculate['billing_details']['survey']['g_siphon_id']
            ) {
                unset($data['billing_details']['invoice_item_price']['nasbfazelab']);
                unset($data['billing_details']['invoice_item_price']['haffari']);
            } else {
                $data['billing_details']['invoice_item_price']['nasbfazelab'] = $currentSaleCalculate['billing_details']['invoice_item_price']['nasbfazelab'];
                $data['billing_details']['invoice_item_price']['haffari'] = $currentSaleCalculate['billing_details']['invoice_item_price']['haffari'];
            }
            
        
            if(!empty($currentSaleCalculate['billing_details']['survey']['g_siphon_id'])) {
                unset($data['billing_details']['invoice_item_price']['tabsare2']);
            }

            $data['billing_details']['total_price'] = array_sum($data['billing_details']['invoice_item_price']);
        }
        
        // dd($data);
        $result = parent::create($data);
        
        $this->_surveyService->update(['id' => $survey->id, 'status' => 3]);
        
        $this->_transaction->create(
            [
                'details' => json_encode($data['billing_details']),
                'type'    => $survey->g_service_type_id,
                'scope'   => $data['scope']
            ]
        );
        
        return $result;
    }
    
    ///////////////////////////////////////////////////////////////////////
    
    // public function recalculate($newSurvey)
    // {
    //     $file = $this->_fileService->getByFieldWithRelations(['file_number' => $newSurvey->file_number], ['request', 'usage']);
    //
    //     $fake = [
    //     'current_survey' => $file,
    //     'survey_id' => $newSurvey->id,
    //     'branch_type' => $file->request->branch_type,
    //     'previous_usage' => $file->usage->title,
    //     'land' => $file->land_id,
    //     ];
    //
    //     $previousSaleCalculate = $this->calculate($file, $fake);
    //     $currentSaleCalculate = $this->calculate($newSurvey, $fake);
    //
    //     $newDiff = $this->calcDiff($previousSaleCalculate, $currentSaleCalculate);
    //     $newDiff['request_id'] = $newSurvey['request_id'];
    //
    //     if ($file->g_branch_diameter_id == $previousSaleCalculate['billing_details']['survey']['g_branch_diameter_id']) {
    //         unset($newDiff['billing_details']['invoice_item_price']['dastmozdnasbab']);
    //         unset($newDiff['billing_details']['invoice_item_price']['lavazemnasbab']);
    //     } else {
    //         $newDiff['billing_details']['invoice_item_price']['dastmozdnasbab'] = $currentSaleCalculate['billing_details']['invoice_item_price']['dastmozdnasbab'];
    //         $newDiff['billing_details']['invoice_item_price']['lavazemnasbab'] = $currentSaleCalculate['billing_details']['invoice_item_price']['lavazemnasbab'];
    //     }
    //
    //         $newDiff['billing_details']['total_price'] = array_sum($newDiff['billing_details']['invoice_item_price']);
    //         return $this->create($newDiff);
    // }
    
    // public function getData($data)
    // {
    //     return $data;
    // }
    
    public function calcDiff($prev, $current)
    {
        $prevPrices = $prev['billing_details']['invoice_item_price'];
        $prevPricesDiscounts = $prev['billing_details']['invoice_item_price_with_discount'];
        
        $shit = [];
        
        /*        foreach($prevPrices as $slug => $value ) {
        if (array_key_exists($slug, $prevPricesDiscounts))
        $shit[$slug] = $value - $prevPricesDiscounts[$slug];
        else
        $shit[$slug] = $value;
        }
        */
        
        $currentPrices = $current['billing_details']['invoice_item_price'];
        $currentDiscount = $current['billing_details']['invoice_item_price_with_discount'];
        
        $newPrices = [];
        $newDiscount = [];
        
        
        //	$newPrices['haghab'] = 0;
        foreach ($currentPrices as $slug => $value) {
            if (array_key_exists($slug, $prevPrices)) {
                //if(array_key_exists($slug, $currentDiscount))
                //$value = $value - $currentDiscount[$slug];
                
                $dval = $value - $prevPrices[$slug];
                
                /*                if ($dval < 0 && $slug != 'haghabmaskoni' && $slug != 'haghabgheiremaskoni')
                $newPrices[$slug] = 0;
                else
                $newPrices[$slug] = $dval;*/
            } else {
                $dval = $value;
            }
            $key = explode('@', $slug);
            
            if (!array_key_exists($key[0], $newPrices)) {
                $newPrices[$key[0]] = $dval;
            } else {
                $newPrices[$key[0]] += $dval;
            }
            
            if (array_key_exists($slug, $currentDiscount)) {
                $newDiscount[$key[0]] = $currentDiscount[$slug];
            }
        }
        
        foreach ($newPrices as $k => $newPrice) {
            switch ($k) {
            case 'haghab':
                if ($newPrice < 0) {
                    $newPrices[$k] = $newPrices[$k] * (30 / 100);
                }
                break;
            default:
                $key = explode('@', $k);
                // if ($newPrice < 0 and count($key) > 1) {
                if ($newPrice < 0) {
                    $newPrices[$k] = 0;
                }
                break;
            }
        }
        
        /*       if (array_key_exists('haghabgheiremaskoni', $newPrices )){
        $newPrices['haghab'] += $newPrices['haghabgheiremaskoni'];
        if($newPrices['haghabgheiremaskoni'] < 0)
        $newPrices['haghabgheiremaskoni'] = $newPrices['haghabgheiremaskoni'] * (30 / 100);
        }

        if (array_key_exists('haghabmaskoni', $newPrices )){
        $newPrices['haghab'] += $newPrices['haghabmaskoni'];
        if($newPrices['haghabmaskoni'] < 0)
        $newPrices['haghabmaskoni'] = $newPrices['haghabmaskoni'] * (30 / 100);
        }

        if(array_key_exists('arzeshtejariab', $newPrices))
        $newPrices['haghab'] += $newPrices['arzeshtejariab'];

        if($newPrices['haghab'] < 0)
        $newPrices['haghab'] =  $newPrices['haghab'] * (30/100);  */
        //dd($newPrices['haghab']);
        $current['billing_details']['invoice_item_price'] = $newPrices;
        $current['billing_details']['invoice_item_price_with_discount'] = $newDiscount;
        
        
        return $current;
    }
}
