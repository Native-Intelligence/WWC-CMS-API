<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\EvaluationUserAssignmentRepository;

class EvaluationUserAssignmentService extends Service
{

    public function __construct(EvaluationUserAssignmentRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'user';
        $this->uniqueKey = '';
    }
}
