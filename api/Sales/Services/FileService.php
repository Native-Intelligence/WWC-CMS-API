<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\FileRepository;

class FileService extends Service
{

    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'file';
        $this->uniqueKey = 'file_number';
        // $t0
    }

    // public function getData($data)
    // {
    //     switch (gettype($data)) {
    //     case 'object':
    //         $data = (object) $data->json($this->key);
    //         break;
    //     case 'array':
    //         $data = (object) $data;
    //         break;
    //     }
    //     // $data = (object) $data->json($this->key);
    //     $data->file_number = $this->setFileNumber();
    //     $data->subscription_number = $this->setSubscriptionNumber();
    //     return $data;
    // }
    //
    // public function setFileNumber()
    // {
    //     return rand(1e7, 99999999);
    // }
    //
    // public function setSubscriptionNumber()
    // {
    //     return 5321;
    // }
}
