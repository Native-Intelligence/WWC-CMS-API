<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\InstallmentRepository;
use Api\Sales\Services\CalculationService;
use Api\Acc\Settings\Services\ConfigService;
use Api\Sales\Settings\Services\InvoiceItemService;


class InstallmentService extends Service
{
    private $_calculationService;
    private $_configService;
    private $_invoiceItemService;
    
    public function __construct(
        InstallmentRepository $repository,
        CalculationService $calculationService,
        InvoiceItemService $invoiceItemService,
        ConfigService $configService
    ) {
        $this->repository = $repository;
        $this->key = 'installment';
        $this->uniqueKey = 'bill_number&pay_number';
        $this->_calculationService = $calculationService;
        $this->_invoiceItemService = $invoiceItemService;
        $this->_configService = $configService;
    }

    public function updateInstallment($data)
    {
        $this->data = (object) $this->getData($data);
                
        $oldInstallments = $this->getAllByField(
            [
            ['survey_id', '=', $this->data->survey_id],
            ['calculation_id', '=', $this->data->id]
            ]
        );
        // dd($oldInstallments->toArray()['data']);
        // dd(['survey_id'=>$this->data->survey_id, 'calculation_id'=>$this->data->id]);
        // dd($oldInstallments->toArray()['data']);
        foreach ($oldInstallments->toArray()['data'] as $value) {
            $this->destroy($value['id']);
        }
        // dd($this->data)
        // $result = $this->calcInstallments($data);

        // return true;
    }

    public function calcInstallments($data)
    {
        // dd("Sss");
        $data = (object) $data->json($this->key);
        
        $calculation = $this->_calculationService->find($data->calculation_id);

        $invoiceItemPrice = (array) $calculation->billing_details->invoice_item_price;
        $invoiceItemPriceWithDiscount = (array) $calculation->billing_details->invoice_item_price_with_discount;

        // if($calculation->billing_details->g_service_type_id == 0) {
        $config = $this->_configService->getByField(
            ['g_service_type_id' => $calculation->billing_details->g_service_type_id]
        );
        // }else{
        //     $config = $this->_configService->getByField(
        //         ['g_document_type_id' => '501']
        //     );
        // }

        foreach ($invoiceItemPrice as $slug => $val) {
            if (array_key_exists($slug, $invoiceItemPriceWithDiscount)) {
                $invoiceItemPrice[$slug] -= $invoiceItemPriceWithDiscount[$slug];
            }
            
            $accountSlugs[$slug] = $config->details->$slug->bank_account->company_code;
            $companyCodeAccountId[$config->details->$slug->bank_account->company_code] = $config->details->$slug->bank_account->id;
        }
        

        // $accountSlugs = $accountConfig;
        // $accountSlugs = $calculation->billing_details->account_slugs;
        // dd($accountSlugs, $accountConfig);
        $installmentPolicy = [];
        $filterInstallmentsOn = [];
        // if (!empty($data)) {
        // isset($data->installment_details) and $installmentPercents = $data->installment_details;

        if (isset($data->installment_details)) {
            foreach ($data->installment_details as $slug => $value) {
                $installmentPolicy[$value['invoice_item']] = [
                $value['percent'],
                $value['count'],
                $value['count'],
                ];

                $filterInstallmentsOn[$value['invoice_item']] = [
                'percent' => $value['percent'],
                'count' => $value['count'],
                'start_date' => strtotime('today'),
                'end_date'   => strtotime('today +'. $value['count'] .' month')
                ];
            }
            // dd($installmentPolicy, $filterInstallmentsOn);
        } else {
            foreach ($invoiceItemPrice as $slug => $value) {
                $installmentPolicy[$slug] = [
                0,
                0,
                0,
                ];
                
                $filterInstallmentsOn[$slug] = [
                'percent' => 0,
                'count' => 0,
                'start_date' => strtotime('today'),
                'end_date'   => strtotime('today +'. 10 .' day')
                ];
            
            
                // $invoiceItems = $this->_invoiceItemService->getByField(['slug', 'IN' , array_keys($invoiceItemPrice)]);
                //
                // foreach ($invoiceItems as $invoice) {
                //     if ($invoice->is_partition == 1) {
                //         $installmentPolicy[$invoice->slug] = [
                //         $invoice->partition_percent,
                //         $invoice->partition_count,
                //         $invoice->partition_count,
                //         ];
                //
                //         $filterInstallmentsOn[$invoice->slug] = [
                //         'percent' => $invoice->partition_percent,
                //         'count' => $invoice->partition_count,
                //         'start_date' => strtotime('today'),
                //         'end_date' => strtotime('today +'. $invoice->partition_count .' month')
                //         ];
                //     }
                // }
            }
        }
            // $v = $this->separateInstallments($invoiceItemPrice, $installmentPolicy);
            
            // $fixedValues = $v[0];
            // $installmentSupportedOnes = $v[1];
            
            // dd($installmentPolicy, $filterInstallmentsOn);
            list(
            $fixedValues,
            $installmentSupportedOnes
            ) = $this->separateInstallments($invoiceItemPrice, $installmentPolicy);

            $result = [];
            $result[] = $this->organize($accountSlugs, $fixedValues);
            $installments = $this->calcInstallment($installmentSupportedOnes, $installmentPolicy);
        foreach ($installments as $installment) {
            $result[] = $this->organize($accountSlugs, $installment);
        }
            $this->updateInstallment(
                [
                'id' => $calculation->id,
                'survey_id' => $calculation->survey_id,
                'installments_details' => [
                'data' => $result,
                'details' => $filterInstallmentsOn
                ],
                ]
            );
            // dd($result);
            // dd($result);
            $filterInstallmentsOn = (object)   $filterInstallmentsOn;
            $i = 0;
            // dd((strlen($i)>1)?$i:'0'.$i);
        foreach ($result as $key => $value) {
            foreach ($value as $code => $slugs) {
                if (!empty($slugs)) {
                    $amount = array_sum((array)$slugs);
                    if ($i == 0) {
                        $count = 0;
                        $startDate = strtotime('today +10 day');
                        $endDate = strtotime('today');
                    } else {
                        $count = $filterInstallmentsOn->{key($slugs)}['count'];
                        $startDate = $filterInstallmentsOn->{key($slugs)}['start_date'];
                        $endDate = $filterInstallmentsOn->{key($slugs)}['end_date'];
                    }
                    
                    $dd = $this->create(
                        [
                        // $dd[]  =  [
                        'calculation_id' => $calculation->id,
                        'survey_id' => $calculation->survey_id,
                        'end_date' => date('Y-m-d H:i:s', $endDate),
                        'start_date' => date('Y-m-d H:i:s', $startDate) ,
                        'account_id' => $companyCodeAccountId[$code],
                        'file_number' => $calculation->file_number,
                        'bill_number' => $billNumber = $this->billNumberGenerator($calculation->file_number, $code),
                        'pay_number' => $this->payNumebrGenerator($amount, 0, $billNumber, jdate()->format('y'), (strlen($i) > 1) ? $i : '0'.$i),
                        'amount' => $amount,
                        'number_of_installment' => $i,
                        'count' => $count,
                        'customer_name' => $calculation->customer_name,
                        // 'percent',
                        // 'installments_details',
                        // 'status',
                        'scope' => $calculation->scope
                        // ];
                        ]
                    );
                
                
                        // );
                }
            }
            $i++;
        }
            // dd($dd);
            return $dd;
        
    }

    public function organize($accounts, $values)
    {
        $data = [];

        foreach ($accounts as $slug => $account) {
            if (!array_key_exists($account, $data)) {
                $res = [];
            } else {
                $res = $data[$account];
            }

            if (array_key_exists($slug, $values)) {
                $res[$slug] = $values[$slug];
            }

            $data[$account] = $res;
        }

        return $data;
    }

    public function separateInstallments($invoiceItemPrice, $exceptInstallmentsOf)
    {
        $fixedOnes = [];
        $dynamicOnes = [];

        foreach ($invoiceItemPrice as $slug => $value) {
            if (!array_key_exists($slug, $exceptInstallmentsOf)) {
                $fixedOnes[$slug] = $value;
                continue;
            }

            $v = $exceptInstallmentsOf[$slug];
            $percent = $v[0];

            $dynamicPart = $value * ($percent / 100);
            $fixedPart   = $value - $dynamicPart;
            
            if ($dynamicPart > 0) {
                $dynamicOnes[$slug] = $dynamicPart;
                if ($fixedPart > 0) {
                    $fixedOnes[$slug] = $fixedPart;
                }
            }else{
                $fixedOnes[$slug] = $fixedPart;
            }

        }

        return [$fixedOnes, $dynamicOnes];
    }

    public function calcInstallment($itemPrices, $installmentPolicy)
    {
        if (array_sum($itemPrices) == 0) {
            return [];
        }

        $installments = [];

        while (count($installmentPolicy) != 0) {
            $res = [];
            foreach ($itemPrices as $slug => $value) {
                if (!array_key_exists($slug, $installmentPolicy)) {
                    continue;
                }

                $total = $installmentPolicy[$slug][2]--;
                if ($total <= 0) {
                    unset($installmentPolicy[$slug]);
                } else {
                    if ($value != 0) {
                        $res[$slug] = $value / $installmentPolicy[$slug][1];
                    } else {
                        $res[$slug] = 0;
                    }
                }
            }

            if (!empty($res)) {
                $installments[] = $res;
            }
        }

        return $installments;
    }
    
    
    public function billNumberGenerator($fileNumber, $code)
    {
        //TODO company code changed to account in accounts
        $billNumber = substr($fileNumber, 0, -1).$code.'1';
        // dd($fileNumber, $billNumber);
        // $billNumber = $fileNumber.$this->_areaService->find($id)->company_code.'1';
        $billNumber .= $this->controlDigitGenerator($billNumber);
        return $billNumber;
    }
    
    public function controlDigitGenerator($number)
    {
        $factorVar = 2;
        $result = 0;
        foreach (array_reverse(str_split($number)) as $val) {
            $result += (integer)$val * $factorVar;
            $factorVar++;
            if ($factorVar > 7) {
                $factorVar = 2;
            }
                
            // code...
        }
        $result %= 11;
        if ($result == 0 or $result == 1) {
            $controlDigit = $result;
        } else {
            $controlDigit = $result - 11;
        }
        
            return abs($controlDigit);
    }

    public function payNumebrGenerator($totalPrice = 0, $totalPriceWithDiscount = 0, $billNumber, $year, $period = '00')
    {
        // $totalPay = floor(($totalPrice - $totalPriceWithDiscount)/1000);
        // $length = strlen($totalPay);
        // substr_replace('00000000', $totalPay, 8-$length)
        $amount =floor(($totalPrice - $totalPriceWithDiscount) / 1000);
        $zeroLength = 8-strlen($amount);
        $zero = ($zeroLength>0)?str_repeat('0', $zeroLength):'';
        
        $payNumebr = $zero . $amount . substr($year, 1, 1) . $period;
        // dd($payNumebr);
        
        
        $payNumebr .= $this->controlDigitGenerator($payNumebr);
        $payNumebr .= $this->controlDigitGenerator($billNumber.$payNumebr);
        return $payNumebr;
    }
    
}
