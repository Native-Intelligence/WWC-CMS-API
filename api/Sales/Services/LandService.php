<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\LandRepository;

class LandService extends Service
{
    public function __construct(LandRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'land';
        $this->uniqueKey = 'cadastre_code';
    }
}
