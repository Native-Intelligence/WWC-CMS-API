<?php

namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\SaleReqRepository;
use Api\Sales\Services\FileService;
use Api\Sales\Settings\Services\AreaService;

class SaleReqService extends Service
{
    private $_fileService;
    
    public function __construct(SaleReqRepository $repository, FileService $fileService, AreaService $areaService)
    {
        $this->repository = $repository;
        $this->key = 'request';
        $this->uniqueKey = 'request_number';
        
        $this->_fileService = $fileService;
        $this->_areaService = $areaService;
    }
    
    public function checkUniqueKeyExist($data)
    {
        $this->data = (object)$this->getData($data);
        $this->data->request_number = $this->setRequestNumber($this->data->scope);
        
        if (!empty($this->uniqueKey)) {
            $uniqueKeys = explode('&', $this->uniqueKey);
            foreach ($uniqueKeys as $value) {
                $keys[$value] = $this->data->{$value};
            }
            $keys[] = ['status', '!=', '9'];
            $this->exist = $this->repository->findWhere($keys)->first();
        }
        
        if (!is_null($this->exist)) {
            throw new Exception(40640);
        }
        
        $requestDetails = $this->data->request_details;
        if (array_key_exists('request_type', $requestDetails)) {
            $this->data->request_type = $requestDetails['request_type'];
        }
        if (array_key_exists('branch_type', $requestDetails)) {
            $this->data->branch_type = $requestDetails['branch_type'];
        }
        if (array_key_exists('land_id', $requestDetails)) {
            $this->data->land_id = $requestDetails['land_id'];
        }
    }
    
    public function setRequestNumber($scope)
    {
        $arrayCodeArea = $this->_areaService->getByField(['scope' => $scope])->code;
        
        rsort($arrayCodeArea);
        
        $lastRequestNumber = $this->getAllByField([['scope', '=', $scope]])->max('request_number');
        
        if(empty($lastRequestNumber)){
	    $areaCode = $arrayCodeArea[0];
        }else{
            $areaCodeLength = strlen($arrayCodeArea[0]);
            $areaCode = substr($lastRequestNumber, 4, $areaCodeLength);
	}
        
        $last = $this->getLastItem();
     
        return jdate()->format('ym') . $areaCode . (isset($last) ? $last->id + 1 : 1);
    }
}
