<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\SurveyRepository;
use Api\Sales\Services\LandService;
use Api\Sales\Settings\Services\AreaService;
use Api\Sales\Services\FileService;
use Api\Sales\Services\SaleReqService;
use Api\Sales\Settings\Services\InstructionService;

class SurveyService extends Service
{

    private $_fileService;
    private $_saleReqService;
    private $_landService;
    private $_areaService;
    private $_instructionService;

    
    public function __construct(
        SurveyRepository $repository,
        FileService $fileService,
        SaleReqService $saleReqService,
        AreaService $areaService,
        LandService $landService,
        InstructionService $instructionService
    ) {
    
        $this->_fileService = $fileService;
        $this->_areaService = $areaService;
        $this->_saleReqService = $saleReqService;
        $this->_landService = $landService;
        $this->_instructionService = $instructionService;
        $this->repository = $repository;
        $this->key = 'survey';
        $this->uniqueKey = '';

        $this->event = [
            'created' => ['Api\Sales\Events\CartableChangeEvent', 'Api\Sales\Events\SurveyCreatedEvent']
        ];
    }

    public function checkUniqueKeyExist($data)
    {
        parent::checkUniqueKeyExist($data);

        // dd($this->data->survey_detail->usage_id);
        $surveyData = (array) $this->data->survey_detail;
        // $surveyData['g_service_type_id'] = $this->data->g_service_type_id;
        $surveyData['request_id'] = $this->data->request_id;
        $surveyData['survey_date'] = $this->data->survey_date;
        $surveyData['coordination_date'] = $this->data->coordination_date;
        $surveyData['scope'] = $this->data->scope;
        $surveyData['scope_city'] = $this->data->scope_city;
        // $surveyData['usage_id'] = $this->data->survey_detail->usage_id;
        // $surveyData['user_id'] = $this->data->user_id;
        
        if (empty($surveyData['instruction_id'])) {
            $surveyData['instruction_id'] = $this->_instructionService->getLastItem()->id;
        }

        // if ($this->data->request_id == 571)
        $request = $this->_saleReqService->find($this->data->request_id);
        if(empty($request->file_number)) {
            $surveyData['file_number'] =
            $this->setFileNumber($surveyData['scope'], $request->land_id, $request->id);
            $this->data = $surveyData;
        }else{
            $file = $this->_fileService->getByField(['file_number' => $request->file_number])->toArray();
            // dd($file, $surveyData);
            // $surveyData['survey_detail'] = array_diff($surveyData, $file);
            $this->data = array_merge($file, $surveyData);
            unset($this->data['status']);
            // dd($this->data);
        }
    }
    
    // public function getAll()
    // {
        // dd(parent::getAll());
        
    // }
    
    public function setFileNumber($scope, $landId, $requestId)
    {
        $arrayCodeArea = $this->_areaService->getByField([ 'scope' => $scope ])->code;

        rsort($arrayCodeArea);

        $lastFileNumber = $this->_fileService->getAllByField(['scope' => $scope])->max('file_number');
        $areaCodeLength = strlen($arrayCodeArea[0]);
        if($lastFileNumber==0) {
            $lastFileNumber = $arrayCodeArea[0].str_repeat('0', 9-$areaCodeLength);
        }
        $areaCode   = substr($lastFileNumber, 0, $areaCodeLength); //code
        $counter    = substr($lastFileNumber, $areaCodeLength, - 1); //number without code and controller
        $counterLength = strlen($counter);
        $lastCounter = str_repeat('9', $counterLength);
        
        if ($counter >= $lastCounter ) {
            if (in_array($areaCode + 1, $arrayCodeArea) ) {
                $areaCode ++;
                $newFileNumber = $areaCode . str_repeat('0', $counterLength - 1) . '1';
            } else {
                return false;
            }
        } else {
            // $newFileNumber = substr( $lastFileNumber, 0, - 1 );
            // $newFileNumber ++;
            // dd(($areaCode.$counter)++);
            $newFileNumber = $areaCode.$counter +1;
        }
        // dd($newFileNumber, $lastCounter);
        
        // $newCounter = substr( $newFileNumber, strlen( $arrayCodeArea[0] ), - 1 );
        $factorVar  = 2;
        $result     = 0;
        // foreach ( array_reverse( str_split( $newCounter ) ) as $key => $val ) {
        foreach ( array_reverse(str_split($counter)) as  $val ) {
            $result += (integer) $val * $factorVar;
            $factorVar ++;
            if ($factorVar > 7 ) {
                $factorVar = 2;
            }
        }
        
        $result %= 11;
        
        if ($result == 0 or $result == 1 ) {
            $controllNumber = 1;
        } else {
            $controllNumber = 11 - $result;
        }

        $this->_landService->update(
            [
            'id' => $landId,
            // 'subscription_number' => $survey->survey->subscription_number,
            'file_number' => $newFileNumber . $controllNumber
            ]
        );
        
        $this->_saleReqService->update(
            [
            'id' => $requestId,
            // 'subscription_number' => $survey->survey->subscription_number,
            'file_number' => $newFileNumber . $controllNumber
            ]
        );
            // dd($newFileNumber, $controllNumber);
        return $newFileNumber . $controllNumber;
    }
    
    public function setSubscriptionNumber()
    {
        return 5321;
    }
}
