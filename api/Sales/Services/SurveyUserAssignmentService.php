<?php
namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Repositories\SurveyUserAssignmentRepository;

class SurveyUserAssignmentService extends Service
{

    public function __construct(SurveyUserAssignmentRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'user';
        $this->uniqueKey = '';
    }
}
