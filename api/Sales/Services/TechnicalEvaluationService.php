<?php

namespace Api\Sales\Services;

use Infrastructure\Services\Service;
use Api\Sales\Repositories\TechnicalEvaluationRepository;
class TechnicalEvaluationService extends Service
{

    protected $uniqueKey;

    public function __construct(
        TechnicalEvaluationRepository $evaluation_repository
    ) {
        $this->repository = $evaluation_repository;
        $this->key = 'evaluation';
        $this->uniqueKey = '';

    }


}
