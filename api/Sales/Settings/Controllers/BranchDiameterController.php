<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\BranchDiameterRequest as REQUEST;
use Api\Sales\Settings\Services\BranchDiameterService as SERVICE;

class BranchDiameterController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
