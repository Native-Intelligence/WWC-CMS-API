<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\BranchDiameterDetailRequest as REQUEST;
use Api\Sales\Settings\Services\BranchDiameterDetailService as SERVICE;

class BranchDiameterDetailController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
