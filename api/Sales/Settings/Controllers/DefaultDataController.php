<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\DefaultDataRequest as REQUEST;
use Api\Sales\Settings\Services\DefaultDataService as SERVICE;

class DefaultDataController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
