<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\DiscountDetailRequest as REQUEST;
use Api\Sales\Settings\Services\DiscountDetailService as SERVICE;

class DiscountDetailController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
