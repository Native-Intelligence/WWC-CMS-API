<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\DocumentClassRequest as REQUEST;
use Api\Sales\Settings\Services\DocumentClassService as SERVICE;

class DocumentClassController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
