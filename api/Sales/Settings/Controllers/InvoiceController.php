<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\InvoiceRequest as REQUEST;
use Api\Sales\Settings\Services\InvoiceService as SERVICE;

class InvoiceController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
    
    // public function attach()
    // {
    //     $this->request->attachValidation();
    //
    //     return $this->response(
    //         app()->helper::jGenerate(
    //             $this->service->attach($this->request)
    //         ),
    //         201
    //     );
    // }
    //
    // public function detach()
    // {
    //     $this->request->attachValidation();
    //
    //     return $this->response(
    //         app()->helper::jGenerate(
    //             $this->service->detach($this->request)
    //         ),
    //         201
    //     );
    // }
}
