<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\InvoiceItemRequest as REQUEST;
use Api\Sales\Settings\Services\InvoiceItemService as SERVICE;

class InvoiceItemController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
