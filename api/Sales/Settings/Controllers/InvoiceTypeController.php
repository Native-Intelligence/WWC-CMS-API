<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\InvoiceTypeRequest as REQUEST;
use Api\Sales\Settings\Services\InvoiceTypeService as SERVICE;

class InvoiceTypeController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
