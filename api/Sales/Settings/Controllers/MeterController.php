<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\MeterRequest as REQUEST;
use Api\Sales\Settings\Services\MeterService as SERVICE;

class MeterController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
