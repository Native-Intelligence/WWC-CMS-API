<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\SiphonRequest as REQUEST;
use Api\Sales\Settings\Services\SiphonService as SERVICE;

class SiphonController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
