<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\SmartAccountingRequest as REQUEST;
use Api\Sales\Settings\Services\SmartAccountingService as SERVICE;

class SmartAccountingController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
