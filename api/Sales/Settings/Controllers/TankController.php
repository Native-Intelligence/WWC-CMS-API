<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\TankRequest as REQUEST;
use Api\Sales\Settings\Services\TankService as SERVICE;

class TankController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
