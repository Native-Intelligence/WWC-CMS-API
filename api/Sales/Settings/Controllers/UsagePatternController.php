<?php
namespace Api\Sales\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Sales\Settings\Requests\UsagePatternRequest as REQUEST;
use Api\Sales\Settings\Services\UsagePatternService as SERVICE;

class UsagePatternController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
