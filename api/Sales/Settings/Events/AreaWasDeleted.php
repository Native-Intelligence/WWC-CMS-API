<?php
namespace Api\Sales\Settings\Events;

use Infrastructure\Events\Event;
use Api\Sales\Settings\Models\Area;

class AreaWasDeleted extends Event
{

    public $area;
    
    public function __construct(Area $area)
    {
        $this->area = $area;
    }
}
