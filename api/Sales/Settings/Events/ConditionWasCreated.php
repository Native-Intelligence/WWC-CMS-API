<?php
namespace Api\Sales\Settings\Events;

use Infrastructure\Events\Event;
use Api\Sales\Settings\Models\Condition;

class ConditionWasCreated extends Event
{

    public $condition;

    public function __construct(Condition $condition)
    {
        $this->condition = $condition;
    }
}
