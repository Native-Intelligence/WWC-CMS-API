<?php
namespace Api\Sales\Settings\Events;

use Infrastructure\Events\Event;
use Api\Sales\Settings\Models\Direction;

class DirectionWasCreated extends Event
{

    public $direction;

    public function __construct(Direction $direction)
    {
        $this->direction = $direction;
    }
}
