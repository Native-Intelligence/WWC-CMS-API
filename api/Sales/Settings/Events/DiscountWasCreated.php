<?php
namespace Api\Sales\Settings\Events;

use Infrastructure\Events\Event;
use Api\Sales\Settings\Models\Discount;

class DiscountWasCreated extends Event
{

    public $discount;

    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }
}
