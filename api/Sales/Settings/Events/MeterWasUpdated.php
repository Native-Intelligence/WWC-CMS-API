<?php
namespace Api\Sales\Settings\Events;

use Infrastructure\Events\Event;
use Api\Sales\Settings\Models\Meter;

class MeterWasUpdated extends Event
{

    public $meter;

    public function __construct(Meter $meter)
    {
        $this->meter = $meter;
    }
}
