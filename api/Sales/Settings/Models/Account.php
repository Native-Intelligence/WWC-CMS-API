<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Account extends Model
{

    protected $table = 'ing_accounts';

    protected $fillable = [
                           'title',
                           'account_type',
                           'account_number',
                           'company_code',
                           'is_identifier',
                           'identifier',
                           'g_bank_id',
                           'bank_branch',
                           'bank_branch_code',
                           'accounting_code',
                           'status',
                           'scope'
                          ];

}
