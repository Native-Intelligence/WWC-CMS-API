<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Accounting extends Model
{

    protected $table = 'ing_accountings';

    protected $fillable = [
                           'title',
                           'document_no',
                           'date',
                           'is_confirmed',
                           'request_id',
                           'invoice_id',
                           'status',
                           'document_class_id',
                           'scope'
                          ];

    public function documentClass()
    {
        return $this->belongsTo('Api\Sales\Settings\Models\DocumentClass');
    }
}
