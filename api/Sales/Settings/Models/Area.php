<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Area extends Model
{

    protected $table = 'ing_areas';
    
    protected $view = 'ing_areas_view';

    protected $fillable = [
                           'name',
                           'code',
                        //    'company_code',
                           'type',
                           'parent_id',
                           'g_area_pattern_id',
                           'scope_max',
                           'display_name',
                           'scope',
                           'status'
                          ];
    
    protected $casts = [
        'code' => 'object'
    ];
}
