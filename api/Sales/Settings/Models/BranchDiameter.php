<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class BranchDiameter extends Model
{

    protected $table = 'ing_branch_diameters';

    protected $fillable = [
                           'title',
                           'code',
                           'type',
                           'status',
                           'scope'
                          ];

    public function branchDiameterDetails()
    {
        return $this->hasMany('Api\Sales\Settings\Models\BranchDiameterDetail');
    }
}
