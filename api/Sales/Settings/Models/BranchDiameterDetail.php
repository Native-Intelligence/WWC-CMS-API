<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class BranchDiameterDetail extends Model
{

    protected $table = 'ing_branch_diameter_details';

    protected $fillable = [
                           'branch_diameter_id',
                           'units_min',
                           'units_max',
                           'scope',
                           'status'
                          ];
    

}
