<?php

namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Discount extends Model
{
    
    protected $table = 'ing_discounts';
    
    protected $view = 'ing_discounts_view';
    
    protected $fillable = [
        'title',
        'g_discount_type_id',
        'code',
        'start_date',
        'end_date',
        'slug',
        'usage_id',
        'type',
        'status',
        'scope'
    ];
    
    public function discountDetails()
    {
        return $this->hasMany('Api\Sales\Settings\Models\DiscountDetail');
    }
}
