<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class DiscountDetail extends Model
{

    protected $table = 'ing_discount_details';

    protected $view = 'ing_discount_details_view';

    protected $fillable = [
                           'discount_id',
                           'formula_id',
                           'invoice_item_id',
                           'pecuniary_percent',
                           'installment_percent',
                           'status'
                          ];
    
//    public function invoiceItem()
//    {
//        return $this->belongsTo('Api\Sales\Settings\Models\InvoiceItem', 'invoice_item_id');
//    }

//    public function formula()
//    {
//        return $this->belongsTo('Api\Sales\Settings\Models\Filter');
//    }
}
