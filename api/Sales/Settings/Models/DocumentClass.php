<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class DocumentClass extends Model
{

    protected $table = 'ing_document_classes';

    protected $fillable = [
                           'title',
                           'type',
                           'status',
                           'scope'
                          ];
}
