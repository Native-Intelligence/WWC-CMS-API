<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Filter extends Model
{

    protected $table = 'ing_filters';

    protected $fillable = [
                           'title',
                           'filter',
                           'type',
                           'status',
                           'scope'
                          ];
}
