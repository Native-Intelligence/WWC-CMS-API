<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Instruction extends Model
{

    protected $table = 'ing_instructions';

    protected $fillable = [
                           'title',
                           'start_date',
                           'end_date',
                           'effective_date',
                           'adviser',
                           'advise_date',
                           'advising_organization',
                           'advising_position',
                           'base_rate',
                           'factors',
                           'instruction_type',
                           'indicator_number',
                           'indicator_date',
                           'status',
                           'scope'
                          ];

    protected $casts = [
        'base_rate' => 'object',
        'factors' => 'object',
    ];
}
