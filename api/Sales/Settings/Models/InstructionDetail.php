<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class InstructionDetail extends Model
{

    protected $table = 'ing_instruction_details';
    
    // protected $view = 'ing_instruction_details_view';

    protected $fillable = [
                           'instruction_id',
                           'title',
                           'slug',
                           'value',
                           'invoice_item_id',
                           'is_home',
                           'usage_id',
                           'g_branch_diameter_id',
                           'g_siphon_id',
                           'g_type_installation_id',
                           'g_land_type_id',
                           'unit_range',
                           'consumption_range',
                           'arena_range',
                           'foundation_range',
                           'g_tank_id',
                           'scope_cities',
                           'status',
                           'scopes',
                           'scope'
                          ];

    protected $casts = [
        'scopes' => 'object',
        'scope_cities' => 'object',
        'foundation_range' => 'object',
        'arena_range' => 'object',
        'unit_range' => 'object',
        'consumption_range' => 'object',
        'g_tank_id' => 'object',
        'usage_id' => 'object',
        // 'factors' => 'object',
    ];
}
