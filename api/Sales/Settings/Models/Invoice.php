<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'ing_invoices';
    protected $view = 'ing_invoices_view';

    protected $fillable = [
                           'usage_id',
                           'expense_id',
                           'formula_id',
                           'instruction_id',
                           'g_service_type_id',
                           'status',
                           'type',
                           'scope'
                          ];

    protected $casts = [ 'invoice_items' => 'object'];
    //
    // protected $hidden = ['formula', 'usage', 'instruction'];
    //
    // protected $appends = ['formula_title', 'usage_title', 'instruction_title', 'invoice_items'];
    //
    // public function getFormulaTitleAttribute()
    // {
    //     return $this->formula->title;
    // }
    //
    // public function getUsageTitleAttribute()
    // {
    //     return $this->usage->title;
    // }
    //
    // public function getInstructionTitleAttribute()
    // {
    //     return $this->instruction->title;
    // }
    //
    // public function getInvoiceItemsAttribute()
    // {
    //     return $this->items;
    // }
    //
    // public function filter()
    // {
    //     return $this->belongsTo('Api\Sales\Settings\Models\Filter');
    // }
    //
    // public function formula()
    // {
    //     return $this->belongsTo('Api\Sales\Settings\Models\Filter');
    // }
    //
    // public function usage()
    // {
    //     return $this->belongsTo('Api\Sales\Settings\Models\Usage');
    // }
    //
    // public function instruction()
    // {
    //     return $this->belongsTo('Api\Sales\Settings\Models\Instruction');
    // }
    //
    public function items()
    {
        return $this->belongsToMany('Api\Sales\Settings\Models\InvoiceItem', 'mid_invoice_item', 'invoice_id', 'invoice_item_id');
    }
}
