<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class InvoiceItem extends Model
{

    protected $table = 'ing_invoice_items';
    protected $view = 'ing_invoice_items_view';

    protected $fillable = [
                           'title',
                           'slug',
                           'is_partition',
                           'is_journal_doc',
                           'filter_id',
                           'formula_id',
                           'partition_percent',
                           'partition_count',
                           'account_id',
                           'step_id',
                           'check_paid',
                           'third_party_id',
                           'status',
                           'scope'
                         ];

    // protected $hidden = [
    //                         'formula',
    //                     ];
    //
    // protected $appends = [
    //                         'formula_title',
    //                     ];

    // public function getFormulaTitleAttribute()
    // {
    //     return $this->formula->title;
    // }
    //
    // public function formula()
    // {
    //     return $this->belongsTo('Api\Sales\Settings\Models\Filter');
    // }
}
