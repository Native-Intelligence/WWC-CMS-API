<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class InvoiceType extends Model
{

    public $table = 'ing_invoice_types';

    protected $fillable = [
                           'title',
                           'type',
                           'status',
                           'scope'
                         ];
}
