<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Meter extends Model
{

    protected $table = 'ing_meters';

    protected $fillable = [
                           'title',
                           'g_class_name_id',
                           'g_model_name_id',
                           'g_factory_name_id',
                           'start_point',
                           'digits_number',
                           'diameter',
                           'diameter_in',
                           'diameter_out',
                           'percentage_error',
                           'status',
                           'scope'
                          ];
}
