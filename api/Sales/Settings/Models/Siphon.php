<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Siphon extends Model
{

    protected $table = 'ing_siphons';

    protected $fillable = ['title', 'status','scope'];
}
