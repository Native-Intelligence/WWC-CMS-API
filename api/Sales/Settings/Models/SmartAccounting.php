<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class SmartAccounting extends Model
{

    protected $table = 'ing_smart_accountings';

    protected $fillable = [
                           'title',
                           'periority',
                           'details',
                           'document_class_id',
                           'status',
                           'scope'
                          ];

    public function documentClass()
    {
        return $this->belongsTo('Api\Sales\Settings\Models\DocumentClass');
    }
}
