<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Tank extends Model
{

    protected $table = 'ing_tanks';

    protected $fillable = ['code','title','address','areas','status','scope'];
	protected $casts = [ 'areas' => 'object'];

}
