<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Usage extends Model
{

    protected $table = 'ing_usages';
    protected $view = 'ing_usages_view';

    protected $fillable = [
                           'title',
                           'type',
                           'code',
                           'receivable_code',
                           'wastewater_code',
                           'water_code',
                           'wastewater_document_code',
                           'water_document_code',
                           'usage_id',
                           'status',
                           'scope'
                          ];

    public function usage()
    {
        return $this->belongsTo('Api\Sales\Settings\Models\Usage');
    }
}
