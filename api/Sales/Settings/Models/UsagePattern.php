<?php
namespace Api\Sales\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class UsagePattern extends Model
{

    protected $table = 'ing_usage_patterns';

    protected $fillable = [
                            'title',
                            'code',
                            'g_unit_id',
                            'value',
                            'description',
                            'status',
                            'scope'
                          ];
}
