<?php
namespace Api\Sales\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Settings\Models\Discount;

class DiscountRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Settings\\Models\\Discount';
    }
}
