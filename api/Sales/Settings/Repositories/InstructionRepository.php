<?php
namespace Api\Sales\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Settings\Models\Instruction;

class InstructionRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Settings\\Models\\Instruction';
    }
}
