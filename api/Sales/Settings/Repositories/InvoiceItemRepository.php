<?php
namespace Api\Sales\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Settings\Models\InvoiceItem;

class InvoiceItemRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Settings\\Models\\InvoiceItem';
    }
}
