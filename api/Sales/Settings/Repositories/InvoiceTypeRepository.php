<?php
namespace Api\Sales\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Settings\Models\InvoiceType;

class InvoiceTypeRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Settings\\Models\\InvoiceType';
    }
}
