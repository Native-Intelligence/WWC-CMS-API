<?php
namespace Api\Sales\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Sales\Settings\Models\SmartAccounting;

class SmartAccountingRepository extends Repository
{

    public function model()
    {
        return 'Api\\Sales\\Settings\\Models\\SmartAccounting';
    }
}
