<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class AccountRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('account');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'account_type'        => 'required|integer',
            'account_number'      => 'required|string',
            'company_code'      => 'required|string',
            'is_identifier'        => 'required|boolean',
            'identifier' => 'string',
            'g_bank_id'      => 'required|integer',
            'bank_branch'      => 'required|string',
            'bank_branch_code'      => 'required|string',
            'accounting_code' => 'string',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'account_type'        => 'sometimes|required|integer',
            'account_number'      => 'sometimes|required|string',
            'company_code'      => 'sometimes|required|string',
            'is_identifier'        => 'sometimes|required|boolean',
            'identifier' => 'string',
            'g_bank_id'      => 'sometimes|required|integer',
            'bank_branch'      => 'sometimes|required|string',
            'bank_branch_code'      => 'sometimes|required|string',
            'accounting_code' => 'string',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
