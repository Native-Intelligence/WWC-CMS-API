<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class AccountingRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('accounting');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'document_no' => 'required|string',
            'document_date' => 'required|date',
            'is_confirmed' => 'required|boolean',
            'request_id' => 'required|integer',
            'invoice_id' => 'required|integer',
            'document_class_id' => 'required|integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'document_no' => 'sometimes|required|string',
            'document_date' => 'sometimes|required|date',
            'is_confirmed' => 'sometimes|required|boolean',
            'request_id' => 'sometimes|required|integer',
            'invoice_id' => 'sometimes|required|integer',
            'document_class_id' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
