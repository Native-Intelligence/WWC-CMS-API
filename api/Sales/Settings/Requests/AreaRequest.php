<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class AreaRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('area');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'name' => 'required|string',
            'code' => 'required|array',
            // 'company_code' => 'required|string',
            'type' => 'required|boolean',
            'scope' => 'integer',
            'g_area_pattern_id' => 'integer',
            'parent_id' => 'required|integer',
            'pattern' => 'required|string',
            // 'length' => 'required|string',
            'scope_max' => 'integer',
            'display_name' => 'string',
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'name' => 'sometimes|required|string',
            'code' => 'sometimes|required|array',
            // 'company_code' => 'sometimes|required|string',
            'type' => 'sometimes|required|boolean',
            'scope' => 'integer',
            'g_area_pattern_id' => 'integer',
            'display_name' => 'string',
            'parent_id' => 'sometimes|required|integer',
            'pattern' => 'sometimes|required|string',
            // 'length' => 'required|string',
            'scope_max' => 'integer',
            'display_name' => 'string',
        ];
        $this->validate($this->data, $this->rules);
    }
}
