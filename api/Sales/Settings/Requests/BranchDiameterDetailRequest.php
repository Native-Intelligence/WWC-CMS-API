<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class BranchDiameterDetailRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('branch_diameter_detail');
    }

    public function createValidation()
    {
        $this->rules = [
            'branch_diameter_id' => 'required|integer',
            'units_min'          => 'required|integer',
            'units_max'          => 'required|integer',
            'scope'              => 'integer',
            'status'             => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id'                 => 'sometimes|required|integer',
            'branch_diameter_id' => 'sometimes|required|integer',
            'units_min'          => 'sometimes|required|integer',
            'units_max'          => 'sometimes|required|integer',
            'scope'              => 'integer',
            'status'             => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
