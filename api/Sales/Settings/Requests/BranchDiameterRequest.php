<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class BranchDiameterRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('branch_diameter');
    }

    public function createValidation()
    {
        $this->rules = [
            'title'  => 'required|string',
            'code'   => 'required|string',
            'type'   => 'required|integer',
            'status' => 'integer',
            'scope'  => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id'     => 'sometimes|required|integer',
            'title'  => 'sometimes|required|string',
            'code'   => 'sometimes|required|string',
            'type'   => 'sometimes|required|integer',
            'status' => 'sometimes|required|integer',
            'scope'  => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
