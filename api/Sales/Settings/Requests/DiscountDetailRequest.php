<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class DiscountDetailRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('discount_detail');
    }

    public function createValidation()
    {
        $this->rules = [
            'discount_id' => 'required|integer',
            'formula_id' => 'required|integer',
            'invoice_item_id' => 'required|integer',
            'pecuniary_percent' => 'required|integer',
            'installment_percent' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'discount_id' => 'sometimes|required|integer',
            'formula_id' => 'sometimes|required|integer',
            'invoice_item_id' => 'sometimes|required|integer',
            'pecuniary_percent' => 'sometimes|required|integer',
            'installment_percent' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
