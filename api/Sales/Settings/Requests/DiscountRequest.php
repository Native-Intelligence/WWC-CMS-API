<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class DiscountRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('discount');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'slug' => 'required|string',
            'g_discount_type_id' => 'required|integer',
            'code' => 'string',
            'start_date' => 'date',
            'end_date' => 'date',
            'usage_id' => 'required|int',
            'type' => 'required|int',
            // 'percent' => 'required|integer',
            // 'discount_detail' => 'required|array',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'slug' => 'sometimes|required|string',
            'g_discount_type_id' => 'sometimes|required|integer',
            'code' => 'string',
            'start_date' => 'date',
            'end_date' => 'date',
            'usage_id' => 'int',
            'type' => 'int',
            // 'percent' => 'sometimes|required|integer',
            // 'discount_detail' => 'sometimes|required|array',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
