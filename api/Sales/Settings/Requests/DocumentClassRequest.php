<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class DocumentClassRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('document_class');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'type'  => 'required|integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'type'  => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
