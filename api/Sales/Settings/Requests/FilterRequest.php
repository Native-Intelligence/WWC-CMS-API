<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class FilterRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('filter');
    }

    public function createValidation()
    {
        $this->rules = [
            'title'   => 'required|string',
            'filter' => 'required|string',
            'type'      => 'required|boolean',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title'   => 'sometimes|required|string',
            'filter' => 'sometimes|required|string',
            'type'      => 'sometimes|required|boolean',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
