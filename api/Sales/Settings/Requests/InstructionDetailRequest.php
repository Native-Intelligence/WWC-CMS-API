<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class InstructionDetailRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('instruction_detail');
    }

    public function createValidation()
    {
        $this->rules = [
            'instruction_id' => 'required|integer',
            'title' => 'required|string',
            'slug' => 'required|string',
            'value' => 'required|integer',
            'invoice_item_id' => 'required|integer',
            'is_home' => 'boolean',
            'usage_id' => 'required|array',
            'g_branch_diameter_id' => 'nullable|integer',
            'g_siphon_id' => 'nullable|integer',
            'g_type_installation_id' => 'nullable|integer',
            'g_land_type_id' => 'nullable|integer',
            'unit_range' => 'nullable|array',
            'consumption_range' => 'nullable|array',
            'arena_range' => 'nullable|array',
            'foundation_range' => 'nullable|array',
            'g_tank_id' => 'nullable|array',
            'scope_cities' => 'required|array',
            'scopes' => 'required|array',
            'scope' => 'required|integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'instruction_id' => 'sometimes|required|integer',
            'title' => 'sometimes|required|string',
            'slug' => 'sometimes|required|string',
            'value' => 'sometimes|required|integer',
            'invoice_item_id' => 'sometimes|required|integer',
            'is_home' => 'boolean',
            'usage_id' => 'sometimes|required|array',
            'g_branch_diameter_id' => 'integer',
            'g_siphon_id' => 'integer',
            'g_type_installation_id' => 'integer',
            'g_land_type_id' => 'integer',
            'unit_range' => 'array',
            'consumption_range' => 'array',
            'arena_range' => 'array',
            'foundation_range' => 'array',
            'g_tank_id' => 'array',
            'scope_cities' => 'sometimes|required|array',
            'scopes' => 'sometimes|required|array',
            'scope' => 'sometimes|required|integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
