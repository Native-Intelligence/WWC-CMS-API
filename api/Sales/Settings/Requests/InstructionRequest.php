<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class InstructionRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('instruction');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'effective_date' => 'required|date',
            'adviser' => 'required|string',
            'advise_date' => 'required|date',
            'advising_organization' => 'required|string',
            'advising_position' => 'required|string',
            'factors' => 'required|array',
            'indicator_number' => 'required|string',
            'indicator_date' => 'required|date',
            'instruction_type' => 'required|integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'start_date' => 'sometimes|required|date',
            'end_date' => 'sometimes|required|date',
            'effective_date' => 'sometimes|required|date',
            'adviser' => 'sometimes|required|string',
            'advise_date' => 'sometimes|required|date',
            'advising_organization' => 'sometimes|required|string',
            'advising_position' => 'sometimes|required|string',
            'factors' => 'sometimes|required|array',
            'indicator_number' => 'sometimes|required|string',
            'indicator_date' => 'sometimes|required|date',
            'instruction_type' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
