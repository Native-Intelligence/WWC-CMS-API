<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class InvoiceItemRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('invoice_item');
    }

    public function createValidation()
    {
        $this->rules = [
            'title'          => 'required|string',
            'slug'          => 'required|string',
            // 'invoice_type_id' => 'required|integer',
            'step_id' => 'integer|nullable',
            'filter_id' => 'integer',
            'formula_id' => 'integer',
            'is_partition'   => 'boolean',
            'is_journal_doc' => 'boolean',
            'check_paid' => 'boolean',
            
            'partition_count' => 'nullable|integer',
            'account_id' => 'nullable|string',
            'third_party_id' => 'nullable|string',
            'scope' => 'required|integer',
            'status' => 'integer',
            'partition_percent' => 'required|integer',
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title'          => 'sometimes|required|string',
            'slug'          => 'sometimes|required|string',
            // 'invoice_type_id' => 'sometimes|required|integer',
            'step_id' => 'integer|nullable',
            'filter_id' => 'integer',
            'formula_id' => 'integer',
            'is_partition'   => 'boolean',
            'is_journal_doc' => 'boolean',
            'partition_percent' => 'sometimes|required|integer',
            'check_paid' => 'boolean',
            
            'partition_count' => 'nullable|integer',
            'account_id' => 'nullable|string',
            'third_party_id' => 'nullable|string',
            'scope' => 'sometimes|required|integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
