<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class InvoiceRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('invoice');
    }

    public function createValidation()
    {
        $this->rules = [
            'usage_id' => 'required|integer',
            'expense_id' => 'nullable|integer',
            // 'filter_id' => 'required|integer',
            'formula_id' => 'required|integer',
            'instruction_id' => 'required|integer',
            'invoice_items' => 'required|array',
            'type' => 'required|integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'usage_id' => 'sometimes|required|integer',
            'expense_id' => 'nullable|integer',
            // 'filter_id' => 'sometimes|required|integer',
            'formula_id' => 'sometimes|required|integer',
            'instruction_id' => 'sometimes|required|integer',
            'invoice_items' => 'sometimes|required|array',
            'type' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    // public function attachValidation()
    // {
    //     $this->rules = [
    //         'id' => 'required|integer',
    //         'ids' => 'required|array'
    //     ];
    //     $this->validate($this->data, $this->rules);
    // }
}
