<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class MeterRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('meter');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'string',
            'g_class_name_id' => 'required|integer',
            'g_model_name_id' => 'required|integer',
            'g_factory_name_id' => 'required|integer',
            'start_point' => 'integer',
            'digits_number' => 'required|integer',
            'diameter' => 'required|numeric',
            'diameter_in' => 'required|numeric',
            'diameter_out' => 'required|numeric',
            'percentage_error' => 'required|integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'g_class_name_id' => 'sometimes|required|integer',
            'g_model_name_id' => 'sometimes|required|integer',
            'g_factory_name_id' => 'sometimes|required|integer',
            'start_point' => 'integer',
            'digits_number' => 'sometimes|required|integer',
            'diameter' => 'sometimes|required|numeric',
            'diameter_in' => 'sometimes|required|numeric',
            'diameter_out' => 'sometimes|required|numeric',
            'percentage_error' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
