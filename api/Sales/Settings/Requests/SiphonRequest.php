<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class SiphonRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('siphon');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
