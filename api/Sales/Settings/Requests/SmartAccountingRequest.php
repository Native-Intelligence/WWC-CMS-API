<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class SmartAccountingRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('smart_accounting');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'periority' => 'required|integer',
            'document_class_id' => 'required|integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'periority' => 'sometimes|required|integer',
            'document_class_id' => 'sometimes|required|integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
