<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class TankRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('tank');
    }

    public function createValidation()
    {
        $this->rules = [
            'code' => 'required|string',
            'title' => 'required|string',
            'address' => 'required|string',
            'areas' => 'required|array',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'code' => 'sometimes|required|string',
            'title' => 'sometimes|required|string',
            'address' => 'sometimes|required|string',
            'areas' => 'sometimes|required|array',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
