<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class UsagePatternRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('usage_pattern');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'code'  => 'required|integer',
            'g_unit_id' => 'required|integer',
            'value' => 'required|numeric',
            'description' => 'string',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'code'  => 'sometimes|required|integer',
            'g_unit_id' => 'sometimes|required|integer',
            'value' => 'sometimes|required|numeric',
            'description' => 'string',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
