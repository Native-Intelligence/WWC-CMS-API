<?php
namespace Api\Sales\Settings\Requests;

use Infrastructure\Requests\Request;

class UsageRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('usage');
    }

    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'type'  => 'required|integer',
            'code' => 'required|integer',
            'receivable_code' => 'string',
            'wastewater_code' => 'string',
            'water_code'  => 'string',
            'wastewater_document_code'  => 'string',
            'water_document_code'  => 'string',
            'usage_id' => 'integer',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }

    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'type'  => 'sometimes|required|integer',
            'code' => 'sometimes|required|integer',
            'receivable_code' => 'string',
            'wastewater_code' => 'string',
            'water_code'  => 'string',
            'wastewater_document_code'  => 'string',
            'water_document_code'  => 'string',
            'usage_id' => 'integer',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
