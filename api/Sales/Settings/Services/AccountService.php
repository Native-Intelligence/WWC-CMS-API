<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\AccountRepository;

class AccountService extends Service
{

    public function __construct(AccountRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'account';
        $this->uniqueKey = 'account_number';
    }
}
