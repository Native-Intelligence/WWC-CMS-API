<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\AccountingRepository;

class AccountingService extends Service
{

    public function __construct(AccountingRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'accounting';
        $this->uniqueKey = 'account_number';
    }
}
