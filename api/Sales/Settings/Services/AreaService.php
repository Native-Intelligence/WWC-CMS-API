<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\AreaRepository;

class AreaService extends Service
{

    public function __construct(AreaRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'area';
        $this->uniqueKey = '';
    }
    
    
    // public function getAllWithoutPaginate()
    // {
    //     return $this->repository->scopeQuery(
    //         function ($query) {
    //             return $query->where('status', '!=', '9');
    //             // ->where(
    //             //     function ($query) {
    //             //         $temp = 0;
    //             //         $query->where(
    //             //             function ($query) use (&$temp) {
    //             //                 foreach ((array) app()->userHelper::$currentUser->access->scope as $k => $v) {
    //             //                     if (!empty(array_diff((array) $v, array(0, 1)))) {
    //             //                         if ($v[0] != 0 and $v[0] != 1) {
    //             //                             $query->WhereRaw('JSON_VALUE(scope,\'$.'.$k.'\') in ('.implode(',', $v).')');
    //             //                         } else if ($v[0] == 0 or $v[1] == 0) {
    //             //                             $query->whereRaw('JSON_VALUE(scope,\'$.'.$k.'\')', '!=', '');
    //             //                         } else if ($v[0] == 1 or $v[1] == 1) {
    //             //                             $query->whereRaw('JSON_VALUE(scope,\'$.'.$k.'\')', '=', '');
    //             //                         }
    //             //                         $temp = 1;
    //             //                     }
    //             //                 }
    //             //             }
    //             //         )->orWhere(
    //             //             function ($query) use (&$temp) {
    //             //                 if($temp == 1) {
    //             //                     $query->orWhereNull('scope');
    //             //                 }
    //             //             }
    //             //         );
    //         }
    //     );
    //     //     }
    //     // );
    // }

    
    public function getAll()
    {
        // return parent::getAllWithoutPaginate()->all();
        // dd($this->getAllWithoutPaginate()->first());
        // dd("Sss");
        return $this->getAllWithoutPaginate()->all();

    }

    public function checkUniqueKeyExist($data)
    {
        parent::checkUniqueKeyExist($data);
        
        if(isset($this->data->parent_id)) {
            $count = $this->repository->findWhere(['parent_id' => $this->data->parent_id])->count();
            // dd($count);
            $count++;
        
            $parent = $this->find($this->data->parent_id);
            // $pattern = str_replace($this->data->pattern, str_pad($count, 2, '0', STR_PAD_LEFT), '');
            // $pattern = ;
        
            // dd($parent->scope+ (integer)$pattern);
            $this->data->scope = $parent->scope + (integer)sprintf($this->data->pattern, $count);
            $this->data->scope_max = $this->data->scope +
            (integer)str_replace('0', '9', explode('d', $this->data->pattern)[1]);
            // dd($this->data->scope_max);
            // foreach ($this->data->scope as $k => $v) {
            //     if (empty($v)) {
            //         $this->data->scope[$k] = $this->repository->first()->orderBy('id', 'desc')->first()->id + app()->helper::$plusId;
            //         break;
            //     }
            // }
        }
    }
}
