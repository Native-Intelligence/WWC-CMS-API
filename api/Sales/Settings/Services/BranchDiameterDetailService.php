<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\BranchDiameterDetailRepository;

class BranchDiameterDetailService extends Service
{

    public function __construct(BranchDiameterDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'branch_diameter_detail';
        $this->uniqueKey = '';
    }

}
