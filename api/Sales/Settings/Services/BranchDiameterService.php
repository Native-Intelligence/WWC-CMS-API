<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\BranchDiameterRepository;

class BranchDiameterService extends Service
{

    public function __construct(BranchDiameterRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'branch_diameter';
        $this->uniqueKey = '';
    }
}
