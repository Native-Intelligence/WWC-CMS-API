<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\DiscountDetailRepository;

class DiscountDetailService extends Service
{
    public function __construct(DiscountDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'discount_detail';
        $this->uniqueKey = '';
    }
}
