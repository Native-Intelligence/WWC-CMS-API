<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\DiscountRepository;

class DiscountService extends Service
{

    public function __construct(DiscountRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'discount';
        $this->uniqueKey = 'title';
    }
}
