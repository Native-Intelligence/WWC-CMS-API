<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\DocumentClassRepository;

class DocumentClassService extends Service
{

    public function __construct(DocumentClassRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'document_class';
        $this->uniqueKey = 'title';
    }
}
