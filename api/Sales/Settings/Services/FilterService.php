<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\FilterRepository;

class FilterService extends Service
{

    public function __construct(FilterRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'filter';
        $this->uniqueKey = 'title';
    }
}
