<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\InstructionDetailRepository;

class InstructionDetailService extends Service
{

    public function __construct(InstructionDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'instruction_detail';
        $this->uniqueKey = '';
    }
}
