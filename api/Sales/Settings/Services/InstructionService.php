<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\InstructionRepository;

class InstructionService extends Service
{

    public function __construct(InstructionRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'instruction';
        $this->uniqueKey = 'title';
    }
}
