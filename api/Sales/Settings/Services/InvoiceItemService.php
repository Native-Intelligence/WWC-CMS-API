<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\InvoiceItemRepository;

class InvoiceItemService extends Service
{

    public function __construct(InvoiceItemRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'invoice_item';
        $this->uniqueKey = 'title';
    }
}
