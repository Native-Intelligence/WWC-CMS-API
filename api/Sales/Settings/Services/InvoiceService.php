<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\InvoiceRepository;

class InvoiceService extends Service
{

    public function __construct(InvoiceRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'invoice';
        $this->uniqueKey = '';
    }
    
    public function create($data)
    {
        $this->checkUniqueKeyExist($data);
        
        $result = $this->repository->create((array) $this->data);

        $this->archiveEvent('create', $result);

        if (isset($this->event['created'])) {
            foreach ($this->event['created'] as $val) {
                event(new $val($result));
            }
        }
        // dd($result->id, (array) $result->invoice_items);
            $this->repository->find($result->id)->items()->sync((array) $this->data->invoice_items);

        return $result;
    }
    
    
    public function update($data)
    {
        $this->data = (object) $this->getData($data);
        
        $result = $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->update((array) $this->data, $this->data->id);
        
        $this->archiveEvent('update', $result);

        if (isset($this->event['updated'])) {
            foreach ($this->event['updated'] as $val) {
                event(new $val($result));
            }
        }

        // dd(
        //     array_map(
        //         function ($value) {
        //             return $value['id'];
        //         }, (array) $this->data->invoice_items
        //     )
        // );
        if(isset($this->data->invoice_items)) {
            $this->repository->find($result->id)->items()->sync((array) $this->data->invoice_items);
        }

        return $result;
    }
    // public function attach($id, $ids)
    // {
    //     return $this->repository->find($id)->invoiceItems()->attach($ids);
    //
    // }
    //
    // public function detach($id, $ids)
    // {
    //     return $this->repository->find($id)->invoiceItems()->detach($ids);
    // }
}
