<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\InvoiceTypeRepository;

class InvoiceTypeService extends Service
{

    public function __construct(InvoiceTypeRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'invoice_type';
        $this->uniqueKey = 'title';
    }
}
