<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\MeterRepository;

class MeterService extends Service
{

    public function __construct(MeterRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'meter';
        $this->uniqueKey = 'title';
    }
}
