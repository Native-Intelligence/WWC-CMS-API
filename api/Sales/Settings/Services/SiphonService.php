<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\SiphonRepository;

class SiphonService extends Service
{

    public function __construct(SiphonRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'siphon';
        $this->uniqueKey = 'title';
    }
}
