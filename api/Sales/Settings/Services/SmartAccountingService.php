<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\SmartAccountingRepository;

class SmartAccountingService extends Service
{

    public function __construct(SmartAccountingRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'smart_accounting';
        $this->uniqueKey = 'title';
    }
}
