<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\TankRepository;

class TankService extends Service
{

    public function __construct(TankRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'tank';
        $this->uniqueKey = 'code';
    }
}
