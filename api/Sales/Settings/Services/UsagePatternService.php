<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\UsagePatternRepository;

class UsagePatternService extends Service
{

    public function __construct(UsagePatternRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'usage_pattern';
        $this->uniqueKey = 'title';
    }
}
