<?php
namespace Api\Sales\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Sales\Settings\Repositories\UsageRepository;

class UsageService extends Service
{

    public function __construct(UsageRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'usage';
        $this->uniqueKey = 'title&code&type';
    }
}
