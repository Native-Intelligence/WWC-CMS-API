<?php
$router->group(
    ['namespace' => 'Controllers', 'prefix' => 'sales', 'middleware' => ['cors', 'auth']],
    function () use ($router) {
        $router->group(
            ['prefix' => 'lands'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getLands',
                    'uses' => 'LandController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getLand',
                    'uses' => 'LandController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createLand',
                    'uses' => 'LandController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateLand',
                    'uses' => 'LandController@update'
                    ]
                );
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyLand',
                    'uses' => 'LandController@destroy'
                    ]
                );
            }
        );
        $router->group(
            ['prefix' => 'persons'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getPersons',
                    'uses' => 'PersonController@getAll'
                    ]
                );
                $router->put(
                    '/get',
                    [
                    'as' => 'getPerson',
                    'uses' => 'PersonController@getByParams'
                    ]
                );
                $router->post(
                    '/create',
                    [
                    'as' => 'createPerson',
                    'uses' => 'PersonController@create'
                    ]
                );
                $router->post(
                    '/update',
                    [
                    'as' => 'updatePerson',
                    'uses' => 'PersonController@update'
                    ]
                );
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyPerson',
                    'uses' => 'PersonController@destroy'
                    ]
                );
            }
        );
        $router->group(
            ['prefix' => 'requests'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getSaleRequets',
                    'uses' => 'SaleReqController@getAll'
                    ]
                );
                $router->put(
                    '/get',
                    [
                    'as' => 'getSaleReq',
                    'uses' => 'SaleReqController@getByParams'
                    ]
                );
                $router->post(
                    '/create',
                    [
                    'as' => 'createSaleReq',
                    'uses' => 'SaleReqController@create'
                    ]
                );
                $router->post(
                    '/update',
                    [
                    'as' => 'updateSaleReq',
                    'uses' => 'SaleReqController@update'
                    ]
                );
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroySaleReq',
                    'uses' => 'SaleReqController@destroy'
                    ]
                );
            }
        );
        $router->group(
            ['prefix' => 'branches'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getBranches',
                    'uses' => 'BranchController@getAll'
                    ]
                );
                $router->put(
                    '/get',
                    [
                    'as' => 'getBranch',
                    'uses' => 'BranchController@getByParams'
                    ]
                );
                $router->post(
                    '/create',
                    [
                    'as' => 'createBranch',
                    'uses' => 'BranchController@create'
                    ]
                );
                $router->post(
                    '/update',
                    [
                    'as' => 'updateBranch',
                    'uses' => 'BranchController@update'
                    ]
                );
                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyBranch',
                    'uses' => 'BranchController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'surveys'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getSurveys',
                    'uses' => 'SurveyController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getSurvey',
                    'uses' => 'SurveyController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createSurvey',
                    'uses' => 'SurveyController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateSurvey',
                    'uses' => 'SurveyController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroySurvey',
                    'uses' => 'SurveyController@destroy'
                    ]
                );
                
                $router->group(
                    ['prefix'=>'users'], function () use ($router) {
                        $router->get(
                            '/', [
                            'as'  =>'getAllSurveyUserAssignment',
                            'uses'=>'SurveyUserAssignmentController@getAll'
                            ]
                        );

                        $router->put(
                            '/get', [
                            'as'  =>'getSurveyUserAssignment',
                            'uses'=>'SurveyUserAssignmentController@getByParams'
                            ]
                        );

                        $router->post(
                            '/create', [
                            'as'  =>'createSurveyUserAssignment',
                            'uses'=>'SurveyUserAssignmentController@create'
                            ]
                        );

                        $router->post(
                            '/update', [
                            'as'  =>'updateSurveyUserAssignment',
                            'uses'=>'SurveyUserAssignmentController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy', [
                            'as'  =>'destroySurveyUserAssignment',
                            'uses'=>'SurveyUserAssignmentController@destroy'
                            ]
                        );
                    }
                );
            }
        );
    
        $router->group(
            ['prefix' => 'files'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getFiles',
                    'uses' => 'FileController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getFile',
                    'uses' => 'FileController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createFile',
                    'uses' => 'FileController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateFile',
                    'uses' => 'FileController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyFile',
                    'uses' => 'FileController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'consumptions'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getConsumptions',
                    'uses' => 'ConsumptionController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getConsumption',
                    'uses' => 'ConsumptionController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createConsumption',
                    'uses' => 'ConsumptionController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateConsumption',
                    'uses' => 'ConsumptionController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyConsumption',
                    'uses' => 'ConsumptionController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'calculations'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getCalculations',
                    'uses' => 'CalculationController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getCalculation',
                    'uses' => 'CalculationController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createCalculation',
                    'uses' => 'CalculationController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateCalculation',
                    'uses' => 'CalculationController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyCalculation',
                    'uses' => 'CalculationController@destroy'
                    ]
                );

                // $router->post(
                //     '/installment',
                //     [
                //     'as' => 'installmentCalculation',
                //     'uses' => 'CalculationController@installment'
                //     ]
                // );
            }
        );

        $router->group(
            ['prefix' => 'installments'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getInstallments',
                    'uses' => 'InstallmentController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getInstallment',
                    'uses' => 'InstallmentController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createInstallment',
                    'uses' => 'InstallmentController@installment'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateInstallment',
                    'uses' => 'InstallmentController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyInstallment',
                    'uses' => 'InstallmentController@destroy'
                    ]
                );
                
                // $router->post(
                //     '/installment',
                //     [
                //     'as' => 'installmentCalculation',
                //     'uses' => 'CalculationController@installment'
                //     ]
                // );

                // $router->post(
                //     '/installment',
                //     [
                //     'as' => 'installmentInstallment',
                //     'uses' => 'InstallmentController@installment'
                //     ]
                // );
            }
        );
    
        $router->group(
            ['prefix' => 'installations'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getInstallations',
                    'uses' => 'InstallationController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getInstallation',
                    'uses' => 'InstallationController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createInstallation',
                    'uses' => 'InstallationController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateInstallation',
                    'uses' => 'InstallationController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyInstallation',
                    'uses' => 'InstallationController@destroy'
                    ]
                );
            }
        );

        $router->group(
            [ 'prefix' => 'evaluations' ], function () use ( $router ) {


                $router->get(
                    '/', [
                    'as'   => 'getAllTechnicalEvaluation',
                    'uses' => 'TechnicalEvaluationController@getAll'
                    ]
                );

                $router->put(
                    '/get', [
                    'as'   => 'getTechnicalEvaluation',
                    'uses' => 'TechnicalEvaluationController@getByParams'
                    ]
                );

                $router->post(
                    '/create', [
                    'as'   => 'createTechnicalEvaluation',
                    'uses' => 'TechnicalEvaluationController@create'
                    ]
                );

                $router->post(
                    '/update', [
                    'as'   => 'updateTechnicalEvaluation',
                    'uses' => 'TechnicalEvaluationController@update'
                    ]
                );

                $router->delete(
                    '/destroy', [
                    'as'   => 'destroyTechnicalEvaluation',
                    'uses' => 'TechnicalEvaluationController@destroy'
                    ]
                );
                
                $router->group(
                    ['prefix'=>'users'], function () use ($router) {
                        $router->get(
                            '/', [
                            'as'  =>'getAllEvaluationUserAssignment',
                            'uses'=>'EvaluationUserAssignmentController@getAll'
                            ]
                        );

                        $router->put(
                            '/get', [
                            'as'  =>'getEvaluationUserAssignment',
                            'uses'=>'EvaluationUserAssignmentController@getByParams'
                            ]
                        );

                        $router->post(
                            '/create', [
                            'as'  =>'createEvaluationUserAssignment',
                            'uses'=>'EvaluationUserAssignmentController@create'
                            ]
                        );

                        $router->post(
                            '/update', [
                            'as'  =>'updateEvaluationUserAssignment',
                            'uses'=>'EvaluationUserAssignmentController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy', [
                            'as'  =>'destroyEvaluationUserAssignment',
                            'uses'=>'EvaluationUserAssignmentController@destroy'
                            ]
                        );
                    }
                );

            }
        );

    }
);


$router->group(
    ['namespace' => 'Settings\Controllers', 'prefix' => 'ingredients', 'middleware' => ['cors', 'auth']],
    function () use ($router) {
        $router->group(
            ['prefix' => 'meters'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getMeters',
                    'uses' => 'MeterController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getMeter',
                    'uses' => 'MeterController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createMeter',
                    'uses' => 'MeterController@create'
                    ]
                );
            
                $router->post(
                    '/update',
                    [
                    'as' => 'updateMeter',
                    'uses' => 'MeterController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyMeter',
                    'uses' => 'MeterController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'instructions'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getInstructions',
                    'uses' => 'InstructionController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getInstruction',
                    'uses' => 'InstructionController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createInstruction',
                    'uses' => 'InstructionController@create'
                    ]
                );
            
                $router->post(
                    '/update',
                    [
                    'as' => 'updateInstruction',
                    'uses' => 'InstructionController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyInstruction',
                    'uses' => 'InstructionController@destroy'
                    ]
                );
                
                $router->group(
                    ['prefix' => 'details'],
                    function () use ($router) {
                        $router->get(
                            '/',
                            [
                            'as' => 'getInstructionDetails',
                            'uses' => 'InstructionDetailController@getAll'
                            ]
                        );
                
                        $router->put(
                            '/get',
                            [
                            'as' => 'getInstructionDetail',
                            'uses' => 'InstructionDetailController@getByParams'
                            ]
                        );
                
                        $router->post(
                            '/create',
                            [
                            'as' => 'createInstructionDetail',
                            'uses' => 'InstructionDetailController@create'
                            ]
                        );

                        $router->post(
                            '/update',
                            [
                            'as' => 'updateInstructionDetail',
                            'uses' => 'InstructionDetailController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy',
                            [
                            'as' => 'destroyInstructionDetail',
                            'uses' => 'InstructionDetailController@destroy'
                            ]
                        );
                    }
                );
            }
        );

        $router->group(
            ['prefix' => 'filters'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getFilters',
                    'uses' => 'FilterController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'updateFilter',
                    'uses' => 'FilterController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createFilter',
                    'uses' => 'FilterController@create'
                    ]
                );
            
                $router->post(
                    '/update',
                    [
                    'as' => 'updateFilter',
                    'uses' => 'FilterController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyFilter',
                    'uses' => 'FilterController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'areas'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'e@getAreas',
                    'uses' => 'AreaController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'e@getArea',
                    'uses' => 'AreaController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createArea',
                    'uses' => 'AreaController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateArea',
                    'uses' => 'AreaController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyArea',
                    'uses' => 'AreaController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'invoices'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getInvoices',
                    'uses' => 'InvoiceController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getInvoice',
                    'uses' => 'InvoiceController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createInvoice',
                    'uses' => 'InvoiceController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateInvoice',
                    'uses' => 'InvoiceController@update'
                    ]
                );
                
                // $router->post(
                //     '/attach',
                //     [
                //     'as' => 'attachInvoice',
                //     'uses' => 'InvoiceController@attach'
                //     ]
                // );
                //
                // $router->post(
                //     '/detach',
                //     [
                //     'as' => 'detachInvoice',
                //     'uses' => 'InvoiceController@detach'
                //     ]
                // );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyInvoice',
                    'uses' => 'InvoiceController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'discounts'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getDiscounts',
                    'uses' => 'DiscountController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getDiscount',
                    'uses' => 'DiscountController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createDiscount',
                    'uses' => 'DiscountController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateDiscount',
                    'uses' => 'DiscountController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyDiscount',
                    'uses' => 'DiscountController@destroy'
                    ]
                );

                $router->group(
                    ['prefix' => 'details'],
                    function () use ($router) {
                        $router->get(
                            '/',
                            [
                            'as' => 'getDiscountDetails',
                            'uses' => 'DiscountDetailController@getAll'
                            ]
                        );
                
                        $router->put(
                            '/get',
                            [
                            'as' => 'getDiscountDetail',
                            'uses' => 'DiscountDetailController@getByParams'
                            ]
                        );
                
                        $router->post(
                            '/create',
                            [
                            'as' => 'createDiscountDetail',
                            'uses' => 'DiscountDetailController@create'
                            ]
                        );

                        $router->post(
                            '/update',
                            [
                            'as' => 'updateDiscountDetail',
                            'uses' => 'DiscountDetailController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy',
                            [
                            'as' => 'destroyDiscountDetail',
                            'uses' => 'DiscountDetailController@destroy'
                            ]
                        );
                    }
                );
            }
        );

        $router->group(
            ['prefix' => 'invoice_items'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getInvoiceItems',
                    'uses' => 'InvoiceItemController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getInvoiceItem',
                    'uses' => 'InvoiceItemController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createInvoiceItem',
                    'uses' => 'InvoiceItemController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateInvoiceItem',
                    'uses' => 'InvoiceItemController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyInvoiceItem',
                    'uses' => 'InvoiceItemController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'invoice_type'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getInvoiceTypes',
                    'uses' => 'InvoiceTypeController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getInvoiceType',
                    'uses' => 'InvoiceTypeController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createInvoiceType',
                    'uses' => 'InvoiceTypeController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateInvoiceType',
                    'uses' => 'InvoiceTypeController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyInvoiceType',
                    'uses' => 'InvoiceTypeController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'usages'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getUsages',
                    'uses' => 'UsageController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getUsage',
                    'uses' => 'UsageController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createUsage',
                    'uses' => 'UsageController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateUsage',
                    'uses' => 'UsageController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyUsage',
                    'uses' => 'UsageController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'tanks'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getTanks',
                    'uses' => 'TankController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getTank',
                    'uses' => 'TankController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createTank',
                    'uses' => 'TankController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateTank',
                    'uses' => 'TankController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyTank',
                    'uses' => 'TankController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'accounts'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getAccounts',
                    'uses' => 'AccountController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getAccount',
                    'uses' => 'AccountController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createAccount',
                    'uses' => 'AccountController@create'
                    ]
                );
            
                $router->post(
                    '/update',
                    [
                    'as' => 'updateAccount',
                    'uses' => 'AccountController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyAccount',
                    'uses' => 'AccountController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'document_classes'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getDocumentClasses',
                    'uses' => 'DocumentClassController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getDocumentClass',
                    'uses' => 'DocumentClassController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createDocumentClass',
                    'uses' => 'DocumentClassController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateDocumentClass',
                    'uses' => 'DocumentClassController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyDocumentClass',
                    'uses' => 'DocumentClassController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'smart_accountings'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getSmartAccountings',
                    'uses' => 'SmartAccountingController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getSmartAccounting',
                    'uses' => 'SmartAccountingController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createSmartAccounting',
                    'uses' => 'SmartAccountingController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateSmartAccounting',
                    'uses' => 'SmartAccountingController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroySmartAccounting',
                    'uses' => 'SmartAccountingController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'accountings'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getAccountings',
                    'uses' => 'AccountingController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getAccounting',
                    'uses' => 'AccountingController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createAccounting',
                    'uses' => 'AccountingController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateAccounting',
                    'uses' => 'AccountingController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyAccounting',
                    'uses' => 'AccountingController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'siphons'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getSiphons',
                    'uses' => 'SiphonController@getAll'
                    ]
                );
            
                $router->put(
                    '/get',
                    [
                    'as' => 'getSiphon',
                    'uses' => 'SiphonController@getByParams'
                    ]
                );
            
                $router->post(
                    '/create',
                    [
                    'as' => 'createSiphon',
                    'uses' => 'SiphonController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateSiphon',
                    'uses' => 'SiphonController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroySiphon',
                    'uses' => 'SiphonController@destroy'
                    ]
                );
            }
        );
    
        $router->group(
            ['prefix' => 'usage_patterns'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getUsagePatterns',
                    'uses' => 'UsagePatternController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getUsagePattern',
                    'uses' => 'UsagePatternController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createUsagePattern',
                    'uses' => 'UsagePatternController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateUsagePattern',
                    'uses' => 'UsagePatternController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyUsagePattern',
                    'uses' => 'UsagePatternController@destroy'
                    ]
                );
            }
        );

        $router->group(
            ['prefix' => 'branch_diameters'],
            function () use ($router) {
                $router->get(
                    '/',
                    [
                    'as' => 'getAllBranchDiameter',
                    'uses' => 'BranchDiameterController@getAll'
                    ]
                );

                $router->put(
                    '/get',
                    [
                    'as' => 'getBranchDiameter',
                    'uses' => 'BranchDiameterController@getByParams'
                    ]
                );

                $router->post(
                    '/create',
                    [
                    'as' => 'createBranchDiameter',
                    'uses' => 'BranchDiameterController@create'
                    ]
                );

                $router->post(
                    '/update',
                    [
                    'as' => 'updateBranchDiameter',
                    'uses' => 'BranchDiameterController@update'
                    ]
                );

                $router->delete(
                    '/destroy',
                    [
                    'as' => 'destroyBranchDiameter',
                    'uses' => 'BranchDiameterController@destroy'
                    ]
                );

                $router->group(
                    ['prefix' => 'details'],
                    function () use ($router) {
                        $router->get(
                            '/',
                            [
                            'as' => 'getAllBranchDiameterDetail',
                            'uses' => 'BranchDiameterDetailController@getAll'
                            ]
                        );

                        $router->put(
                            '/get',
                            [
                            'as' => 'getBranchDiameterDetail',
                            'uses' => 'BranchDiameterDetailController@getByParams'
                            ]
                        );

                        $router->post(
                            '/create',
                            [
                            'as' => 'createBranchDiameterDetail',
                            'uses' => 'BranchDiameterDetailController@create'
                            ]
                        );

                        $router->post(
                            '/update',
                            [
                            'as' => 'updateBranchDiameterDetail',
                            'uses' => 'BranchDiameterDetailController@update'
                            ]
                        );

                        $router->delete(
                            '/destroy',
                            [
                            'as' => 'destroyBranchDiameterDetail',
                            'uses' => 'BranchDiameterDetailController@destroy'
                            ]
                        );
                    }
                );
            }
        );
    }
);
