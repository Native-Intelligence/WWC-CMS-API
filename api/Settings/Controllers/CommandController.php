<?php

namespace Api\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Settings\Requests\CommandRequest as REQ;
use Api\Settings\Services\CommandService as SERVICE;


class CommandController extends Controller
{

    public function __construct(SERVICE $service, REQ $request)
    {
        $this->service = $service;
        $this->request = $request;
    }



	public function command() {

		$this->request->commandValidation();
		return $this->response(
			app()->helper::jGenerate(
				$this->service->command($this->request)
			),
			201
		);

	}

}
