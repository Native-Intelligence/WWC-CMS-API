<?php
namespace Api\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Settings\Requests\GdataRequest as REQUEST;
use Api\Settings\Services\GdataService as SERVICE;

class GdataController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
