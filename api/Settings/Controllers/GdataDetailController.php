<?php
namespace Api\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Settings\Requests\GdataDetailRequest as REQUEST;
use Api\Settings\Services\GdataDetailService as SERVICE;

class GdataDetailController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
