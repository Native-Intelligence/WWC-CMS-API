<?php
namespace Api\Settings\Controllers;

use Infrastructure\Controllers\Controller;
use Api\Settings\Requests\RoleRequest as REQUEST;
use Api\Settings\Services\RoleService as SERVICE;

class RoleController extends Controller
{

    public function __construct(SERVICE $service, REQUEST $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
}
