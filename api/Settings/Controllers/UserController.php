<?php

namespace Api\Settings\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Controllers\Controller;
use Api\Settings\Requests\UserRequest as REQ;
use Api\Settings\Services\UserService as SERVICE;

class UserController extends Controller
{

    public function __construct(SERVICE $service, REQ $request)
    {
        $this->service = $service;
        $this->request = $request;
    }
    
    public function login(Request $header)
    {
        $this->request->loginValidation();
        
        return $this->response(
            app()->helper::jGenerate(
                $this->service->authenticated($this->request, $header)
            ),
            200
        );
    }

    public function isLogin()
    {
        return $this->response(
            app()->helper::jGenerate(
                $this->service->isLogin()
            ),
            200
        );
    }
    
    public function logout(Request $header)
    {
        return $this->response(
            app()->helper::jGenerate(
                $this->service->logout($header)
            ),
            200
        );
    }
}
