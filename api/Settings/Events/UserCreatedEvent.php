<?php

namespace Api\Settings\Events;

use Infrastructure\Events\Event;
use Api\Settings\Models\User;

class UserCreatedEvent extends Event
{

    public $userId;

    public function __construct(User $user)
    {
        $this->userId = $user->id;
    }
}
