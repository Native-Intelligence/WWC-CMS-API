<?php

namespace Api\Settings\Events;

use Infrastructure\Events\Event;
use Api\Settings\Models\User;

class UserWasUpdated extends Event
{

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
