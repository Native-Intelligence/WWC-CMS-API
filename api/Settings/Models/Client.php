<?php

namespace Api\Settings\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Infrastructure\Database\Eloquent\Model;

// use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
// use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Client extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                           'app_name',
                           'redirect',
                           'status',
                          ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['app_secret'];

    public function tokens()
    {
        return $this->hasMany('Api\Settings\Models\Token');
    }//end tokens()
}//end class
