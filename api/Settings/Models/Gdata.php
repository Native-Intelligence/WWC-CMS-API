<?php
namespace Api\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class Gdata extends Model
{

    protected $table = 'set_gdatas';

    protected $fillable = [
        'title',
        'slug',
        'tag',
        'status',
        'scope'
    ];
}
