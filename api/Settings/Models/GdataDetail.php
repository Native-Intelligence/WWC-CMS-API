<?php
namespace Api\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class GdataDetail extends Model
{

    protected $table = 'set_gdata_details';
    
    protected $view = 'set_gdata_details_view';

    protected $fillable = [
        'title',
        'value',
        'slug',
        'gdata_id',
        'status',
        'scope'
    ];
}
