<?php

namespace Api\Settings\Models;

// use Illuminate\Auth\Authenticatable;
// use Laravel\Lumen\Auth\Authorizable;
use Infrastructure\Database\Eloquent\Model;

// use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
// use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Role extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                           'slug',
                           'name',
                           'permissions',
                           'scope',
                           'status'
                          ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = ['permissions'];
    protected $casts = ['permissions' => 'object'];

    // public function tokens() {
    // return $this->hasMany('Api\Settings\Models\Token');
    // }
}//end class
