<?php

namespace Api\Settings\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Infrastructure\Database\Eloquent\Model;

// use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
// use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Token extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                           'user_id',
                           'client_id',
                           'status',
                           'access_token',
                           'refresh_token',
                           'access_expires_at',
                           'refresh_expires_at',
                          ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
                         'access_expires_at',
                         'refresh_expires_at',
                        ];

    /**
     * The attributes visible in the model's JSON form.
     *
     * @var array
     */
    protected $visible = [
                          'access_token',
                          'refresh_token',
                         ];

    public function user()
    {
        return $this->belongsTo('Api\Settings\Models\User');
    }
    
    public function client()
    {
        return $this->belongsTo('Api\Settings\Models\Client');
    }
}
