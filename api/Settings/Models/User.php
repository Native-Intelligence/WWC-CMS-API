<?php
namespace Api\Settings\Models;

use Infrastructure\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = [
                           'full_name',
                           'email',
                           'mobile',
                           'username',
                           'password',
                           'security_code',
                           'status',
                           'access',
                           'scope'
                          ];
                          
    protected $hidden = ['password'];

    protected $casts = [
                        'access' => 'object',
                       ];

    public function tokens()
    {
        return $this->hasMany('Api\Settings\Models\Token');
    }

    public function clients()
    {
        return $this->belongsToMany(
            'Api\Settings\Models\Client',
            'tokens',
            'user_id',
            'client_id'
        );
    }
    
    public function setPasswordAttribute($value)
    {
        $hasher = app()->make('hash');
        $this->attributes['password'] = $hasher->make($value);
    }
}
