<?php
namespace Api\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Settings\Models\User;

class ClientRepository extends Repository
{

    public function model()
    {
        return 'Api\\Settings\\Models\\Client';
    }
}
