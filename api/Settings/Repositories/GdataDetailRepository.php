<?php
namespace Api\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Settings\Models\GdataDetail;

class GdataDetailRepository extends Repository
{

    public function model()
    {
        return 'Api\\Settings\\Models\\GdataDetail';
    }
}
