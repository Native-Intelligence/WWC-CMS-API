<?php
namespace Api\Settings\Repositories;

use Infrastructure\Database\Eloquent\Repository;
use Api\Settings\Models\Gdata;

class GdataRepository extends Repository
{

    public function model()
    {
        return 'Api\\Settings\\Models\\Gdata';
    }
}
