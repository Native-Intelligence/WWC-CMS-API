<?php
namespace Api\Settings\Repositories;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use Infrastructure\Database\Eloquent\Repository;
use Api\Settings\Models\Role;

class RoleRepository extends Repository
{

    public function model()
    {
        return 'Api\\Settings\\Models\\Role';
    }

    public function getRoleBySlug(string $username)
    {
        return $this->findByField('username', $username)->first();
    }
    
    public function getRoleById(int $id)
    {
        return $this->find($id)->first();
    }

    public function getRolePermissions($user, $appName)
    {
        return $user->clients->where('app_name', $appName)->first();
    }

    public function getUserToken($client)
    {
        return $client->tokens->first();
    }
}
