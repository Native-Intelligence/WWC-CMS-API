<?php
namespace Api\Settings\Repositories;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use Infrastructure\Database\Eloquent\Repository;
use Api\Settings\Models\Token;

class TokenRepository extends Repository
{

    public function model()
    {
        return 'Api\\Settings\\Models\\Token';
    }

    public function getToken(string $token)
    {
        return $this->findByField('access_token', $token)->first();
    }

    public function getUserByToken($token)
    {
        return $token->user()->first();
    }
    
    public function getClientByToken($token)
    {
        return $token->client()->first();
    }

    public function setToken(array $attributes, $token)
    {
        $this->applyScope();

        if (!is_null($this->validator)) {
            $this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }

        $_skipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        $model = $token;
        $model->fill($attributes);
        $model->save();

        $this->skipPresenter($_skipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
        // $post = $this->repository->update( Input::all(), $id );
    }
}
