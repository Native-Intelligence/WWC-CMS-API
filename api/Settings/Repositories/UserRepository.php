<?php
namespace Api\Settings\Repositories;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use Infrastructure\Database\Eloquent\Repository;
use Api\Settings\Models\User;

class UserRepository extends Repository
{

    public function model()
    {
        return 'Api\\Settings\\Models\\User';
    }

    public function getUserByUsername(string $username)
    {
        return $this->findByField('username', $username)->first();
    }
    
    public function getUserById(int $id)
    {
        return $this->find($id)->first();
    }

    public function getUserClient($user, $appName)
    {
        return $user->clients->where('app_name', $appName)->first();
    }

    public function getUserToken($client)
    {
        return $client->tokens->first();
    }

    public function setUserToken(array $attributes, $client)
    {
        // return $client->tokens->first();
        $this->applyScope();

        if (!is_null($this->validator)) {
            $this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }

        $_skipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        $model = $this->getUserToken($client);
        $model->fill($attributes);
        $model->save();

        $this->skipPresenter($_skipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
        // $post = $this->repository->update( Input::all(), $id );
    }
}
