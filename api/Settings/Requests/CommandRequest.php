<?php
namespace Api\Settings\Requests;

use Infrastructure\Requests\Request;

class CommandRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('command');
    }



	public function commandValidation( ) {

			$this->rules = [
				'command' => 'required',
				'operator'     => 'required'
			];
			$this->validate( $this->data, $this->rules );

	}

}
