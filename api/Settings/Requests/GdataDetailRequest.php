<?php
namespace Api\Settings\Requests;

use Infrastructure\Requests\Request;

class GdataDetailRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('gdata_detail');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'value' => 'string|nullable',
            'slug' => 'string|nullable',
            'gdata_id' => 'required|integer',
            // 'gdata_title' => 'required|string',
            'status' => 'integer',
            'scope' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'value' => 'string',
            'slug' => 'string|nullable',
            'gdata_id' => 'sometimes|required|integer',
            // 'gdata_title' => 'sometimes|required|string',
            'status' => 'integer',
            'scope' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
