<?php
namespace Api\Settings\Requests;

use Infrastructure\Requests\Request;

class GdataRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('gdata');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'title' => 'required|string',
            'slug' => 'required|string',
            'tag' => 'string',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'title' => 'sometimes|required|string',
            'slug' => 'sometimes|required|string',
            'tag' => 'string',
            'scope' => 'sometimes|required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
