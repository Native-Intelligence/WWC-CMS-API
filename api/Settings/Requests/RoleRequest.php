<?php
namespace Api\Settings\Requests;

use Infrastructure\Requests\Request;

class RoleRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('role');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'slug' => 'required|string',
            'name' => 'required|string',
            'permissions' => 'array',
            'scope' => 'required|integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'slug' => 'sometimes|required|string',
            'name' => 'sometimes|required|string',
            'permissions' => 'array',
            'scope' => 'sometimes|required|integer',
            'status' => 'integer'
        ];
        $this->validate($this->data, $this->rules);
    }
}
