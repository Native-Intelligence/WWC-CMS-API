<?php
namespace Api\Settings\Requests;

use Infrastructure\Requests\Request;

class UserRequest extends Request
{

    public function __construct(Request $request)
    {
        $this->data = $request->json('user');
    }
    
    public function createValidation()
    {
        $this->rules = [
            'username' => 'required|string',
            // 'password' => 'string|min:4',
            'full_name' => 'required|string',
            'access' => 'required|array',
            'mobile' => 'required|string',
            'email' => 'string',
            'scope' => 'required|integer'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        $this->rules = [
            'id' => 'required|integer',
            'username' => 'sometimes|required|string',
            'mobile' => 'sometimes|required|string',
            'password' => 'string|min:4',
            'password_confirm' => 'string|min:4',
            'full_name' => 'sometimes|required|string',
            'access' => 'sometimes|required|array',
            'email' => 'string',
            'status' => 'integer',
            'scope' => 'sometimes|required|integer',
        'security_code'        =>'sometimes|required|integer',
        'new_password'        =>'sometimes|required|string|min:4|max:20',
        'new_password_confirm'=>'sometimes|required|string|min:4|max:20',

        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function loginValidation()
    {
        $this->rules = [
            'username' => 'required|string',
            'password' => 'required|string|min:4'
        ];
        $this->validate($this->data, $this->rules);
    }
}
