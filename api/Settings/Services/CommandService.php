<?php

namespace Api\Settings\Services;

use Infrastructure\Services\Service;

use Illuminate\Support\Facades\Artisan;


class CommandService extends Service {

	public function __construct() {
		$this->key        = 'command';
		$this->uniqueKey  = '';

	}


	public function command( $data ) {
		$this->checkUniqueKeyExist( $data );
		return Artisan::call( $this->data->command . ':' . $this->data->operator );

	}

}
