<?php
namespace Api\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Settings\Repositories\GdataDetailRepository;

class GdataDetailService extends Service
{

    public function __construct(GdataDetailRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'gdata_detail';
        $this->uniqueKey = '';
    }

    public function getAll()
    {
        return $this->getAllWithoutPaginate()->all();
    }
}
