<?php
namespace Api\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Settings\Repositories\GdataRepository;

class GdataService extends Service
{

    public function __construct(GdataRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'gdata';
        $this->uniqueKey = 'title';
    }
}
