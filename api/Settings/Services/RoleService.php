<?php
namespace Api\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use Api\Settings\Repositories\RoleRepository;

class RoleService extends Service
{

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
        $this->key = 'role';
        $this->uniqueKey = 'slug';
    }
}
