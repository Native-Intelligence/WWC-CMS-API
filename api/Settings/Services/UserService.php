<?php
namespace Api\Settings\Services;

use Infrastructure\Services\Service;
use Infrastructure\Exceptions\Exception;
use DateTime;
//use Artisan;
use Illuminate\Support\Facades\Artisan;

use Illuminate\Support\Facades\Hash;
use Api\Settings\Repositories\UserRepository;
use Api\Settings\Repositories\TokenRepository;

class UserService extends Service
{

    private $_tokenRepository;

    public function __construct(UserRepository $repository, TokenRepository $tokenRepository)
    {
        $this->repository = $repository;
        $this->key = 'user';
        $this->uniqueKey = 'username';
        $this->_tokenRepository = $tokenRepository;
        $this->event = [
            'created' => ['Api\Settings\Events\UserCreatedEvent']
            // 'created' => ['Api\Sales\Events\CartableChangeEvent', 'Api\Settings\Events\UserCreatedEvent']
        ];
    }

    public function getData($data)
    {
        $this->data = (array) parent::getData($data);
        
        // dd($this->data->access['permissions']);
        if (!empty($this->data['access']['permissions'])) {
            $this->data['access->permissions'] = $this->data['access']['permissions'];
        }
        if (!empty($this->data['access']['roles'])) {
            $this->data['access->roles'] = $this->data['access']['roles'];
        }
        
        if (!empty($this->data['access']['scope'])) {
            $this->data['access->scope'] = $this->data['access']['scope'];
        }
        
        if (!empty($this->data['access']['workflow'])) {
            $this->data['access->workflow'] = $this->data['access']['workflow'];
        }

        unset($this->data['access']);
        
        return (object) $this->data;
    }
    
    public function authenticated($data, $header)
    {
        $this->_data = (object) $data->json('user');
        $user = $this->repository->with(['tokens', 'clients'])->findByField('username', $this->_data->username)->first();
        
        if (!is_null($user)
            and Hash::check($this->_data->password, $user->password)
        ) {
            if (in_array($user->status, app()->helper::$blockStatus)) {
                throw new Exception(40123);
            }
            $client = $user->clients->where('app_name', $header->header('app-name'))->first();
            // $client = $this->repository->getUserClient($user, $header->header('app-name'));
            if (!is_null($client)
                and Hash::check($header->header('app-secret'), $client->app_secret)
            ) {
                $token = $user->tokens->where('client_id', $client->id)->first();
                $token->update(app()->userHelper::CreateUniqueToken($user->id));
                
                return $user;
            }

            throw new Exception(40122);
        }
        
        throw new Exception(40121);
    }
    
    public function isLogin()
    {
        if (is_null(app()->userHelper::$currentUser)) {
            return false;
        }
        
        return app()->userHelper::$currentUser;
    }
    
    public function logout($header)
    {
        $accessToken = $header->header('access-token');
        $refreshToken = $header->header('refresh-token');

        $token = $this->_tokenRepository->findWhere(
            ['access_token' => $accessToken, 'refresh_token' => $refreshToken]
        )->first();
        
        if (!is_null($token)) {
            $now = new DateTime();
            $this->_tokenRepository->update(
                ['access_expires_at' => $now, 'refresh_expires_at' => $now],
                $token->id
            );

            return true;
        }

        return false;
    }
    
    public function create($data)
    {
        $this->uniqueKey = 'username';
        $this->data = (object) $data->json($this->key);
        $this->exist = $this->repository->findByField($this->uniqueKey, $this->data->{$this->uniqueKey})->first();
        $pass=rand(1000, 9999);
        $this->data->password=$pass;
        if (is_null($this->exist)) {
            $result = $this->repository->create((array) $this->data);
            
            if (isset($this->event['created'])) {
                foreach ($this->event['created'] as $val) {
                    event(new $val($result));
                }
            }
            
            $exitCode = Artisan::call(
                'sms:send', [
                'text' => $pass,
                'number' => [$result->mobile]
                ]
            );
            return $result;
        }

        throw new Exception(40640);
    }
    
    public function update($data)
    {
        $this->uniqueKey = '';
        // dd($data);
        $this->checkUniqueKeyExist($data);
        $user=app()->userHelper::$currentUser;
        
        if(in_array(
            '1',
            array_map(
                function ($v) {
                        return $v->id;
                },
                $user->access->roles
            )
        ) and $user->id != $this->data->id
        ) {
            $data = $this->data;
        }elseif(isset($this->data->security_code)) {
            if(!$user->security_code==$this->data->security_code or !$user->id == $this->data->id) {
                throw new Exception(40123);
            }
            $this->data->security_code = null;
            // $this->data->id = $user->id;
            $data = $this->data;
        }else{
            $data = null;
            $data['security_code'] =  rand(1000, 9999);
            $data['id'] =  $this->data->id;

            Artisan::call(
                'sms:send', [
                    'text'  =>trans('message.10001').$data['security_code'],
                    'number'=>[$user->mobile]
                ]
            );
        }

        parent::update((array)$data);
    }
}
