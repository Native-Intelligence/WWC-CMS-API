<?php
$router->group(['prefix' => 'settings', 'middleware' => ['cors', 'auth']], function () use ($router) {
    $router->group(['prefix' => 'gdata'], function () use ($router) {
        $router->get('/', [
            'as' => 'e@getGdatas',
            'uses' => 'GdataController@getAll'
        ]);
        
        $router->put('/get', [
            'as' => 'e@getGdata',
            'uses' => 'GdataController@getByParams'
        ]);

        $router->post('/create', [
            'as' => 'createGdata',
            'uses' => 'GdataController@create'
        ]);
        
        $router->post('/update', [
            'as' => 'updateGdata',
            'uses' => 'GdataController@update'
        ]);

        $router->delete('/destroy', [
            'as' => 'destroyGdata',
            'uses' => 'GdataController@destroy'
        ]);
        
        $router->group(['prefix' => 'details'], function () use ($router) {
            $router->get('/', [
                'as' => 'e@getGdataGroups',
                'uses' => 'GdataDetailController@getAll'
            ]);
            
            $router->put('/get', [
                'as' => 'e@getGdataGroup',
                'uses' => 'GdataDetailController@getByParams'
            ]);

            $router->post('/create', [
                'as' => 'createGdataGroup',
                'uses' => 'GdataDetailController@create'
            ]);
            
            $router->post('/update', [
                'as' => 'updateGdataGroup',
                'uses' => 'GdataDetailController@update'
            ]);

            $router->delete('/destroy', [
                'as' => 'destroyGdataGroup',
                'uses' => 'GdataDetailController@destroy'
            ]);
        });
    });

	$router->post( '/command', [
		'as'   => 'command',
		'uses' => 'commandController@command'
	] );

});

$router->group(['prefix' => 'users', 'middleware' => ['cors']], function () use ($router) {
    $router->post('/login', [
        'as' => 'e@login',
        'uses' => 'UserController@login'
    ]);
    
    
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('/create', [
            'as' => 'register',
            'uses' => 'UserController@create'
        ]);
    
        $router->get('/is_login', [
            'as' => 'e@isLogin',
            'uses' => 'UserController@isLogin'
        ]);

        $router->get('/logout', [
            'as' => 'e@logout',
            'uses' => 'UserController@logout'
        ]);

        $router->get('/', [
            'as' => 'getUsers',
            'uses' => 'UserController@getAll'
        ]);
        
        $router->put('/get', [
            'as' => 'getUser',
            'uses' => 'UserController@getByParams'
        ]);
        
        $router->post('/update', [
            'as' => 'updateUser',
            'uses' => 'UserController@update'
        ]);
        
        $router->delete('/destroy', [
            'as' => 'destroyUser',
            'uses' => 'UserController@destroy'
        ]);
        // $router->post('/', 'UserController@create');
        $router->group(['prefix' => 'role'], function () use ($router) {
            $router->get('/', [
                'as' => 'getRoles',
                'uses' => 'RoleController@getAll'
            ]);
            
            $router->put('/get', [
                'as' => 'getRole',
                'uses' => 'RoleController@getByParams'
            ]);
            
            $router->post('/create', [
                'as' => 'createRole',
                'uses' => 'RoleController@create'
            ]);
            
            $router->post('/update', [
                'as' => 'updateRole',
                'uses' => 'RoleController@update'
            ]);
            
            $router->delete('/destroy', [
                'as' => 'destroyRole',
                'uses' => 'RoleController@destroy'
            ]);
        });
    });

    // $router->get('/users/{id}', 'UserController@getByParams');
    // $router->put('/users/{id}', 'UserController@update');
    // $router->delete('/users/{id}', 'UserController@delete');

    // $router->get('/', function () use ($router) {
    //     return $router->version();
    // });
});
