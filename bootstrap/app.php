<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
}

/*
    |--------------------------------------------------------------------------
    | Create The Application
    |--------------------------------------------------------------------------
    |
    | Here we will load the environment and create the application instance
    | that serves as the central piece of this framework. We'll use this
    | application as an "IoC" container and router for this framework.
    |
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->instance('path.storage', app()->basePath() . DIRECTORY_SEPARATOR . 'storage');

$app->configure('cors');

$app->withFacades();

$app->withEloquent();
// dd($app['config']->get('cors.allowedOrigins'));
/*
    |--------------------------------------------------------------------------
    | Register Container Bindings
    |--------------------------------------------------------------------------
    |
    | Now we will register a few bindings in the service container. We will
    | register the exception handler and the console kernel. You may add
    | your own bindings here if you like or you can make another file.
    |
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    Infrastructure\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    Infrastructure\Console\Kernel::class
);

/*
    |--------------------------------------------------------------------------
    | Register Middleware
    |--------------------------------------------------------------------------
    |
    | Next, we will register the middleware with the application. These can
    | be global middleware that run before and after each request into a
    | route or middleware that'll be assigned to some specific routes.
    |
*/

// $app->middleware([
// App\Http\Middleware\ExampleMiddleware::class
// ]);
$app->routeMiddleware(
    [
     'auth' => Infrastructure\Middleware\Authenticate::class,
     'cors' => \Barryvdh\Cors\HandleCors::class,
    ]
);

/*
    |--------------------------------------------------------------------------
    | Register Service Providers
    |--------------------------------------------------------------------------
    |
    | Here we will register all of the application's service providers which
    | are used to bind services into the container. Service providers are
    | totally optional, so you are not required to uncomment this line.
    |
*/

// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);
$app->register(Infrastructure\Providers\ApiServiceProvider::class);
$app->register(Infrastructure\Providers\EventServiceProvider::class);
$app->register(Barryvdh\Cors\LumenServiceProvider::class);
// $app->register(Barryvdh\Cors\ServiceProvider::class);
$app->register(Appzcoder\LumenRoutesList\RoutesCommandServiceProvider::class);

$app->register(Prettus\Repository\Providers\LumenRepositoryServiceProvider::class);
$app->register(Morilog\Jalali\JalaliServiceProvider::class);

// $app->register(Mpociot\ApiDoc\ApiDocGeneratorServiceProvider::class);

/*
    |--------------------------------------------------------------------------
    | Load The Application Routes
    |--------------------------------------------------------------------------
    |
    | Next we will include the routes file so that they can all be added to
    | the application. This will provide all of the URLs the application
    | can respond to, as well as the controllers that may handle them.
    |
*/
// $app->router->group([
//     'namespace' => 'App\Http\Controllers',
// ], function ($router) {
//     require __DIR__.'/../routes/web.php';
// });

// $app->router->group(
//     ['namespace' => 'Api\Settings\Controllers'],
//     function ($router) {
//         require __DIR__.'/../api/Users/routes.php';
//     }
// );

$app->router->group(
    ['namespace' => 'Api\Settings\Controllers'],
    function ($router) {
        include __DIR__.'/../api/Settings/routes.php';
    }
);

$app->router->group(
    ['namespace' => 'Api\Reports\Controllers'],
    function ($router) {
        include __DIR__.'/../api/Reports/routes.php';
    }
);

// $app->router->group(
//     ['namespace' => 'Api\Sales\Settings\Controllers'],
//     function ($router) {
//         require __DIR__.'/../api/Sales/Settings/routes.php';
//     }
// );

$app->router->group(
    ['namespace' => 'Api\Sales'],
    function ($router) {
        include __DIR__.'/../api/Sales/routes.php';
    }
);
$app->router->group(
    ['namespace' => 'Api\Cartables'],
    function ($router) {
        include __DIR__.'/../api/Cartables/routes.php';
    }
);

// $app->router->group(
//     ['namespace' => 'Api\Ass\Controllers'],
//     function ($router) {
//         include __DIR__.'/../api/Ass/routes.php';
//     }
// );

$app->router->group(
    ['namespace' => 'Api\Acc'],
    function ($router) {
        include __DIR__.'/../api/Acc/routes.php';
    }
);

return $app;
