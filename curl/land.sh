#!/bin/bash

source ./login.sh

saleReq=$(curl --request POST \
  --url $URL/sales/request/create \
  --header "access-token: $accessToken" \
  --header 'app-name: test' \
  --header 'app-secret: test' \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --header "refresh-token: $refreshToken" \
  --data '{"request":{"survey_date":"'"$NOW"'","coordination_date":"'"$TOMORROW"'","user_id":"'"$userId"'","land_id":1,"scope":444}}' | jq ".")
echo $saleReq
echo $userId

