#!/bin/bash

if [ -z ${URL+x} ]; then
URL=http://127.0.0.1:8009
NOW=$(date +"%Y-%m-%d %H:%M:%S")
TOMORROW=$(date +"%Y-%m-%d %H:%M:%S" --date='1 day')
login=$(curl --request POST \
  --url $URL/users/login \
  --header 'app-name: test' \
  --header 'app-secret: test' \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --header 'postman-token: 1d6f4be4-259e-fc7d-26b7-dbf9cb741aef' \
	--data '{"user" : {"username":"admin", "password": "1234"  }}')
result=$(jq -r '.result' <<< "${login}")
loginStatus=$(jq -r '.status' <<< "${login}")
userId=$(jq -r '.id' <<< "${result}")
if [ "$loginStatus" != "true" ]
then
  echo $loginStatus
else
  accessToken=$(jq -r '.tokens[0].access_token' <<< "${result}")
  refreshToken=$(jq -r '.tokens[0].refresh_token' <<< "${result}")

  echo $accessToken 
  echo $refreshToken 
fi
fi
