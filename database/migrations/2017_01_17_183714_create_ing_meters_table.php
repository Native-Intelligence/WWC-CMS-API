<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngMetersTable extends Migration
{

    public function up()
    {
        Schema::create('ing_meters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('g_class_name_id');
            $table->unsignedInteger('g_model_name_id');
            $table->unsignedInteger('g_factory_name_id');
            $table->unsignedInteger('start_point')->nullable();
            $table->unsignedInteger('digits_number');
            $table->float('diameter');
            $table->float('diameter_in');
            $table->float('diameter_out');
            $table->unsignedInteger('percentage_error');
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedBigInteger('scope');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_meters');
    }
}
