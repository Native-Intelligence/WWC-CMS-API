<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngAreasTable extends Migration
{

    public function up()
    {
        Schema::create(
            'ing_areas',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('code')->nullable();
                // $table->string('company_code')->nullable();
                $table->string('display_name')->nullable();
                $table->unsignedInteger('g_area_pattern_id')->nullable();
//                $table->unsignedInteger('scope_max');
                $table->unsignedInteger('parent_id')->nullable();
                
                $table->unsignedTinyInteger('type')->default(0);
                $table->unsignedBigInteger('scope')->nullable();
                $table->unsignedBigInteger('scope_max')->nullable();
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
        DB::update('ALTER TABLE ing_areas AUTO_INCREMENT = '.app()->helper::$autoIncrement.';');
    }

    public function down()
    {
        Schema::drop('ing_areas');
    }
}
