<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngTanksTable extends Migration
{

    public function up()
    {
        Schema::create('ing_tanks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('title');
            $table->string('address');
            $table->json('areas');
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedBigInteger('scope');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_tanks');
    }
}
