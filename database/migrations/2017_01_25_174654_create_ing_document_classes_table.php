<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngDocumentClassesTable extends Migration
{

    public function up()
    {
        Schema::create('ing_document_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedInteger('scope');
            $table->unsignedInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_document_classes');
    }
}
