<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngSiphonsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_siphons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('status')->default(0);
            $table->unsignedBigInteger('scope');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_siphons');
    }
}
