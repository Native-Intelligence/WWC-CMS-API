<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngDiscountsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->unsignedInteger('g_discount_type_id')->index();
            $table->string('code', 30);
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->unsignedInteger('type');
            $table->unsignedInteger('usage_id');
            // $table->unsignedTinyInteger('percent');
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_discounts');
    }
}
