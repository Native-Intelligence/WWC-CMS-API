<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngDiscountDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_discount_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discount_id');
            $table->integer('formula_id');
            $table->integer('invoice_item_id');
            $table->string('pecuniary_percent');
            $table->string('installment_percent');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_discount_details');
    }
}
