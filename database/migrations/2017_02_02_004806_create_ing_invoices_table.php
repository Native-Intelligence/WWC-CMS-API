<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngInvoicesTable extends Migration
{

    public function up()
    {
        Schema::create(
            'ing_invoices',
            function (Blueprint $table) {
                $table->increments('id')->index();
                $table->unsignedInteger('usage_id')->index();
                 $table->unsignedInteger('filter_id')->index();
                $table->unsignedInteger('formula_id')->nullable()->index();
                $table->unsignedInteger('instruction_id')->index();
                $table->unsignedTinyInteger('type')->default(0);
                $table->unsignedInteger('expense_id');
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->timestamps();
                // $table->foreign('usage_id')->references('id')->on('ing_usages');
                // $table->foreign('filter_id')->references('id')->on('ing_filters');
                // $table->foreign('formula_id')->references('id')->on('ing_filters');
                // $table->foreign('instruction_id')->references('id')->on('ing_instructions');
                // $table->foreign('invoice_item_id')->references('id')->on('ing_invoice_items');
            }
        );
    }

    public function down()
    {
        Schema::drop('ing_invoices');
    }
}
