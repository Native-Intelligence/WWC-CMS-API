<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngInvoiceItemsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'ing_invoice_items',
            function (Blueprint $table) {
                $table->increments('id')->index();
                $table->string('title');
                $table->string('slug');
                // $table->unsignedInteger('invoice_type_id')->index();
                $table->string('account_id')->nullable()->index();
                $table->string('third_party_id')->nullable();
                $table->boolean('is_partition')->default(0);
                $table->boolean('is_journal_doc')->default(0);
                $table->unsignedInteger('filter_id')->nullable()->index();
                $table->unsignedInteger('formula_id')->nullable()->index();
                $table->unsignedTinyInteger('partition_percent');
                $table->unsignedTinyInteger('partition_count')->nullable();
	            $table->integer('step_id')->nullable();
                $table->boolean('check_paid');
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->timestamps();
                // $table->foreign('invoice_type_id')->references('id')->on('ing_invoice_types');
                // $table->foreign('account_id')->references('id')->on('ing_accounts');
                // $table->foreign('filter_id')->references('id')->on('ing_filters');
                // $table->foreign('formula_id')->references('id')->on('ing_filters');
            }
        );
    }

    public function down()
    {
        Schema::drop('ing_invoice_items');
    }
}
