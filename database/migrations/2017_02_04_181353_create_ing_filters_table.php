<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngFiltersTable extends Migration
{

    public function up()
    {
        Schema::create('ing_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('filter');
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_filters');
    }
}
