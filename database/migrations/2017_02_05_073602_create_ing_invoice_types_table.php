<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngInvoiceTypesTable extends Migration
{

    public function up()
    {
        Schema::create('ing_invoice_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedTinyInteger('type');
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_invoice_types');
    }
}
