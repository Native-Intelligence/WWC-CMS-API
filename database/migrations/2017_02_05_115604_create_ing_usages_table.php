<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngUsagesTable extends Migration
{

    public function up()
    {
        Schema::create('ing_usages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedInteger('code');
            $table->string('receivable_code')->nullable();
            $table->string('wastewater_code')->nullable();
            $table->string('water_code')->nullable();
            $table->string('wastewater_document_code')->nullable();
            $table->string('water_document_code')->nullable();
            $table->unsignedInteger('usage_id')->index()->nullable();
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->foreign('usage_id')->references('id')->on('ing_usages');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_usages');
    }
}
