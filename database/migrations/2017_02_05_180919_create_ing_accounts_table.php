<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngAccountsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedTinyInteger('account_type');
            $table->string('account_number');
            $table->boolean('is_identifier');
            $table->string('identifier');
            $table->unsignedTinyInteger('g_bank_id');
            $table->string('bank_branch');
            $table->string('bank_branch_code');
            $table->string('accounting_code');
	        $table->char('company_code', 3)->nullable();
	        $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedBigInteger('scope');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_accounts');
    }
}
