<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngAccountingTable extends Migration
{

    public function up()
    {
        Schema::create('ing_accountings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('document_no');
            $table->timestamp('document_date');
            $table->boolean('is_confirmed');
            $table->unsignedInteger('request_id');
            $table->unsignedInteger('invoice_id')->index();
            $table->unsignedInteger('document_class_id')->index();
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
            $table->foreign('invoice_id')->references('id')->on('ing_invoices');
            $table->foreign('document_class_id')->references('id')->on('ing_document_classes');
        });
    }

    public function down()
    {
        Schema::drop('ing_accountings');
    }
}
