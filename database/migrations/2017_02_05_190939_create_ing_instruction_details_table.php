<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngInstructionDetailsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'ing_instruction_details', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('slug');
                $table->float('value');
                $table->unsignedInteger('instruction_id ');
                $table->unsignedInteger('invoice_item_id');
                $table->boolean('is_home');
                $table->json('usage_id');
                $table->unsignedInteger('g_branch_diameter_id')->nullable();
                $table->unsignedInteger('g_siphon_id')->nullable();
                $table->unsignedInteger('g_type_installation_id')->nullable();
                $table->unsignedInteger('g_land_type_id')->nullable();
                $table->json('unit_range')->nullable();
                $table->json('consumption_range')->nullable();
                $table->json('arena_range')->nullable();
                $table->json('foundation_range')->nullable();
                $table->json('g_tank_id')->nullable();
                $table->unsignedBigInteger('scope_city');
                $table->json('scopes');
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('ing_instruction_details');
    }
}
