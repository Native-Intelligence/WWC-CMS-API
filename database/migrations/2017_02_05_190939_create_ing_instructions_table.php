<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngInstructionsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_instructions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamp('start_date')->default('0000-00-00 00:00:00');
            $table->timestamp('end_date')->default('0000-00-00 00:00:00');
            $table->timestamp('effective_date')->default('0000-00-00 00:00:00');
            $table->string('adviser');
            $table->timestamp('advise_date')->default('0000-00-00 00:00:00');
            $table->string('advising_organization');
            $table->string('advising_position');
            $table->json('base_rate');
            $table->json('factors');
            $table->string('indicator_number');
            $table->timestamp('indicator_date')->default('0000-00-00 00:00:00');
            $table->unsignedInteger('instruction_type');
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_instructions');
    }
}
