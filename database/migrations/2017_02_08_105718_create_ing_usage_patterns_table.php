<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngUsagePatternsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'ing_usage_patterns', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->unsignedInteger('code');
                $table->unsignedInteger('g_unit_id');
                $table->float('value', 8, 3);
                $table->text('description');
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ing_usage_patterns');
    }
}
