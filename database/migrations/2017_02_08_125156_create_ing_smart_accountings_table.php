<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngSmartAccountingsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_smart_accountings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('periority')->default(0);
            $table->unsignedInteger('document_class_id')->index();
            $table->json('details')->nullable();
            $table->unsignedInteger('status')->default(0);
            $table->unsignedBigInteger('scope')->nullable();
            $table->timestamps();
            $table->foreign('document_class_id')->references('id')->on('ing_document_classes');
        });
    }

    public function down()
    {
        Schema::drop('ing_smart_accountings');
    }
}
