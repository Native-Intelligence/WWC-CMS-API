<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalEvaluationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_evaluations',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('request_id');
                $table->json('branch_info');
                $table->json('gis_info');
                $table->json('other_info');
	            $table->dateTime('review_date');
	            $table->text('description')->nullable();
	            $table->unsignedInteger('status')->default(0);
	            $table->bigInteger('scope')->nullable();
                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_technical_evaluations');
    }
}
