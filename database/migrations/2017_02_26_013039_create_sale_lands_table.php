<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleLandsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'sale_lands',
            function (Blueprint $table) {
                $table->increments('id')->index();
                $table->string('municipality_number');
                $table->string('registration_number');
                $table->string('postal_code');
                $table->string('cadastre_code');
                $table->string('file_number')->nullable();
                $table->string('subscription_number')->nullable();
                $table->text('address');
                $table->json('coordination');
                $table->unsignedInteger('customer_number')->nullable();
                $table->float('arena');
                $table->float('foundation');
                $table->unsignedInteger('usage_id')->index();
                $table->unsignedTinyInteger('g_certificate_type_id')->index();
                $table->unsignedTinyInteger('g_evidence_type_id')->index();
                $table->unsignedInteger('g_tank_id')->index();
                $table->unsignedTinyInteger('g_land_type_id')->index();
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->unsignedBigInteger('scope_city');
                $table->json('persons_id');
                $table->foreign('usage_id')->references('id')->on('ing_usages');
                /*
                * lands_code
                * */
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('sale_lands');
    }
}
