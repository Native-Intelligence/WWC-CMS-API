<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalePersonsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_persons',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('full_name');
                $table->json('contact_info');
                $table->string('identity_number');
                $table->boolean('person_type');
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->json('person_details');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_persons');
    }
}
