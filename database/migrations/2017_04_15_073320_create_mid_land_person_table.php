<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMidLandPersonTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mid_land_person', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('land_id')->index();
            $table->unsignedInteger('person_id')->index();
            $table->unsignedInteger('owner_relationship_applicant');
            $table->timestamps();
            $table->foreign('land_id')->references('id')->on('sale_lands');
            $table->foreign('person_id')->references('id')->on('sale_persons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mid_land_person');
    }
}
