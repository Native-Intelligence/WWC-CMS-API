<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccDocDetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'acc_doc_dets',
            function (Blueprint $table) {
                $table->increments('id')->index();
                $table->string('title');
                // $table->unsignedTinyInteger('type');
                $table->unsignedInteger('document_id')->index();
                $table->unsignedInteger('type')->default(0);
                $table->string('acc_id')->index();
                // $table->string('acc_grp_id')->index();
                // $table->string('acc_det_id')->index();
                // $table->json('det_acc_ids');
                // $table->json('det_acc_ids')->index();
                $table->unsignedInteger('amount');
                $table->unsignedBigInteger('scope');
                // $table->json('details');
                $table->unsignedInteger('level_4')->nullable();
                $table->unsignedInteger('level_5')->nullable();
                $table->unsignedInteger('level_6')->nullable();
                
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acc_doc_dets');
    }
}
