<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccDocsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'acc_docs',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->dateTime('document_date');
                $table->unsignedInteger('document_number');
                $table->unsignedInteger('g_document_type_id');
	            $table->boolean('is_manual')->default(0);
	            // $table->json('details');
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acc_docs');
    }
}
