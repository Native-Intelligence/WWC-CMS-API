<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingPrePaymentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'accounting_pre_payments', function (Blueprint $table) {
                $table->increments('id')->index();
                $table->unsignedInteger('amount');
                $table->dateTime('pre_payment_date');
                $table->unsignedInteger('pre_payment_number');
                $table->unsignedInteger('document_id')->index();
                $table->unsignedInteger('payment_id');
                $table->unsignedInteger('recipt_id');
                $table->boolean('payment_type');
                $table->json('pre_payment_detail');
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounting_pre_payments');
    }
}
