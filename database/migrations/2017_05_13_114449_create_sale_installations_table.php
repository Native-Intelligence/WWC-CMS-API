<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleInstallationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_installations', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->dateTime('installation_date');
            $table->json('branch_info');
            $table->json('meter_info');
            $table->json('other_info');
            $table->unsignedInteger('user_installer_id')->index();
            $table->unsignedInteger('user_supervisor_id')->index();
            $table->unsignedInteger('request_id')->index();
            $table->dateTime('minutes_date');
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            // $table->foreign('meter_id')->references('id')->on('ing_meters');
            // $table->foreign('request_id')->references('id')->on('sale_requests');
            // $table->foreign('user_supervisor_id')->references('id')->on('users');
            // $table->foreign('user_installer_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_installations');
    }
}
