<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleBranchesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_branches',
            function (Blueprint $table) {
                $table->increments('id')->index();
                $table->unsignedInteger('use_file_number')->nullable();
                $table->unsignedInteger('residential_units')->default(0);
                $table->unsignedInteger('non_residential_units')->default(0);
                $table->unsignedInteger('empty_residential_units')->default(0);
                $table->boolean('mounted')->default(0);
                $table->unsignedInteger('land_id')->index();
                $table->unsignedTinyInteger('g_branch_diameter_id')->index();
                $table->unsignedInteger('usage_id')->index();
                $table->unsignedInteger('g_tank_id')->index();
                $table->float('consumption_instruction')->default(0);
                $table->string('bill_name');
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->foreign('land_id')->references('id')->on('sale_lands');
                $table->foreign('usage_id')->references('id')->on('ing_usages');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_branches');
    }
}
