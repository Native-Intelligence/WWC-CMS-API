<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleFilesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_files',
            function (Blueprint $table) {
                $table->increments('id')->index();
                // $table->unsignedInteger('invoice_id')->nullable();
                $table->dateTime('survey_date');
                $table->string('file_number')->nullable();
                $table->string('full_name')->nullable();
                $table->string('subscription_number')->nullable();
                $table->dateTime('coordination_date');
                $table->unsignedTinyInteger('g_service_type_id');
                $table->unsignedInteger('request_id')->index();
                $table->unsignedInteger('land_id')->index();
                $table->unsignedInteger('last_survey_id')->index();
                $table->unsignedInteger('last_calculation_id')->index();
                $table->unsignedInteger('first_request_id')->index();
                $table->unsignedInteger('user_id')->index();
                $table->unsignedInteger('instruction_id')->nullable();
                $table->unsignedInteger('usage_id')->index();
                // $table->json('discount_id');
                $table->json('survey_detail');
                $table->json('number_of_units');
                // $table->unsignedInteger('residential_units_wa')->default(0);
                // $table->unsignedInteger('residential_units_sw')->default(0);
                // $table->unsignedInteger('none_residential_units_wa')->default(0);
                // $table->unsignedInteger('none_residential_units_sw')->default(0);
                // $table->unsignedInteger('none_residential_surface_units_sw')->default(0);
                // $table->unsignedInteger('residential_surface_units_sw')->default(0);
                $table->boolean('is_split')->default(0);
                $table->unsignedTinyInteger('g_siphon_id')->index();
                $table->unsignedTinyInteger('g_branch_diameter_id')->nullable()->index();
                $table->unsignedTinyInteger('branch_type')->index();
                $table->unsignedInteger('g_type_installation_id')->default(0);
                $table->float('consumption_instruction')->default(0);
                $table->unsignedBigInteger('scope_city');
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
                // $table->foreign('user_id')->references('id')->on('users');
                // $table->foreign('usage_id')->references('id')->on('ing_usages');
                // $table->foreign('request_id')->references('id')->on('sale_requests');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_files');
    }
}
