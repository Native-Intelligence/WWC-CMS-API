<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleRequestsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_requests',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('request_number')->nullable()->default('0');
                $table->string('file_number')->nullable();
                $table->string('subscription_number')->nullable();
	            $table->dateTime('request_date')->nullable();
                $table->unsignedTinyInteger('request_type')->nullable();
                $table->unsignedInteger('g_service_type_id');
                $table->unsignedTinyInteger('branch_type');
                $table->unsignedInteger('person_id')->index();
                $table->unsignedInteger('land_id')->nullable()->index();
                $table->json('discount_id');
                $table->json('request_details')->nullable();
                $table->unsignedTinyInteger('status')->default(0);
                // $table->foreign('person_id')->references('id')->on('sale_persons');
                // $table->foreign('land_id')->references('id')->on('sale_lands');
                // $table->foreign('discount_id')->references('id')->on('ing_discounts');
                $table->unsignedBigInteger('scope');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_requests');
    }
}
