<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleCalculationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_calculations',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('request_id')->index();
                $table->unsignedInteger('instruction_id')->index();
                $table->unsignedInteger('survey_id')->index();
                $table->decimal('total_price',16,2);
                $table->string('file_number');
                // $table->string('bill_number');
                // $table->string('pay_number');
                $table->unsignedInteger('counts_installments');
                $table->string('customer_name');
                $table->json('billing_details');
                $table->json('installments_details')->nullable();
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_calculations');
    }
}
