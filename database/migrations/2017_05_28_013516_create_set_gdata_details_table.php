<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetGdataDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'set_gdata_details',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->index();
                $table->string('value')->nullable();
                $table->string('slug')->nullable();
                // $table->string('gdata_title');
                $table->unsignedInteger('gdata_id');
	            $table->json('extra')->nullable();
	            $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('set_gdata_details');
    }
}
