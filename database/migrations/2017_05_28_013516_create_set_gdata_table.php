<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetGdataTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_gdatas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->string('slug');
            $table->string('tag')->nullable();
            $table->unsignedBigInteger('scope');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('set_gdatas');
    }
}
