<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingPayments extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('g_payment_type_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->json('payment_details')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedBigInteger('scope');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounting_payments');
    }
}
