<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingFtp extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'accounting_ftp',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('g_bank_id');
                $table->unsignedInteger('payment_id')->nullable();
                $table->unsignedInteger('amount');
                $table->string('file_number')->nullable();
                $table->string('company_code');
                $table->string('bill_number');
                $table->string('pay_number');
                $table->date('paid_at');
                $table->timestamps();
                $table->unsignedTinyInteger('status')->default(0);
                // $table->unsignedBigInteger('scope');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounting_ftp');
    }
}
