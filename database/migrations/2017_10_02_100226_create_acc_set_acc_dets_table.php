<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccSetAccDetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'acc_set_acc_dets',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('level');
                $table->unsignedInteger('account_id');
                $table->unsignedInteger('account_det_id');
                $table->timestamps();
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acc_set_acc_dets');
    }
}
