<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccSetAccGrpsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'acc_set_acc_grps',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('code');
                $table->unsignedInteger('account_id');
                $table->unsignedTinyInteger('type');
                // $table->unsignedInteger('group_id')->nullable();
                $table->timestamps();
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acc_set_acc_grps');
    }
}
