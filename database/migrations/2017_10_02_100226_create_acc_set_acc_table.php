<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccSetAccTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'acc_set_accs',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('code');
                $table->unsignedInteger('level_4')->nullable();
                $table->unsignedInteger('level_5')->nullable();
                $table->unsignedInteger('level_6')->nullable();
                $table->unsignedTinyInteger('type');
                $table->unsignedTinyInteger('g_account_type_id')->nullable();
                $table->unsignedTinyInteger('parent_id')->nullable();
                $table->unsignedTinyInteger('temp')->nullable();
                $table->timestamps();
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acc_set_accs');
    }
}
