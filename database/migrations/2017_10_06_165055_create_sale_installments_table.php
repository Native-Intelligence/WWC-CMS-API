<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleInstallmentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'sale_installments',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('calculation_id')->index();
                $table->unsignedInteger('survey_id');
                $table->date('end_date');
                $table->date('start_date');
                $table->unsignedInteger('account_id');
                $table->string('file_number');
                $table->decimal('amount',16, 2);
                $table->unsignedTinyInteger('number_of_installment');
                $table->unsignedTinyInteger('count');
                $table->string('customer_name');
                $table->unsignedTinyInteger('percent')->nullable();
                $table->string('bill_number')->index();
                $table->string('pay_number')->index();
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_installments');
    }
}
