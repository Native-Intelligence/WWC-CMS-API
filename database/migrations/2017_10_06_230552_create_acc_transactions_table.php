<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccTransactionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'acc_transactions',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('type');
                $table->json('details');
                $table->timestamps();
                $table->unsignedBigInteger('scope');
                $table->unsignedTinyInteger('status')->default(0);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_transactions');
    }
}
