<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportDesignTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'report_designs',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('group');
                $table->string('query');
                $table->string('description')->nullable();
                $table->json('details')->nullable();
                $table->json('format')->nullable();
                $table->json('filters')->nullable();
                $table->json('columns')->nullable();
                $table->text('print_layout')->nullable();
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_designs');
    }
}
