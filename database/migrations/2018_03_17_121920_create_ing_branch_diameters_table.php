<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngBranchDiameterTable extends Migration
{

    public function up()
    {
        Schema::create(
        	'ing_branch_diameters', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('title');
	        $table->string('code');
	        $table->unsignedTinyInteger('type');
	        $table->unsignedTinyInteger('status')->default(0);
	        $table->unsignedBigInteger('scope');
	        $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_branch_diameters');
    }
}
