<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngBranchDiameterDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('ing_branch_diameter_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_diameter_id');
            $table->unsignedTinyInteger('units_min');
            $table->unsignedTinyInteger('units_max');
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedBigInteger('scope');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ing_branch_diameter_details');
    }
}
