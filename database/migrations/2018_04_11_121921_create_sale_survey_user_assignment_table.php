<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleSurveyUserAssignmentTable extends Migration
{

    public function up()
    {
        Schema::create(
            'sale_survey_user',
            function (Blueprint $table) {
	            $table->increments('id');
	            $table->unsignedInteger('request_id');
	            $table->unsignedInteger('user_id');
	            $table->unsignedTinyInteger('status')->default(0);
	            $table->unsignedBigInteger('scope');
	            $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('sale_survey_user');
    }
}
