<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleTechnicalEvaluationUserAssignmentTable extends Migration
{

    public function up()
    {
        Schema::create(
            'sale_tech_eval_user',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('request_id');
                $table->integer('user_id');
                $table->unsignedTinyInteger('status')->default(0);
                $table->unsignedBigInteger('scope');
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('sale_tech_eval_user');
    }
}
