<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartableStepsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cartable_steps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('model')->nullable();
			$table->unsignedTinyInteger('status')->default(0);
			$table->unsignedBigInteger('scope');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cartable_steps');
	}

}
