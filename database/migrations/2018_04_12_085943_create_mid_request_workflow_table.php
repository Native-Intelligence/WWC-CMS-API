<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMidRequestWorkflowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mid_request_workflow', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('request_id');
			$table->unsignedInteger('workflow_detail_id');
			$table->unsignedInteger('user_id')->nullable();
			$table->unsignedTinyInteger('status')->default(0);
			$table->unsignedBigInteger('scope');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mid_request_workflow');
	}

}
