<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartableWorkflowDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cartable_workflow_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('workflow_id');
			$table->unsignedInteger('step_id');
			$table->unsignedInteger('prev_id')->nullable();
			$table->unsignedInteger('next_id')->nullable();
			$table->unsignedInteger('next_false_id')->nullable();
			$table->unsignedInteger('g_cartable_condition_id')->nullable();
			$table->unsignedTinyInteger('status')->default(0);
			$table->unsignedBigInteger('scope');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cartable_workflow_details');
	}

}
