<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartableWorkflowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cartable_workflows', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->unsignedInteger('g_service_type_id');
			$table->unsignedTinyInteger('status')->default(0);
			$table->unsignedBigInteger('scope');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cartable_workflows');
	}

}
