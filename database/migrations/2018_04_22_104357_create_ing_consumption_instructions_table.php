<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngConsumptionInstructionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ing_consumption_instructions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('daily_consumption');
			$table->string('monthly_consumption');
			$table->unsignedInteger('unit_measurement_id')->index();
			$table->unsignedInteger('usage_id')->index();
			$table->unsignedTinyInteger('status')->default(0);
			$table->timestamps();
			$table->unsignedBigInteger('scope');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ing_consumption_instructions');
	}

}
