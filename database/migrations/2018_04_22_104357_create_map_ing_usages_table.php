<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapIngUsagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('map_ing_usages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('id_ing_usages')->nullable();
			$table->unsignedInteger('id_usages_sys_old')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('map_ing_usages');
	}

}
