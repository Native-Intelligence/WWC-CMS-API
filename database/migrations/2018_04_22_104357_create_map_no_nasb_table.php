<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapNoNasbTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('map_no_nasb', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('id_no_nasb')->nullable();
			$table->unsignedInteger('id_no_nasb_sys_old')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('map_no_nasb');
	}

}
