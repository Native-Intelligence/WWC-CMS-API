<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaleConsumptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sale_consumptions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('usage_id');
			$table->unsignedInteger('value');
			$table->unsignedInteger('land_id');
			$table->unsignedTinyInteger('status')->default(0);
			$table->timestamps();
			$table->unsignedBigInteger('scope');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sale_consumptions');
	}

}
