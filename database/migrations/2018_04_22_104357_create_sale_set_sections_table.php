<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaleSetSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sale_set_sections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->unsignedTinyInteger('status')->default(0);
			$table->timestamps();
			$table->unsignedBigInteger('scope');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sale_set_sections');
	}

}
