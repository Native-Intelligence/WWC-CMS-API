<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersSeeder');
        $this->command->info('Users tables seeded :)');
        
        $this->call('IngredientsSeeder');
        $this->command->info('Ingredients tables seeded :)');
        
        $this->call('SalesSeeder');
        $this->command->info('Sales tables seeded :)');
        
        $this->call('SettingsSeeder');
        $this->command->info('Settings tables seeded :)');
    }
}
