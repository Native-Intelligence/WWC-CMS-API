<?php
use Illuminate\Database\Seeder;

class IngredientsSeeder extends Seeder
{
 
    public function run()
    {
        /* area */
        DB::table('ing_areas')->delete();
        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ایران',
            'code' => 10,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => '',
                'city' => '',
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areaId = DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId;
        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'تهران',
            'code' => 11,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => $areaId,
                'city' => '',
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'گیلان',
            'code' => 12,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'city' => '',
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب منطقه 1',
            'code' => 13,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب منطقه 2',
            'code' => 14,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب منطقه 3',
            'code' => 15,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب منطقه 4',
            'code' => 16,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب منطقه 5',
            'code' => 17,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت فاضلاب تهران',
            'code' => 18,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();


        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت شهرها و شهرکهای غرب تهران',
            'code' => 19,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'َشرکت تامین و تصفیه اب و فاضلاب تهران ',
            'code' => 20,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب جنوب غربی استان تهران',
            'code' => 21,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب جنوب شرقی استان تهران',
            'code' => 22,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت اب و فاضلاب شرق استان تهران',
            'code' => 23,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'شرکت کارخانجات لوله سازی تهران ',
            'code' => 24,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه یک (دیباجی)',
            'code' => 25,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 13)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه دو (شمیران)',
            'code' => 26,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 13)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه سه (شهرک غرب)',
            'code' => 27,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 13)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه یک',
            'code' => 28,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 14)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه دو',
            'code' => 29,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 14)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه سه',
            'code' => 30,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 14)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();


        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه یک',
            'code' => 31,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 15)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه دو',
            'code' => 32,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 15)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه سه',
            'code' => 33,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 15)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه یک (بهارستان)',
            'code' => 34,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 16)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه دو(سلیمانیه)',
            'code' => 35,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 16)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'ناحیه سه (افسریه)',
            'code' => 36,
            'type' => '0',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('name', 'تهران')->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 16)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        /* Municipality */

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'تهران',
            'code' => 37,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'city' => '',
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'تهران',
            'code' => 38,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'region' => '',
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 1 شهرداری',
            'code' => 39,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 2 شهرداری',
            'code' => 40,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 3 شهرداری',
            'code' => 41,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 4 شهرداری',
            'code' => 42,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 5 شهرداری',
            'code' => 43,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 6 شهرداری',
            'code' => 44,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 7 شهرداری',
            'code' => 45,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 8 شهرداری',
            'code' => 46,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 9 شهرداری',
            'code' => 47,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();

        $areas = app()->make('Api\Sales\Settings\Models\Area');
        $areas->fill([
            'name' => 'منطقه 10 شهرداری',
            'code' => 48,
            'type' => '1',
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => DB::table('ing_areas')->select('id')->where('code', 37)->first()->id,
                'city' => DB::table('ing_areas')->select('id')->where('code', 38)->first()->id,
                'region' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id + app()->helper::$plusId,
                'area' => ''
            ]
        ]);
        $areas->save();
        // dd('dddddddd');


        /* $areas = app()->make('Api\Sales\Settings\Models\Area');
         $areas->fill([
             'name' =>'ناحیه 1',
             'code' => 17,
             'type' => '0',
             'scope' => [
                 'country' => app()->helper::$autoIncrement,
                 'province' => DB::table('ing_areas')->select('id')->where('name','تهران')->first()->id,
                 'city' => DB::table('ing_areas')->select('id')->where('name','اسلامشهر')->first()->id,
                 'region' => DB::table('ing_areas')->select('id')->where('name','منطقه 1')->first()->id,
                 'area' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id+ app()->helper::$plusId,
             ]
         ]);
         $areas->save();

         $areas = app()->make('Api\Sales\Settings\Models\Area');
         $areas->fill([
             'name' =>'ناحیه 2',
             'code' => 18,
             'type' => '0',
             'scope' => [
                 'country' => app()->helper::$autoIncrement,
                 'province' => DB::table('ing_areas')->select('id')->where('name','تهران')->first()->id,
                 'city' => DB::table('ing_areas')->select('id')->where('name','اسلامشهر')->first()->id,
                 'region' => DB::table('ing_areas')->select('id')->where('name','منطقه 1')->first()->id,
                 'area' => DB::table('ing_areas')->select('id')->orderBy('id', 'desc')->first()->id+ app()->helper::$plusId,
             ]
         ]);
         $areas->save();*/

        /* Meter */

        DB::table('ing_meters')->delete();
        $meters = app()->make('Api\Sales\Settings\Models\Meter');
        $meters->fill([
            'title' => 'B Meters',
            'g_class_name_id' => 1,
            'g_model_name_id' => 1,
            'g_factory_name_id' => 1,
            'digits_number' => 5,
            'start_point' => 100,
            'diameter' => 0.75,
            'diameter_in' => 1,
            'diameter_out' => 1,
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => '',
                'city' => '',
                'region' => '',
                'area' => ''
            ]
        ]);
        $meters->save();

        $meters = app()->make('Api\Sales\Settings\Models\Meter');
        $meters->fill([
            'title' => 'دوریس',
            'g_class_name_id' => 2,
            'g_model_name_id' => 2,
            'g_factory_name_id' => 2,
            'digits_number' => 5,
            'start_point' => 200,
            'diameter' => 0.5,
            'diameter_in' => 2,
            'diameter_out' => 1,
            'scope' => [
                'country' => app()->helper::$autoIncrement,
                'province' => '',
                'city' => '',
                'region' => '',
                'area' => ''
            ]
        ]);
        $meters->save();

        /* Filters */

        DB::table('ing_filters')->delete();
        $conditions = app()->make('Api\Sales\Settings\Models\Filter');
        $conditions->fill([
            'title' => 'شرط برای منطقه 1 اب و فاضلاب ',
            'filter' => '1',
            'type' => 0,
            'scope' => [
                'country' => 10,
                'province' => 11,
                'city' => 13,
                'region' => '',
                'area' => ''
            ]
        ]);
        
        $conditions->save();
        
        
        $conditions = app()->make('Api\Sales\Settings\Models\Filter');
        $conditions->fill([
            'title' => 'شرط برای منطقه 2 اب و فاضلاب ',
            'filter' => '1',
            'type' => 0,
            'scope' => [
                'country' => 10,
                'province' => 11,
                'city' => 14,
                'region' => '',
                'area' => ''
            ]
        ]);
        
        $conditions->save();
        
        $conditions = app()->make('Api\Sales\Settings\Models\Filter');
        $conditions->fill([
            'title' => 'فرمول برای منطقه 1 اب و فاضلاب',
            'filter' => 'priceWaterBranch',
            'type' => 1,
            'scope' => [
                'country' => 10,
                'province' => 11,
                'city' => 13,
                'region' => '',
                'area' => ''
            ]
        ]);
        
        $conditions->save();
        
        
        $conditions = app()->make('Api\Sales\Settings\Models\Filter');
        $conditions->fill([
            'title' => 'فرمول برای منطقه 2 اب و فاضلاب',
            'filter' => 'priceWaterBranch',
            'type' => 1,
            'scope' => [
                'country' => 10,
                'province' => 11,
                'city' => 14,
                'region' => '',
                'area' => ''
            ]
        ]);

        $conditions->save();

        /* Instruction */

        DB::table('ing_instructions')->delete();
        $instructions = app()->make('Api\Sales\Settings\Models\Instruction');
        $instructions->fill([
            'title' => 'سري اول سال 96',
            'start_date' => '2016-02-19',
            'end_date' => '2017-05-29',
            'effective_date' => '2016-05-29',
            'adviser' => 'حميد چيت چيان',
            'advise_date' => '2016-05-29',
            'advising_organization' => 'وزارت نيرو',
            'advising_position' => 'وزارت نيرو',
            'base_rate' => [['id' => 1,
                'slug' => 'priceWaterBranch',
                'scope' => [
                    'area' => '',
                    'city' => 13,
                    'region' => '',
                    'country' => 10,
                    'province' => 11
                ],
                'title' => 'حق انشعاب',
                'value' => '11400000',
                'is_home' => 1,
                'usage_id' => 1,
                'scope_city' => [
                    'area' => '',
                    'city' => 38,
                    'region' => 39,
                    'country' => 10,
                    'province' => 37
                ],
                'invoice_item_id' => 2
            ],
            ['id' => 2,
            'slug' => 'priceWaterBranch',
            'scope' => [
                'area' => '',
                'city' => 14,
                'region' => '',
                'country' => 10,
                'province' => 11
            ],
            'title' => 'حق انشعاب',
            'value' => '6650000',
            'is_home' => 1,
            'usage_id' => 1,
            'scope_city' => [
                'area' => '',
                'city' => 38,
                'region' => 40,
                'country' => 10,
                'province' => 37
            ],
            'invoice_item_id' => 2
            ],
            ['id' => 3,
            'slug' => 'priceWaterBranch',
            'scope' => [
            'area' => '',
            'city' => 15,
            'region' => '',
            'country' => 10,
            'province' => 11
            ],
            'title' => 'حق انشعاب',
            'value' => '12800000',
            'is_home' => 1,
            'usage_id' => 1,
            'scope_city' => [
            'area' => '',
            'city' => 38,
            'region' => 41,
            'country' => 10,
            'province' => 37
            ],
            'invoice_item_id' => 2
            ],
            [
            'id' => 4,
            'slug' => 'priceWaterBranch',
            'scope' => [
            'area' => '',
            'city' => 16,
            'region' => '',
            'country' => 10,
            'province' => 11
            ],
            'title' => 'حق انشعاب',
            'value' => '8550000',
            'is_home' => 1,
            'usage_id' => 1,
            'scope_city' => [
            'area' => '',
            'city' => 38,
            'region' => 42,
            'country' => 10,
            'province' => 37
            ],
            'invoice_item_id' => 2
            ],
            [
            'id' => 5,
            'slug' => 'priceWaterBranch',
            'scope' => [
            'area' => '',
            'city' => 17,
            'region' => '',
            'country' => 10,
            'province' => 11
            ],
            'title' => 'حق انشعاب',
            'value' => '18050000',
            'is_home' => 1,
            'usage_id' => 1,
            'scope_city' => [
            'area' => '',
            'city' => 38,
            'region' => 43,
            'country' => 10,
            'province' => 37
            ],
            'invoice_item_id' => 2
            ],
            [
            'id' => 6,
            'slug' => 'priceWaterBranch',
            'scope' => [
            'area' => '',
            'city' => 18,
            'region' => '',
            'country' => 10,
            'province' => 11
            ],
            'title' => 'حق انشعاب',
            'value' => '9550000',
            'is_home' => 1,
            'usage_id' => 1,
            'scope_city' => [
            'area' => '',
            'city' => 38,
            'region' => 44,
            'country' => 10,
            'province' => 37
            ],
            'invoice_item_id' => 2
            ],[
            'id' => 7,
            'slug' => 'priceWaterBranch',
            'scope' => [
            'area' => '',
            'city' => 19,
            'region' => '',
            'country' => 10,
            'province' => 11
            ],
            'title' => 'حق انشعاب',
            'value' => '19550000',
            'is_home' => 1,
            'usage_id' => 1,
            'scope_city' => [
            'area' => '',
            'city' => 38,
            'region' => 45,
            'country' => 10,
            'province' => 37
            ],
            'invoice_item_id' => 2
            ]],
            'factors' => [
                [
                    'id' => 1,
                    'slug' => 'adjustmentFactor',
                    'scope' => [
                        'area' => '',
                        'city' => '',
                        'region' => '',
                        'country' => 10,
                        'province' => 11
                    ],
                    'title' => 'ضريب تعديل',
                    'value' => '1.4',
                    'is_home' => 'aa',
                    'usage_id' => 1,
                    'scope_city' => [
                        'area' => '',
                        'city' => 38,
                        'region' => '',
                        'country' => 10,
                        'province' => 37
                    ],'
                    invoice_item_id' => 2,
                    'g_factor_type_id' => 0
                    ]],
            'instruction_type' => '0',
            'indicator_number' => '92/49765/20/100',
            'indicator_date' => '2017-02-29',
        ]);
        $instructions->save();

        /* usagePattern */

        DB::table('ing_usage_patterns')->delete();
        $usagePatterns = app()->make('Api\Sales\Settings\Models\UsagePattern');
        $usagePatterns->fill([
            'title' => 'هتل چهار ستاره و بالاتر',
            'code' => '1',
            'g_unit_id' => 1,
            'value' => 13.5,
            'description' => 'شامل تمام مصارف شرب، بهداشتی، اشپزخونه،فضای سبزو سایر امکانات',
        ]);
        $usagePatterns->save();

        $usagePatterns = app()->make('Api\Sales\Settings\Models\UsagePattern');
        $usagePatterns->fill([
            'title' => 'هتل تا سه ستاره',
            'code' => '2',
            'g_unit_id' => 1,
            'value' => 10.5,
            'description' => 'شامل تمام مصارف شرب، بهداشتی، اشپزخونه،فضای سبزو سایر امکانات',
        ]);
        $usagePatterns->save();

        $usagePatterns = app()->make('Api\Sales\Settings\Models\UsagePattern');
        $usagePatterns->fill([
            'title' => 'مسافر خانه و مهمانپذیر',
            'code' => '3',
            'g_unit_id' => 0,
            'value' => 4.5,
            'description' => 'شامل تمام مصارف شرب، بهداشتی، اشپزخونه،فضای سبزو سایر امکانات',
        ]);
        $usagePatterns->save();

        /* Usage */
        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'خانگی',
            'type' => '1',
            'code' => '1',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'صنعتی',
            'type' => '1',
            'code' => '2',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'تجاری',
            'type' => '1',
            'code' => '3',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'اداری و دولتی',
            'type' => '1',
            'code' => '4',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'عمومی و خدمات',
            'type' => '1',
            'code' => '5',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'اماکن مذهبی',
            'type' => '1',
            'code' => '6',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'گرمابه',
            'type' => '1',
            'code' => '7',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'نانوایی',
            'type' => '1',
            'code' => '8',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'مدارس',
            'type' => '1',
            'code' => '9',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'فضای سبز',
            'type' => '1',
            'code' => '10',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'آزاد و بنایی',
            'type' => '1',
            'code' => '11',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'مراکز فرهنگی ',
            'type' => '1',
            'code' => '12',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        $usage = app()->make('Api\Sales\Settings\Models\Usage');
        $usage->fill([
            'title' => 'سایر',
            'type' => '1',
            'code' => '13',
            'receivable_code' => '141-111',
            'wastewater_code' => '141-111'
        ]);
        $usage->save();

        /* CategoryDocuments */

        DB::table('ing_document_classes')->delete();
        $categoryDocuments = app()->make('Api\Sales\Settings\Models\DocumentClass');
        $categoryDocuments->fill([
            'title' => 'دسته بندی سندها ',
            'type' => '1',
        ]);
        $categoryDocuments->save();

        /* Certificate */

//        DB::table('ing_certificate')->delete();
//        $certificate = app()->make('Api\Sales\Settings\Models\Certificate');
//        $certificate->fill([
//            'title' => ' فایل مجوز'
//        ]);
//        $certificate->save();

        /* AccountNumber */

        DB::table('ing_accounts')->delete();
        $accountNumber = app()->make('Api\Sales\Settings\Models\Account');
        $accountNumber->fill([
            'title' => 'سپرده جاري',
            'account_type' => 1,
            'account_number' => '160-2-6604100-1',
            'is_identifier' => 0,
            'identifier' => '1111',
            'g_bank_id' => 9,
            'bank_branch' => '15 خرداد غربي',
            'bank_branch_code' => '1',
            'accounting_code' => '90-322193',
        ]);
        $accountNumber->save();

        $accountNumber = app()->make('Api\Sales\Settings\Models\Account');
        $accountNumber->fill([
            'title' => 'سپرده حق انشعاب',
            'account_type' => 1,
            'account_number' => '160-2-6604100-1',
            'is_identifier' => 0,
            'identifier' => '1111',
            'g_bank_id' => 9,
            'bank_branch' => '15 خرداد غربي',
            'bank_branch_code' => '1',
            'accounting_code' => '32-322193',
        ]);
        $accountNumber->save();

        $accountNumber = app()->make('Api\Sales\Settings\Models\Account');
        $accountNumber->fill([
            'title' => 'سپرده جاري',
            'account_type' => 1,
            'account_number' => '160-2-6604100-1',
            'is_identifier' => 0,
            'identifier' => '1111',
            'g_bank_id' => 9,
            'bank_branch' => 'سامان',
            'bank_branch_code' => '1',
            'accounting_code' => '00-322193',
        ]);
        $accountNumber->save();

        $accountNumber = app()->make('Api\Sales\Settings\Models\Account');
        $accountNumber->fill([
            'title' => 'سپرده نصب لوازم متفرقه',
            'account_type' => 2,
            'account_number' => '323-2-6604130-1',
            'is_identifier' => 0,
            'identifier' => '1111',
            'g_bank_id' => 22,
            'bank_branch' => 'کاشاني',
            'bank_branch_code' => '1',
            'accounting_code' => '52-322193',
        ]);
        $accountNumber->save();

        $accountNumber = app()->make('Api\Sales\Settings\Models\Account');
        $accountNumber->fill([
            'title' => 'هزينه لوازم',
            'account_type' => 1,
            'account_number' => '545-2-6604100-1',
            'is_identifier' => 0,
            'identifier' => '1111',
            'g_bank_id' => 1,
            'bank_branch' => 'ميدان امام',
            'bank_branch_code' => '1',
            'accounting_code' => '11-322193',
        ]);
        $accountNumber->save();

        /* InvoiceType */

        DB::table('ing_invoice_types')->delete();
        $invoice = app()->make('Api\Sales\Settings\Models\InvoiceType');
        $invoice->fill([
            'title' => 'مانده بدهی ',
            'type' => 'بدهکار'
        ]);
        $invoice->save();

        /* InvoiceItem */
        DB::table('ing_invoice_items')->delete();

                $invoice = app()->make('Api\Sales\Settings\Models\InvoiceItem');
                $invoice->fill([
                    'title' => 'حق انشعاب آب',
                    'slug' => 'priceWaterBranch',
                    'filter_id' => 1,
                    'formula_id' => 3,
                    'partition_percent' => 0,
                    'partition_count' => 0,
                    'invoice_type_id' => 1,
                    'is_partition' => 0,
                    'is_journal_doc' => 1,
                    'account_id' => 1
                ]);
                $invoice->save();


                $invoice = app()->make('Api\Sales\Settings\Models\InvoiceItem');
                $invoice->fill([
                    'title' => 'تبصره 2 حق انشعاب آب',
                    'slug' => 'priceRemark2Water',
                    'filter_id' => 2,
                    'formula_id' => 4,
                    'partition_percent' => 0,
                    'partition_count' => 0,
                    'invoice_type_id' => 1,
                    'is_partition' => 0,
                    'is_journal_doc' => 1,
                    'account_id' => 1
                ]);
                $invoice->save();

                $invoice = app()->make('Api\Sales\Settings\Models\InvoiceItem');
                $invoice->fill([
                    'title' => 'تبصره 3 حق انشعاب اب',
                    'slug' => 'priceRemark3Water',
                    'filter_id' => 2,
                    'formula_id' => 4,
                    'partition_percent' => 30,
                    'partition_count' => 4,
                    'invoice_type_id' => 1,
                    'is_partition' => 1,
                    'is_journal_doc' => 1,
                    'account_id' => 1
                ]);
                $invoice->save();

                $invoice = app()->make('Api\Sales\Settings\Models\InvoiceItem');
                $invoice->fill([
                    'title' => 'حق انشعاب فاضلاب',
                    'slug' => 'priceWasteWater',
                    'filter_id' => 1,
                    'formula_id' => 3,
                    'partition_percent' => 50,
                    'partition_count' => 5,
                    'invoice_type_id' => 1,
                    'is_partition' => 1,
                    'is_journal_doc' => 1,
                    'account_id' => 1
                ]);
                $invoice->save();

        /* Invoice */
        
        DB::table('ing_invoices')->delete();
        $invoice = app()->make('Api\Sales\Settings\Models\Invoice');
        $invoice->fill([
            'type' => '1',
            'usage_id' => '1',
            'filter_id' => '1',
            'formula_id' => '3',
            'instruction_id' => '1',
            'invoice_items_id' => [1,2,3,4]
        ]);
        $invoice->save();

        $invoice = app()->make('Api\Sales\Settings\Models\Invoice');
        $invoice->fill([
            'type' => '1',
            'usage_id' => '1',
            'filter_id' => '2',
            'formula_id' => '4',
            'instruction_id' => '1',
            'invoice_items_id' => [1,2,3,4]
        ]);
        $invoice->save();

        $invoice = app()->make('Api\Sales\Settings\Models\Invoice');
        $invoice->fill([
            'type' => '1',
            'usage_id' => '1',
            'filter_id' => '2',
            'formula_id' => '3',
            'instruction_id' => '1',
            'invoice_items_id' => [1,2,3,4]
        ]);
        $invoice->save();

        // DB::table('ing_invoices')->delete();
        // $invoice = app()->make('Api\Sales\Settings\Models\Invoice');
        // $invoice->fill([
        //     'abfa_area_id' => '10',
        //     'area_id' => '10',
        //     'usage_id' => '1',
        //     'filter_id' => ' 1',
        //     'formula_id' => '1',
        //     'instruction_id' => '1',
        //     'invoice_items_id' => ['aaa' => 'aaa']
        // ]);
        // $invoice->save();

        /* Accounting */

        // DB::table('ing_accountings')->delete();
        // $accounting = app()->make('Api\Sales\Settings\Models\Accounting');
        // $accounting->fill([
        //     'title' => ' حساب',
        //     'document_no' => 'شماره حساب',
        //     'document_date' => '2015-10-5',
        //     'is_confirmed' => '1',
        //     'request_id' => '1',
        //     'invoice_id' => '1',
        //     'document_class_id' => '1',
        // ]);
        // $accounting->save();
        //
//         DB::table('ing_areas')->delete();
//         $user = app()->make('Api\Sales\Settings\Models\Area');
//         $user->fill([
//          'name' => 'ایران',
//          'code' => app()->helper::$autoIncrement,
//          'type' => '1',
//          'scope' => [
//              'country' => app()->helper::$autoIncrement,
//              'province' => '',
//              'city' => '',
//              'region' => '',
//              'area' => ''
//              ]
//         ]);
//         $user->save();
//
//
//         /* tanks */
//
//         DB::table('ing_tanks')->delete();
//         $tanksCode = app()->make('Api\Sales\Settings\Models\Tank');
//         $tanksCode->fill([
//             'code' => '001',
//         ]);
//         $tanksCode->save();
//
//         /* meters */
//
//         DB::table('ing_meters')->delete();
//         $meters = app()->make('Api\Sales\Settings\Models\Meter');
//         $meters->fill([
//             'title' => 'کنتور یک',
//             'g_class_name_id' => 1,
//             'g_model_name_id' => 1,
//             'g_factory_name_id' => 1,
//             'digits_number' => '12',
//             'start_point' => '12',
//             'diameter' => '12',
//             'diameter_in' => '12',
//             'diameter_out' => '12'
//         ]);
//         $meters->save();
//
//         /* Conditions */
//
//         DB::table('ing_filters')->delete();
//         $conditions = app()->make('Api\Sales\Settings\Models\Filter');
//         $conditions->fill([
//             'title' => 'عنوان شرط',
//             'filter' => 'شرط',
//             'type' => 'شرط|فرمول',
//         ]);
//         $conditions->save();
//
//         /* Direction */
//
//         DB::table('ing_instructions')->delete();
//         $directions = app()->make('Api\Sales\Settings\Models\Instruction');
//         $directions->fill([
//             'title' => 'نوع حساب',
//             'start_date' => '2015-01-09',
//             'end_date' => ' 2015-03-05',
//             'effective_date' => ' 2015-10-9',
//             'adviser' => '1',
//         ]);
//         $directions->save();
//
//         /* Usage */
//
//         DB::table('ing_usages')->delete();
//         $usage = app()->make('Api\Sales\Settings\Models\Usage');
//         $usage->fill([
//             'title' => 'کاربری مصرف ',
//             'type' => '1',
//             'code' => '2',
//             'receivable_code' => '141-111',
//             'wastewater_code' => '141-111'
//         ]);
//         $usage->save();
//
//         /* CategoryDocuments */
//
//         DB::table('ing_document_classes')->delete();
//         $categoryDocuments = app()->make('Api\Sales\Settings\Models\DocumentClass');
//         $categoryDocuments->fill([
//             'title' => 'دسته بندی سندها ',
//             'type' => '1',
//         ]);
//         $categoryDocuments->save();
//
//         /* Certificate */
//
// //        DB::table('ing_certificate')->delete();
// //        $certificate = app()->make('Api\Sales\Settings\Models\Certificate');
// //        $certificate->fill([
// //            'title' => ' فایل مجوز'
// //        ]);
// //        $certificate->save();
//
//         /* AccountNumber */
//
//         DB::table('ing_accounts')->delete();
//         $accountNumber = app()->make('Api\Sales\Settings\Models\Account');
//         $accountNumber->fill([
//             'account_type' => 1,
//             'account_number' => '1111',
//             'is_identifier' => 1,
//             'identifier' => '1111',
//             'bank_name' => '1',
//             'bank_branch' => '1',
//             'bank_branch_code' => '1'
//         ]);
//         $accountNumber->save();
//
//         /* InvoiceType */
//
//         DB::table('ing_invoice_types')->delete();
//         $invoice = app()->make('Api\Sales\Settings\Models\InvoiceType');
//         $invoice->fill([
//             'title' => 'مانده بدهی ',
//             'type' => 'بدهکار'
//         ]);
//         $invoice->save();
//
//         /* InvoiceItem */
//
//         DB::table('ing_invoice_items')->delete();
//         $invoice = app()->make('Api\Sales\Settings\Models\InvoiceItem');
//         $invoice->fill([
//             'title' => '10',
//             'invoice_type_id' => 1,
//             'is_partition' => 1,
//             'is_journal_doc' => 1,
//             'account_id' => 1
//         ]);
//         $invoice->save();
//
//         /* Invoice */
//
//         DB::table('ing_invoices')->delete();
//         $invoice = app()->make('Api\Sales\Settings\Models\Invoice');
//         $invoice->fill([
//             'abfa_area_id' => '10',
//             'area_id' => '10',
//             'usage_id' => '1',
//             'filter_id' => ' 1',
//             'formula_id' => '1',
//             'instruction_id' => '1',
//             'invoice_items_id' => ['aaa' => 'aaa']
//         ]);
//         $invoice->save();
//
//         /* Accounting */
//
//         DB::table('ing_accountings')->delete();
//         $accounting = app()->make('Api\Sales\Settings\Models\Accounting');
//         $accounting->fill([
//             'title' => ' حساب',
//             'document_no' => 'شماره حساب',
//             'document_date' => '2015-10-5',
//             'is_confirmed' => '1',
//             'request_id' => '1',
//             'invoice_id' => '1',
//             'document_class_id' => '1',
//         ]);
//         $accounting->save();
    }
}
