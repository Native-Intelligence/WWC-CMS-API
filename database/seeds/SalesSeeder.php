<?php
use Illuminate\Database\Seeder;

class SalesSeeder extends Seeder
{
 
    public function run()
    {
        /* persons */

        DB::table('sale_persons')->delete();
        $person = app()->make('Api\Sales\Models\Person');
        $person->fill(
            [
            'full_name' => 'مریم راد',
            'identity_number' => 1309,
            'person_type' => 0,
            'person_details' => []
            ]
        );
        $person->save();

        $person = app()->make('Api\Sales\Models\Person');
        $person->fill(
            [
            'full_name' => 'منا محمدی ',
            'phone' => '88032490',
            'identity_number' => 0016032675,
            'person_type' => 0,
            'person_details' => []
            ]
        );
        $person->save();

        $person = app()->make('Api\Sales\Models\Person');
        $person->fill(
            [
            'full_name' => 'محمد عزیزی ',
            'phone' => '88995451',
            'identity_number' => 0016032674,
            'person_type' => 0,
            'person_details' => []
            ]
        );
        $person->save();

        $person = app()->make('Api\Sales\Models\Person');
        $person->fill(
            [
            'full_name' => 'سارا عطایی',
            'phone' => '88971213',
            'identity_number' => 0016032655,
            'person_type' => 0,
            'person_details' => []
            ]
        );
        $person->save();


        /* lands */

        DB::table('sale_lands')->delete();
        $lands = app()->make('Api\Sales\Models\Land');
        $lands->fill(
            [
            'municipality_number' => '1234567890',
            'registration_number' => '131456',
            'postal_code' => '9087098710',
            'file_number' => '715001532',
            'address' => 'بلوار فردوس غرب وفا آذر جنوبی ',
            'coordination' => ['35.717001', '51.393514'],
            'customer_number' => 1309,
            'arena' => 150,
            'foundation' => 100,
            'usage_id' => 1 ,
            'g_certificate_type_id' => 0 ,
            'g_evidence_type_id' => 1,
            'g_tank_id' => 1,
            'persons_id' => [1],
            'scope' => [
                'area' => '',
                'city' => 13,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $lands->save();

        $lands = app()->make('Api\Sales\Models\Land');
        $lands->fill(
            [
            'municipality_number' => '1213451236',
            'registration_number' => '1000236542',
            'postal_code' => '9865741263',
            'file_number' => '745101532',
            'address' => 'خیابان فاطمی نبش حجاب کوچه پروین اعتصامی  پلاک 5',
            'coordination' => ['35.717001', '51.393514'],
            'customer_number' => 3456,
            'arena' => 250,
            'foundation' => 200,
            'usage_id' => 1 ,
            'g_certificate_type_id' => 0 ,
            'g_evidence_type_id' => 1,
            'g_tank_id' => 2,
            'persons_id' => [2],
            'scope' => [
                'area' => '',
                'city' => 14,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $lands->save();

        $lands = app()->make('Api\Sales\Models\Land');
        $lands->fill(
            [
            'municipality_number' => '2154636547',
            'registration_number' => '1009689754',
            'postal_code' => '9365741279',
            'file_number' => '732101532',
            'address' => 'بلوار فردوس شرق ابراهیمی شمالی کوچه 5 پلاک 3 ',
            'coordination' => ['35.717001', '51.393514'],
            'customer_number' => 1256,
            'arena' => 90,
            'foundation' => 48,
            'usage_id' => 1 ,
            'g_certificate_type_id' => 0 ,
            'g_evidence_type_id' => 1,
            'g_tank_id' => 3,
            'persons_id' => [3],
            'scope' => [
                'area' => '',
                'city' => 15,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $lands->save();


        $lands = app()->make('Api\Sales\Models\Land');
        $lands->fill(
            [
            'municipality_number' => '3154636547',
            'registration_number' => '1002689754',
            'postal_code' => '0365741279',
            'file_number' => '832101532',
            'address' => 'میدان ازادی خیابان ازادی کوچه ناصر خسرو پلاک 12 واحد 9',
            'coordination' => ['35.717001', '51.393514'],
            'customer_number' => 3456,
            'arena' => 110,
            'foundation' => 70,
            'usage_id' => 1 ,
            'g_certificate_type_id' => 0 ,
            'g_evidence_type_id' => 1,
            'g_tank_id' => 3,
            'persons_id' => [4],
            'scope' => [
                'area' => '',
                'city' => 16,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $lands->save();
        
        /* sale_requests */

        DB::table('sale_requests')->delete();
        $saleReq = app()->make('Api\Sales\Models\SaleReq');
        $saleReq->fill(
            [
            'request_number' => '96030901',
            // 'request_date' => '2017-05-30 00:00:00',
            'request_type' => 0,
            'branch_type' => 0,
            'person_id' => 1,
            'land_id' => 1,
            'scope' => [
                'area' => '',
                'city' => 13,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $saleReq->save();

        $saleReq = app()->make('Api\Sales\Models\SaleReq');
        $saleReq->fill(
            [
            'request_number' => '96030902',
            // 'request_date' => '2017-05-30 00:00:00',
            'request_type' => 0,
            'branch_type' => 0,
            'person_id' => 2,
            'land_id' => 2,
            'scope' => [
                'area' => '',
                'city' => 14,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $saleReq->save();

        $saleReq = app()->make('Api\Sales\Models\SaleReq');
        $saleReq->fill(
            [
            'request_number' => '96030903',
            // 'request_date' => '2017-05-30 00:00:00',
            'request_type' => 0,
            'branch_type' => 0,
            'person_id' => 3,
            'land_id' => 3,
            'scope' => [
                'area' => '',
                'city' => 15,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $saleReq->save();

        $saleReq = app()->make('Api\Sales\Models\SaleReq');
        $saleReq->fill(
            [
            'request_number' => '96030904',
            // 'request_date' => '2017-05-30 00:00:00',
            'request_type' => 0 ,
            'branch_type' => 0,
            'person_id' => 3,
            'land_id' => 4,
            'scope' => [
                'area' => '',
                'city' => 16,
                'region' => '',
                'country' => 10,
                'province' => 11
            ]
            ]
        );
        $saleReq->save();
    }
}
