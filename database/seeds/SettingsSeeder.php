<?php
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
 
    public function run()
    {
        // $hasher = app()->make('hash');

        DB::table('set_gdatas')->delete();
        $gdata = app()->make('Api\Settings\Models\Gdata');
                
        $gdata->fill([
         'title' => 'تخفیف',
         'slug' => 'discount',
         'tag' => 'table',
         'status' => 1
        ]);
        $gdata->save();
        
        $gdata = app()->make('Api\Settings\Models\Gdata');
                
        $gdata->fill([
         'title' => 'ردیف هزینه',
         'slug' => 'invoice_item',
         'tag' => 'table',
         'status' => 1
        ]);
        $gdata->save();
        
        $gdata = app()->make('Api\Settings\Models\Gdata');
                
        $gdata->fill([
         'title' => 'نرخ پایه',
         'slug' => 'base_rate',
         'tag' => 'table',
         'status' => 1
        ]);
        $gdata->save();
        
        $gdata = app()->make('Api\Settings\Models\Gdata');
                
        $gdata->fill([
         'title' => 'ضرایب',
         'slug' => 'factors',
         'tag' => 'table',
         'status' => 1
        ]);
        $gdata->save();
    }
}
