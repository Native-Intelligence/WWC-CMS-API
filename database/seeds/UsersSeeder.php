<?php
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
 
    public function run()
    {
        $hasher = app()->make('hash');

        DB::table('users')->delete();
        $user = app()->make('Api\Settings\Models\User');
                
        $user->fill([
         'full_name' => 'نوید رجایی',
         'email' => 'navid.rajaie@gmail.com',
         'username' => 'admin',
         'password' => $hasher->make('1234'),
         'scope' => [
             'country' => '',
             'province' => '',
             'city' => '',
             'region' => '',
             'area' => ''
         ],
         'access' => [
             'roles' => [
                 [ 'id' => 1,
                   'name' => 'مدیر سیستم'
                 ]
             ],
             'scope' => [
                 'country' => [0, 1],
                 'province' => [0, 1],
                 'city' => [0, 1],
                 'region' => [0, 1],
                 'area' => [0, 1]
             ],
            //  'scope' => [
            //      'country' => [1 => 'null', 0 => 'not null!'],
            //      'province' => [1 => 'null', 0 => 'not null!'],
            //      'city' => [1 => 'null', 0 => 'not null!'],
            //      'region' => [1 => 'null', 0 => 'not null!'],
            //      'area' => [1 => 'null', 0 => 'not null!']
            //  ],
             'permissions' => [
                 'GET' => ['Api\Settings\Controllers\UserController@getAll'],
                 'POST' => ['Api\Settings\Controllers\UserController@create'],
                 'PUT' => [],
                 'DELETE' => [],
             ]
             ],
         'status' => 1
        ]);
        $user->save();
        
        $user = app()->make('Api\Settings\Models\User');
        
        $user->fill([
         'full_name' => 'test1',
         'email' => 'user@user.com',
         'username' => 'test',
         'password' => $hasher->make('1234'),
         'scope' => [
             'country' => 10,
             'province' => '',
             'city' => '',
             'region' => '',
             'area' => ''
         ],
         'access' => [
             'roles' => [
                 [ 'id' => 1,
                   'name' => 'مدیر سیستم'
                 ]
             ],
             'scope' => [
                 'country' => [0, 1],
                 'province' => [0, 1],
                 'city' => [0, 1],
                 'region' => [0, 1],
                 'area' => [0, 1]
             ],
             'permissions' => [
                 'GET' => ['Api\Settings\Controllers\UserController@getAll'],
                 'POST' => ['Api\Settings\Controllers\UserController@create'],
                 'PUT' => [],
                 'DELETE' => [],
                 ]
             ],
         'status' => 1
        ]);
        $user->save();
        

        $user = app()->make('Api\Settings\Models\User');

        $user->fill([
            'full_name' => 'test2',
            'email' => 'test2@gmail.com',
            'username' => 'test2',
            'password' => $hasher->make('1234'),
            'scope' => [
                'country' => '',
                'province' => '',
                'city' => '',
                'region' => '',
                'area' => ''
            ],
            'access' => [
                'roles' => [
                    [ 'id' => 1,
                      'name' => 'مدیر سیستم'
                    ]
                ],
                'scope' => [
                    'country' => [0, 1],
                    'province' => [0, 1],
                    'city' => [0, 1],
                    'region' => [0, 1],
                    'area' => [0, 1]
                ],
                //  'scope' => [
                //      'country' => [1 => 'null', 0 => 'not null!'],
                //      'province' => [1 => 'null', 0 => 'not null!'],
                //      'city' => [1 => 'null', 0 => 'not null!'],
                //      'region' => [1 => 'null', 0 => 'not null!'],
                //      'area' => [1 => 'null', 0 => 'not null!']
                //  ],
                'permissions' => [
                    'GET' => ['Api\Settings\Controllers\UserController@getAll'],
                    'POST' => ['Api\Settings\Controllers\UserController@create'],
                    'PUT' => [],
                    'DELETE' => [],
                ]
            ],
            'status' => 1
        ]);
        $user->save();

        $user = app()->make('Api\Settings\Models\User');

        $user->fill([
            'full_name' => 'test3',
            'email' => 'test3@gmail.com',
            'username' => 'test3',
            'password' => $hasher->make('1234'),
            'scope' => [
                'country' => '',
                'province' => '',
                'city' => '',
                'region' => '',
                'area' => ''
            ],
            'access' => [
                'roles' => [
                    [ 'id' => 1,
                      'name' => 'مدیر سیستم'
                    ]
                ],
                'scope' => [
                    'country' => [0, 1],
                    'province' => [0, 1],
                    'city' => [0, 1],
                    'region' => [0, 1],
                    'area' => [0, 1]
                ],
                //  'scope' => [
                //      'country' => [1 => 'null', 0 => 'not null!'],
                //      'province' => [1 => 'null', 0 => 'not null!'],
                //      'city' => [1 => 'null', 0 => 'not null!'],
                //      'region' => [1 => 'null', 0 => 'not null!'],
                //      'area' => [1 => 'null', 0 => 'not null!']
                //  ],
                'permissions' => [
                    'GET' => ['Api\Settings\Controllers\UserController@getAll'],
                    'POST' => ['Api\Settings\Controllers\UserController@create'],
                    'PUT' => [],
                    'DELETE' => [],
                ]
            ],
            'status' => 1
        ]);
        $user->save();

        DB::table('clients')->delete();
        $client = app()->make('Api\Settings\Models\Client');
        $client->fill([
         'app_name' => 'test',
         'redirect' => 'test',
         'app_secret' => $hasher->make('test'),
        ]);
        $client->save();

        DB::table('tokens')->delete();
        $token = app()->make('Api\Settings\Models\Token');
        $token->fill([
         'access_token' => $hasher->make('test'),
         'refresh_token' => $hasher->make('test'),
         'user_id' => 1,
         'client_id' => 1,
         'access_expires_at' => new \DateTime(),
         'refresh_expires_at' => new \DateTime(),
        ]);
        $token->save();
        
        $token = app()->make('Api\Settings\Models\Token');
        
        $token->fill([
         'access_token' => $hasher->make('test'),
         'refresh_token' => $hasher->make('test'),
         'user_id' => 2,
         'client_id' => 1,
         'access_expires_at' => new \DateTime(),
         'refresh_expires_at' => new \DateTime(),
        ]);
        $token->save();
        
        $token = app()->make('Api\Settings\Models\Token');
        
        $token->fill([
         'access_token' => $hasher->make('test'),
         'refresh_token' => $hasher->make('test'),
         'user_id' => 3,
         'client_id' => 1,
         'access_expires_at' => new \DateTime(),
         'refresh_expires_at' => new \DateTime(),
        ]);
        $token->save();

        DB::table('roles')->delete();
        $role = app()->make('Api\Settings\Models\Role');
        $role->fill([
         'slug' => 'Admin',
         'name' => 'مدیر',
         'permissions' => [
             'GET' => ['Api\Settings\Controllers\UserController@getAll'],
             'POST' => ['Api\Settings\Controllers\UserController@getAll'],
             'PUT' => [],
             'DELETE' => [],
         ],
         'scope' => [
             'country' => 10,
             'province' => '',
             'city' => '',
             'region' => '',
             'area' => ''
         ],
         'status' => 1
        ]);

        $role->save();
    }
}
