<?php

namespace Infrastructure\Console\Commands;

use Closure;
use Illuminate\Console\Command;
use Api\Sales\Models\Calculation;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\DB;
// use \nusoap_client;


class CalcConvertCommand extends Command
{

    protected $signature = 'convert:calc';

    protected $description = 'Convert requests table.5';

    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    public function handle()
    {
        DB::table('eshterak')->distinct('F3')->where('F38', '>', '0')->orWhere('F39', '>', '0')->orderBy('F3')->chunk(
            100, function ($calcs) {
                foreach ($calcs as $calc) {
                    // $eshterak = DB::table('eshterak')->select()->where('F3', '=', $request->F5)->first();
                    // if($eshterak->F4) {
                    //     continue;
                    // }
                    // $discounts = DB::table('eshterak4')->select('F1', 'F5', 'F6', 'F7', 'F8', 'F9')->where('F5', '=', $request->F3)->get();
                    // $land = DB::table('sale_lands')->select('id', 'scope')->where('file_number', '=', $request->F3)->first();
                    // $disArray = [];
                    // if(!is_null($discounts)) {
                    //     foreach ($discounts as $discount) {
                    //         if($discount->F6 == 1) {
                    //             $disArray []= ['discount' => ['id'=>$discount->F8,
                    //             'title' => DB::table('ing_discounts')->select('title')->find($discount->F8)->title
                    //             ],
                    //             'national_code'=> $discount->F1,
                    //             'exemption_code'=> 111111
                    //             ];
                    //         }
                    //         if($discount->F7 == 1) {
                    //             $disArray []= ['discount' => ['id'=>$discount->F9,
                    //             'title' => DB::table('ing_discounts')->select('title')->find($discount->F9)->title
                    //             ],
                    //             'national_code'=> $discount->F1,
                    //             'exemption_code'=> 111111
                    //             ];
                    //         }
                    //         $disArray = [$disArray];
                    //     }
                    //
                    // }
                    if(empty($calc->F6)) {
                        continue;
                    }
                    // dd($disArray);
                    $request = DB::table('sale_requests')->select()->where('file_number', '=', $calc->F3)->first();
                    $discounts = DB::table('eshterak4')->select('F1', 'F5', 'F6', 'F7', 'F8', 'F9')->where('F5', '=', $calc->F3)->get();
                    $disArray=[];
                    if(!is_null($discounts)) {
                        foreach ($discounts as $discount) {
                            if($discount->F6 == 1) {
                                $disArray []= ['discount' => [
                                    'id'=>$discount->F8,
                                    'title' => DB::table('ing_discounts')->select('title')->find($discount->F8)->title
                                ],
                                'national_code'=> $discount->F1,
                                'exemption_code'=> 111111,
                                'count' => 1
                                ];
                            }
                            if($discount->F7 == 1) {
                                $disArray []= ['discount' => ['id'=>$discount->F9,
                                'title' => DB::table('ing_discounts')->select('title')->find($discount->F9)->title
                                ],
                                'national_code'=> $discount->F1,
                                'exemption_code'=> 111111,
                                'count' => 1
                                ];
                            }
                            $disArray = [$disArray];
                        }
                        
                    }
                    // dd($disArray);
                    // $idIngUsage = DB::table('map_ing_usages')->select('id_ing_usages')->where('id_usages_sys_old', '=', $calc->F16)->first()->id_ing_usages;
                    $survey = DB::table('sale_surveys')->select()->where('file_number', '=', $calc->F3)->first();
                    
                    $person = DB::table('sale_persons')->select()->where('identity_number', '=', $calc->F6)->first();
                    
                    $land = DB::table('sale_lands')->select()->where('file_number', '=', $calc->F3)->first();
                    
                    $usage = DB::table('ing_usages')->select('code', 'id', 'title')->where('id', '=', $land->usage_id)->first();
                    
                    $coding = DB::table('ing_usages')->select('code', 'id', 'title')->where('code', '=', $calc->F18)->first();
                    $survey->request = $request;
                    $survey->request->person = $person;
                    $survey->request->land = $land;
                    $c = Calculation::create(
                        [
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'request_id' => $request->id,
                        'instruction_id' => 6,
                        'survey_id' => $survey->id,
                        'total_price' => $calc->F39+$calc->F38,
                        'counts_installments' => 0,
                        'scope' => $survey->scope,
                        'customer_name' => $person->full_name,
                        // 'customer_name' => $details['customer_name'],
                        'file_number' => $calc->F3,
                        'billing_details' =>
                            [
                        //                                 discount_types
                        // invoice_item_price
                        // account_slugs
                        // invoice_item_price_with_discount
                            // 'bill_number' => '123456789',
                            // 'pay_number' => '123456789',
                            // 'survey' => $survey,
                            'current_survey' => $survey,
                            // 'subscription_number' => $calc->F4,
                            // 'consumption_instruction' => '22222222',
                            // 'file_number' => $calc->F3,
                            // 'special_type' => 0,
                            // 'quota_type' => 0,
                            // 'quota_count' => 0,
                            // 'superstructure' => 120,
                            // 'total_price' => $calc->F39+$calc->F38,
                            // 'total_price_with_discount' => '',
                            'invoice_item_price' => ['mande' => $calc->F39+$calc->F38],
                            'account_slugs' => ['mande'=>'1'],
                            // 'invoice_item_names' => '',
                            'invoice_item_price_with_discount' => '',
                            // 'request_number' => $details['request_number'],
                            // 'request_number' => $request->id,
                            'g_service_type_id' => $survey->g_service_type_id,
                            // 'g_service_type_id' => $details['g_service_type_id'],
                            // 'branch_type' => $request->branch_type,
                            // 'request_date' => $details['request_date'],
                            // 'full_name' => $details['full_name'],
                            // 'identity_number' => $person->identity_number,
                            // 'phone_number' => '',
                            // 'postal_code' => $person->identity_number,
                            // 'address' => $details['address'],
                            // 'current_usage' => $usage->title,
                            // 'previous_usage' => '',
                            // 'land' => $land,
                            'discount_types' => ''
                            ]
                        ,
                        'installments_details' => [],
                        ]
                    );
                    $c = DB::table('sale_calculations')->select()->where('file_number', '=', $calc->F3)->first();;
                    $installmentAB = DB::table('eshterak2')->select()->where('F1', '=', $calc->F3)->get();
                    // dd($installmentAB);
                    $size = sizeof($installmentAB);
                    foreach ($installmentAB as $value) {
                        // code...
                        DB::table('sale_installments')->insert(
                            [
                                'calculation_id' => $c->id,
                                'survey_id' => $survey->id,
                                'end_date'=>date('Y-m-d H:i:s'),
                                'start_date'=>date('Y-m-d H:i:s'),
                                'account_id' => 1,
                                'file_number' => $calc->F3,
                                'bill_number' => $value->F6,
                                'pay_number' => $value->F7,
                                'amount' => $value->F5,
                                'number_of_installment' => $value->F3,
                                'count' => $size,
                                'customer_name' => $person->full_name,
                                'status' => 0,
                                'scope' => $survey->scope
                            ]
                        );
                    }
                    $installmentFA = DB::table('eshterak3')->select()->where('F1', '=', $calc->F3)->get();
                    $size = sizeof($installmentFA);
                    foreach ($installmentFA as $value) {
                        // code...
                        DB::table('sale_installments')->insert(
                            [
                                'calculation_id' => $c->id,
                                'survey_id' => $survey->id,
                                'end_date'=>date('Y-m-d H:i:s'),
                                // 'end_date'=>date($value->F4),
                                'start_date'=>date('Y-m-d H:i:s'),
                                'account_id' => 1,
                                'file_number' => $calc->F3,
                                'bill_number' => $value->F6,
                                'pay_number' => $value->F7,
                                'amount' => $value->F5,
                                'number_of_installment' => $value->F3,
                                'count' => $size,
                                'customer_name' => $person->full_name,
                                'status' => 0,
                                'scope' => $survey->scope
                            ]
                        );
                    }
                }
            }
        );
        
    }
}
