<?php

namespace Infrastructure\Console\Commands;

use Illuminate\Console\Command;
use Api\Sales\Models\File;
use Illuminate\Support\Facades\DB;


class FileConvertCommand extends Command
{
    
    protected $signature = 'convert:file';
    
    protected $description = 'Convert requests table.3';
    
    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;
    protected $factor = 1.05;
    
    protected $falseDuplicate = [];
    protected $falseLand = [];
    
    public function createNumberOfUnits($discounts, $residential, $consumption, $usage, $expense, $coding, $type)
    {
        // Fixed consumption
        $consumption = $usage->code == '1' ? 0 : round($consumption / $this->factor, 2);
        
        return [
            'type'              => $type,
            'consumption'       => $consumption,
            'usage_pattern'     => $usage->code == '1' ? null : ['id' => '0', 'title' => 'ناقص', 'value' => $consumption],
            'consumption_unit'  => 1,
            'discounts'         => array_reduce(
                $discounts,
                function ($result, $discount) use ($type) {
                    if ($discount->{$type == 0 ? 'F6' : 'F7'} == 1) {
                        $temp = $discount->{$type == 0 ? 'F8' : 'F9'};
                        $result[] = [
                            'discount'       => [
                                'id'    => $temp,
                                'title' => DB::table('ing_discounts')->select('title')->find($temp)->title
                            ],
                            'national_code'  => $discount->F1,
                            'exemption_code' => 111111,
                            'count'          => 1,
                        ];
                    }
                    
                    return $result;
                },
                []
            ),
            'coding'            => empty($coding) ? null : ['id' => $coding->id, 'title' => $coding->title, 'code' => $coding->code],
            'usage'             => ['id' => $usage->id, 'code' => $usage->code, 'title' => $usage->title],
            'expense'           => ['id' => $expense->id, 'code' => $expense->code, 'title' => $expense->title],
            'residential_units' => $residential,
        ];
    }
    
    public function handle()
    {
        DB::table('##main')->orderBy('F3')->chunk(
            10000,
            function ($surveys) {
                foreach ($surveys as $survey) {
                    // number of units
                    $numberOfUnits = [];
                    
                    // print file number
                    $this->info($survey->F3);
                    
                    // check duplicate
                    if (!empty(DB::table('sale_files')->select('id')->where('file_number', '=', $survey->F3)->first())) {
                        $this->info("duplicate");
                        $this->falseDuplicate[] = $survey->F3;
                        
                        continue;
                    }
                    
                    // check land exist
                    $land = DB::table('sale_lands')->select('id', 'usage_id', 'scope', 'scope_city')->where('file_number', '=', $survey->F3)->first();
                    if (empty($land)) {
                        $this->info("land");
                        $this->falseLand[] = $survey->F3;
                        
                        continue;
                    }
                    
                    $discounts = DB::table('##discount')
                        ->select('F1', 'F5', 'F6', 'F7', 'F8', 'F9')
                        ->where('F5', '=', $survey->F3)
                        ->get();
                    
                    $usage = DB::table('ing_usages')
                        ->select('code', 'id', 'title')
                        ->where('id', '=', $land->usage_id)
                        ->first();
                    
                    $coding = $usage->code == '1' ? null : DB::table('ing_usages')
                        ->select('code', 'id', 'title')
                        ->where('type', '=', 3)
                        ->where('code', '=', $survey->F18)
                        ->first();
                    
                    // check usage
                    if ($usage->code == '4') {
                        $usage = DB::table('ing_usages')
                            ->select('code', 'id', 'title')
                            ->where('type', '=', 1)
                            ->whereIn('code', ['1', '2'])
                            ->get();
                        $expense = DB::table('ing_usages')
                            ->select('code', 'id', 'title')
                            ->where('type', '=', 2)
                            ->whereIn('code', ['1', '2'])
                            ->get();
                        
                        $numberOfUnits[] = $this->createNumberOfUnits($discounts->toArray(), $survey->F19, $survey->F21, $usage[0], $expense[0], null, 0);
                        $numberOfUnits[] = $this->createNumberOfUnits($discounts->toArray(), $survey->F20, $survey->F21, $usage[1], $expense[1], $coding, 0);
                        
                        // for siphon units
                        if ($survey->F31 > 0) {
                            $numberOfUnits[] = $this->createNumberOfUnits($discounts->toArray(), $survey->F31, $survey->F33, $usage[0], $expense[0], null, 1);
                            $numberOfUnits[] = $this->createNumberOfUnits($discounts->toArray(), $survey->F32, $survey->F33, $usage[1], $expense[1], $coding, 1);
                        }
                    } else {
                        $expense = DB::table('ing_usages')
                            ->select('code', 'id', 'title')
                            ->where('type', '=', 2)
                            ->where('code', '=', $survey->F17)
                            ->first();
                        
                        $numberOfUnits[] = $this->createNumberOfUnits($discounts->toArray(), $survey->F19, $survey->F21, $usage, $expense, $coding, 0);
                    }
                    
                    $gBranchDiameterId = DB::table('set_gdata_details_view')
                        ->where('value', '=', $survey->F22)
                        ->where('gdata_slug', '=', 'branch_diameter')
                        ->first();
                    
                    $gSiphonId = DB::table('set_gdata_details_view')
                        ->where('value', '=', $survey->F34)
                        ->where('gdata_slug', '=', 'siphon')
                        ->first();
                    
                    File::create(
                        [
                            'branch_type'             => 0,
                            'land_id'                 => $land->id,
                            'full_name'               => DB::table('sale_persons')->select('full_name')->where('identity_number', '=', ((is_null($survey->F6)) ? '1111111111' : $survey->F6))->first()->full_name,
                            'last_survey_id'          => 0,
                            'last_calculation_id'     => 0,
                            'first_request_id'        => 0,
                            'survey_date'             => '2015-12-23 00:00:00',
                            'file_number'             => $survey->F3,
                            'subscription_number'     => $survey->F4,
                            'coordination_date'       => '2015-12-23 00:00:00',
                            'request_id'              => 0,
                            'instruction_id'          => null,
                            'usage_id'                => $land->usage_id,
                            'number_of_units'         => $numberOfUnits,
                            'is_split'                => 1,
                            'g_siphon_id'             => empty($gSiphonId) ? null : $gSiphonId->id,
                            'g_branch_diameter_id'    => empty($gBranchDiameterId) ? null : $gBranchDiameterId->id,
                            'g_type_installation_id'  => DB::table('map_no_nasb')->select('id_no_nasb')->where('id_no_nasb_sys_old', '=', $survey->F25)->first()->id_no_nasb,
                            'consumption_instruction' => $survey->F21,
                            'g_service_type_id'       => 571,
                            'scope_city'              => $land->scope_city,
                            'scope'                   => $land->scope,
                            'status'                  => 0,
                        ]
                    );
                }
            }
        );
        
        $this->info("<==========================Land==========================>");
        foreach ($this->falseLand as $val) {
            $this->info($val);
        }
        
        $this->info("<==========================Duplicate==========================>");
        foreach ($this->falseDuplicate as $val) {
            $this->info($val);
        }
    }
}
