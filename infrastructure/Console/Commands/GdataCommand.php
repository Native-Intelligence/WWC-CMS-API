<?php

namespace Infrastructure\Console\Commands;

use Closure;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Api\Settings\Repositories\GdataRepository;
use Api\Sales\Settings\Repositories\InstructionRepository;
use Api\Sales\Settings\Repositories\InvoiceItemRepository;
use Api\Sales\Settings\Repositories\DiscountRepository;

class GdataCommand extends Command
{

    private $_gdataRepository;
    
    private $_instructionRepository;
    
    private $_invoiceItemRepository;
    
    private $_discountRepository;

    protected $name = 'gdata:update';

    protected $description = 'Update gdata rows.';
    
    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;

    public function __construct(
        GdataRepository $gdataRepository,
        InstructionRepository $instructionRepository,
        InvoiceItemRepository $invoiceItemRepository,
        DiscountRepository $discountRepository
    ) {
        parent::__construct();

        // $this->router = $router;
        // $this->routes = app()->getRoutes();
        $this->_gdataRepository = $gdataRepository;
        $this->_instructionRepository = $instructionRepository;
        $this->_invoiceItemRepository = $invoiceItemRepository;
        $this->_discountRepository = $discountRepository;
    }

    public function handle()
    {
        $discounts = $this->_discountRepository->all();
        $discountGdata = [];
        
        foreach ($discounts as $discount) {
            // if(array_key_exists('title', $discount) and array_key_exists('slug', $discount))
            // {
            $temp = [
                'title' => $discount->title,
                'value' => $discount->slug,
                'slug' => null
            ];
            
            if (!in_array($temp, $discountGdata)) {
                $discountGdata[] = $temp;
            }
            // }
        }
        
        $invoiceItems = $this->_invoiceItemRepository->all();
        $invoiceItemsGdata = [];
        foreach ($invoiceItems as $invoiceItem) {
            $temp = [
                'title' => $invoiceItem->title,
                'value' => $invoiceItem->slug,
                'slug' => null
            ];
            
            if (!in_array($temp, $invoiceItemsGdata)) {
                $invoiceItemsGdata[] = $temp;
            }
        }
        
        $instructions = $this->_instructionRepository->all();
        // dd($this->invoiceItem);
        $baseRatesGdata = [];
        foreach ($instructions as $instruction) {
            if (is_null($instruction->base_rate)) {
                break;
            }
                
            foreach ($instruction->base_rate as $v) {
                $temp = [
                    'title' => $v->title,
                    'value' => $v->slug,
                    'slug' => null
                ];
                
                if (!in_array($temp, $baseRatesGdata)) {
                    $baseRatesGdata[] = $temp;
                }
            }
        }
        
        // dd($instructionsz);
        $factorsGdata = [];
        foreach ($instructions as $instruction) {
            if (is_null($instruction->base_rate)) {
                break;
            }
                
            foreach ($instruction->factors as $v) {
                $temp = [
                    'title' => $v->title,
                    'value' => $v->slug,
                    'slug' => null
                ];
                
                if (!in_array($temp, $factorsGdata)) {
                    $factorsGdata[] = $temp;
                }
            }
        }
        
        // dd($factorsGdata);
        // dd($invoiceItems);
        if (!empty($discountGdata)) {
            $id = $this->_gdataRepository->findByField('slug', 'discount')->first()->id;
            $this->_gdataRepository->update(['rows' => $discountGdata], $id);
        }
        
        // dd($invoiceItemsGdata);
        if (!empty($invoiceItemsGdata)) {
            $id = $this->_gdataRepository->findByField('slug', 'invoice_item')->first()->id;
            $this->_gdataRepository->update(['rows' => $invoiceItemsGdata], $id);
        }
        
        if (!empty($baseRatesGdata)) {
            $id = $this->_gdataRepository->findByField('slug', 'base_rate')->first()->id;
            $this->_gdataRepository->update(['rows' => $baseRatesGdata], $id);
        }
        
        if (!empty($factorsGdata)) {
            $id = $this->_gdataRepository->findByField('slug', 'factors')->first()->id;
            $this->_gdataRepository->update(['rows' => $factorsGdata], $id);
        }
        
        $this->info('Update gdata table name success!!!');
    }
}
