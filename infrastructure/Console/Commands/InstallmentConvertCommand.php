<?php

namespace Infrastructure\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use \Morilog\Jalali\jDateTime;


class InstallmentConvertCommand extends Command
{
    
    protected $signature = 'convert:installment';
    
    protected $description = 'Convert installment table.4';
    
    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;
    protected $falseDuplicate = [];
    protected $falseLand = [];
    protected $falseAccountId = [];
    
    public function insert($installment, $water = 1)
    {
        $this->info($installment->F1);
        
        $code = substr((string)$installment->F6,-5,3);
        $accountId = DB::table('ing_accounts')->select('id')
            ->where('company_code', '=', $code)
            ->first();
        if(empty($accountId)){
            $this->falseAccountId[] = $installment->F1;
            return 0;
        }

        // check duplicate
        if (!empty(DB::table('sale_installments')->select('id')
            ->where('file_number', '=', $installment->F1)
            ->where('number_of_installment', '=', $installment->F3)
            ->where('account_id', '=', $accountId->id)
            ->first()
        )) {
            $this->info("duplicate");
            $this->falseDuplicate[] = $installment->F1;
            
            return 0;
        }
    
        // check land exist
        $land = DB::table('sale_lands')->where('file_number', '=', $installment->F1)->first();
        if (empty($land)) {
            $this->info("land");
            $this->falseLand[] = $installment->F1;
            
            return 0;
        }
        
        $temp = DB::table($water ? '##water' : '##siphon')->where('F1', '=', $installment->F1);
        
        $date = explode('/', $temp->max('F4'));
        $max = implode('-', jDateTime::toGregorian($date[0], $date[1], $date[2]));
        
        $date = explode('/', $temp->min('F4'));
        $min = implode('-', jDateTime::toGregorian($date[0], $date[1], $date[2]));
        
        DB::table('sale_installments')->insert([
            'calculation_id'        => 0,
            'survey_id'             => 0,
            'end_date'              => $max,
            'start_date'            => $min,
            'account_id'            => $accountId->id,
            'file_number'           => $installment->F1,
            'amount'                => $installment->F5,
            'number_of_installment' => $installment->F3,
            'count'                 => $temp->count(),
            'customer_name'         => DB::table('sale_persons')->where('id', '=', json_decode($land->persons_id, true)[0])->first()->full_name,
            'percent'               => null,
            'bill_number'           => $installment->F6,
            'pay_number'            => sprintf('%013d', $installment->F7),
            'status'                => 0,
            'scope'                 => $land->scope
        ]);
    }
    
    public function handle()
    {
        DB::table('##siphon')->orderBy('F1')->chunk(
            10000,
            function ($installments) {
                foreach ($installments as $installment) {
                    if ($this->insert($installment, 0) === 0)
                    continue;
                }
            }
        );
        
        $this->info('###################################');
        
        DB::table('##water')->orderBy('F1')->chunk(
            10000,
            function ($installments) {
                foreach ($installments as $installment) {
                    if ($this->insert($installment, 1) === 0)
                        continue;
                }
            }
        );
        
        
        $this->info("<==========================Land==========================>");
        foreach($this->falseLand as $val){
            $this->info($val);
        }

        $this->info("<==========================Duplicate==========================>");
        foreach($this->falseDuplicate as $val){
            $this->info($val);
        }
        
        $this->info("<==========================Account Id==========================>");
        foreach($this->falseAccountId as $val){
            $this->info($val);
        }
    }
}
