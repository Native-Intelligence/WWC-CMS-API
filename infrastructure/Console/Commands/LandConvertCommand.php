<?php

namespace Infrastructure\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class LandConvertCommand extends Command
{
    
    protected $signature = 'convert:land';
    
    protected $description = 'Convert person table.2';
    
    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;
    protected $falseScope = [];
    protected $falseScopeCity = [];
    protected $falseDuplicate = [];
    protected $falseIdentity = [];
    protected $falseTank = [];
    protected $falseUsage = [];
    
    public function handle()
    {
        // get area code
        $code = trim($this->ask('Please enter parent area code'));
        
        DB::table('##main')->select('F2', 'F3', 'F4', 'F6', 'F9', 'F10', 'F11', 'F12', 'F13', 'F15', 'F16')->orderBy('F3')->chunk(
            10000,
            function ($lands) use ($code) {
                foreach ($lands as $land) {
                    $this->info($land->F3);
                    
                    // check duplicate
                    if (!empty(DB::table('sale_lands')->select('id')->where('file_number', '=', $land->F3)->first())) {
                        $this->info("duplicate");
                        $this->falseDuplicate[] = $land->F3;
                        
                        continue;
                    }
                    
                    // check person exist
                    $person = DB::table('sale_persons')->select('id')->where('identity_number', '=', ((is_null($land->F6)) ? $land->F3 : $land->F6))->first();
                    if (empty($person)) {
                        $this->info("person");
                        $this->falseIdentity[] = $land->F3;
                        continue;
                    }
                    
                    // check tank exist
                    $tank = DB::table('set_gdata_details_view')->select('id')->where('gdata_slug', '=', 'tank')->where('value', '=', $land->F13)->first();
                    if (empty($tank)) {
                        $this->info("tank");
                        $this->falseTank[] = $land->F3;
        
                        continue;
                    }
                    
                    // check usage exist
                    $usage = DB::table('ing_usages')->select('id')->where('code', '=', $land->F16)->where('type', '=', 1)->first();
                    if (empty($usage)) {
                        $this->info("usage");
                        $this->falseUsage[] = $land->F3;
                        
                        continue;
                    }
                    
                    // check scope exist
                    $area = DB::table('ing_areas_view')->select('scope')->where('type', '=', 0)->where('parent_code', 'like', '%"' . $code . '"%')->where('code', 'like', '%"' . $land->F2 . '"%')->first();
                    if (empty($area)) {
                        $this->info("scope");
                        $this->falseScope[] = $land->F3;
                        
                        continue;
                    }
                    
                    // check scope city exist
                    $city = DB::table('ing_areas_view')->select('scope')->where('type', '=', 1)->where('code', 'like', '%"' . $land->F14 . '"%')->first();
                    if (empty($city)) {
                        $this->info("scope_city");
                        $this->falseScopeCity[] = $land->F3;
                        
                        continue;
                    }
                    
                    DB::table('sale_lands')->insert([
                        'file_number'         => $land->F3,
                        'subscription_number' => $land->F4,
                        'address'             => $land->F9,
                        'postal_code'         => $land->F10,
                        'arena'               => (integer)$land->F11,
                        'foundation'          => (integer)$land->F12,
                        'g_tank_id'           => $tank->id,
                        'usage_id'            => $usage->id,
                        'persons_id'          => '[' . $person->id . ']',
                        'g_land_type_id'      => 221,
                        'status'              => 0,
                        'scope'               => $area->scope,
                        'scope_city'          => $city->scope
                    ]);
                }
            }
        );
        
        $this->info("<==========================Identity==========================>");
        foreach ($this->falseIdentity as $val) {
            $this->info($val);
        }
        
        $this->info("<==========================Duplicate==========================>");
        foreach ($this->falseDuplicate as $val) {
            $this->info($val);
        }
        
        $this->info("<==========================Scope==========================>");
        foreach ($this->falseScope as $val) {
            $this->info($val);
        }
        
        $this->info("<=======================Scope City==========================>");
        foreach ($this->falseScopeCity as $val) {
            $this->info($val);
        }
        
        $this->info("<==========================Tank==========================>");
        foreach ($this->falseTank as $val) {
            $this->info($val);
        }
        
        $this->info("<==========================Usage==========================>");
        foreach ($this->falseUsage as $val) {
            $this->info($val);
        }
        
        // update address
        DB::update('UPDATE sale_lands SET address = dbo.fixPersianLang(RTRIM(LTRIM(address)))');
    }
}
