<?php

namespace Infrastructure\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class PersonConvertCommand extends Command
{
    
    protected $signature = 'convert:person';
    
    protected $description = 'Convert person table.1';
    
    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;
    
    protected $falseIdentity = [];
    protected $falseDuplicate = [];
    
    public function handle()
    {

        DB::table('##main')->select('F3', 'F6', 'F7', 'F8')->orderBy('F3')->chunk(
            10000,
            function ($persons) {
                
                foreach ($persons as $person) {
                    $identity = (is_null($person->F6)) ? $person->F3 : $person->F6;
                    
                    $this->info($identity);
    
                    // check unvalid identity
                    if (is_null($identity) || $identity == "0") {
                        $this->info("continue");
                        $this->falseIdentity[] = $person->F3;
        
                        continue;
                    }
                    
                    // check duplicate
                    if (!empty(DB::table('sale_persons')->select('id')->where('identity_number', '=', $identity)->first())) {
                        $this->info("duplicate");
                        $this->falseDuplicate[] = $person->F3;
                        
                        continue;
                    }
                    
                    DB::table('sale_persons')->insert(
                        [
                        'full_name'       => ($person->F7 == $person->F8) ? empty($person->F7) ? 'ناقص ' . $person->F3 : $person->F7 : ((
                            (!empty($person->F7)) ? $person->F7 : '') . ' ' . ((!empty($person->F8)) ? $person->F8 : '')),
                        'status'          => 0,
                        'person_type'     => 0,
                        'scope'           => 1,
                        'identity_number' => $identity
                        ]
                    );
                }
                
                
            }
        );
    
        $this->info("identity==========================>");
        foreach($this->falseIdentity as $val){
            $this->info($val);
        }

        $this->info("duplicate==========================>");
        foreach($this->falseDuplicate as $val){
            $this->info($val);
        }
        
        // update full name
        DB::update('UPDATE sale_persons SET full_name = dbo.fixPersianLang(RTRIM(LTRIM(full_name)))');
    }
}
