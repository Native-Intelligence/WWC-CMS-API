<?php

namespace Infrastructure\Console\Commands;

use Closure;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\DB;
// use \nusoap_client;


class RequestConvertCommand extends Command
{

    protected $signature = 'convert:request';

    protected $description = 'Convert requests table.3';

    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    public function handle()
    {
        $i = 931000;
        DB::table('eshterak')->select('F3', 'F4', 'F40', 'F6')->distinct()->orderBy('F3')->chunk(
            100, function ($requests) use ($i) {
                foreach ($requests as $request) {
                    // $eshterak = DB::table('eshterak')->select()->where('F3', '=', $request->F5)->first();
                    // if($eshterak->F4) {
                    //     continue;
                    // }
                    $discounts = DB::table('eshterak4')->select('F1', 'F5', 'F6', 'F7', 'F8', 'F9')->where('F5', '=', $request->F3)->get();
                    $land = DB::table('sale_lands')->select('id', 'scope')->where('file_number', '=', $request->F3)->first();
                    $disArray = [];
                    if(!is_null($discounts)) {
                        foreach ($discounts as $discount) {
                            if($discount->F6 == 1) {
                                $disArray []= ['discount' => ['id'=>$discount->F8,
                                'title' => DB::table('ing_discounts')->select('title')->find($discount->F8)->title
                                ],
                                'national_code'=> $discount->F1,
                                'exemption_code'=> 111111
                                ];
                            }
                            if($discount->F7 == 1) {
                                $disArray []= ['discount' => ['id'=>$discount->F9,
                                'title' => DB::table('ing_discounts')->select('title')->find($discount->F9)->title
                                ],
                                'national_code'=> $discount->F1,
                                'exemption_code'=> 111111
                                ];
                            }
                            $disArray = [$disArray];
                        }
                        
                    }
                    if(empty($request->F6)) {
                        continue;
                    }
                    // dd($disArray);
                    DB::table('sale_requests')->insert(
                        ['file_number' => $request->F3,
                        'subscription_number' => $request->F4,
                        'request_number' => $i+=1,
                        'land_id' => $land->id,
                        'request_type' => 2,
                        'g_service_type_id' => 0,
                        'branch_type' => $request->F40,
                        'discount_id' => json_encode($disArray),
                        'person_id' => DB::table('sale_persons')->select('id')->where('identity_number', '=', $request->F6)->first()->id,
                        'status' => 0,
                        'scope' => $land->scope
                        ]
                    );
                }
            }
        );
        
    }
}
