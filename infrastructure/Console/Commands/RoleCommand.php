<?php

namespace Infrastructure\Console\Commands;

use Closure;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Api\Settings\Repositories\RoleRepository;
use Api\Settings\Repositories\UserRepository;

class RoleCommand extends Command
{

    private $_roleRepository;
    
    private $_userRepository;

    protected $name = 'role:update';

    protected $description = 'Update permissions in admin role.';

    protected $routes;

    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;

    public function __construct(RoleRepository $roleRepository, UserRepository $userRepository)
    {
        parent::__construct();

        // $this->router = $router;
        $this->routes = app()->router->getRoutes();
        $this->_roleRepository = $roleRepository;
        $this->_userRepository = $userRepository;
    }

    public function handle()
    {
        if (count($this->routes) == 0) {
            return $this->error("Your application doesn't have any routes.");
        }

        foreach ($this->routes as $route) {
            // dd($route);
            // $isPublic = (integer)(strpos($route['action']['as'], 'e@') === 0);
            
                
            
            switch ($route['method']) {
            case 'GET':
                $get[] = $route['uri'];
                // [
                //     //   'name' => $route['action']['as'],
                //     //   'is_public' => $isPublic,
                //     'uri' => $route['uri'],
                //     //   'uses' => $route['action']['uses']
                //      ];
                break;
            case 'PUT':
                $put[] = $route['uri'];
                // [
                //     //    'name' => $route['action']['as'],
                //     // 'is_public' => $isPublic,
                //
                //     'uri' => $route['uri'],
                //     //    'uses' => $route['action']['uses']
                //       ];
                break;
            case 'POST':
                $post[] = $route['uri'];
                //  [
                //     //    'name' => $route['action']['as'],
                //     // 'is_public' => $isPublic,
                //
                //     'uri' => $route['uri'],
                //     //    'uses' => $route['action']['uses']
                //       ];
                break;
            case 'DELETE':
                $delete[] = $route['uri'];
                // [
                //         //  'name' => $route['action']['as'],
                //         // 'is_public' => $isPublic,
                //
                //         'uri' => $route['uri'],
                //         //  'uses' => $route['action']['uses']
                //         ];
                break;
            }
        }
        
        $results = ['permissions' => [
                      'GET' => $get,
                      'PUT' => $put,
                      'POST' => $post,
                      'DELETE' => $delete
                     ]];

        if ($this->_roleRepository->update($results, 1)) {
             $this->info('Update admin role success!!!');
             $result = true;
        }
    
        
        $permissions = ['access->permissions' => [
            'GET' => $get,
            'PUT' => $put,
            'POST' => $post,
            'DELETE' => $delete
            ]
        ];

        if ($this->_userRepository->update($permissions, 1)) {
            $this->info('Update user admin access success!!!');
            $result = true;
        }
        
        // if ($this->_userRepository->update($permissions, 2)) {
        //     $this->info('Update user test access success!!!');
        //     $result = true;
        // }
        //
        // if ($this->_userRepository->update($permissions, 3)) {
        //     $this->info('Update user test2 access success!!!');
        //     $result = true;
        // }
        
        if ($this->_userRepository->update($permissions, 22)) {
            $this->info('Update user hasan access success!!!');
            $result = true;
        }
        if (is_null($result)) {
            return $this->error('Update admin role failed!!!');
        }
    }
}
