<?php

namespace Infrastructure\Console\Commands;

use Closure;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use \nusoap_client;


class SendSmsCommand extends Command
{

    protected $signature = 'sms:send  {text} {number*}';

    protected $description = 'Send SMS.';

    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    public function handle()
    {
        $input = $this->arguments();
        $client = new nusoap_client(env('SMS_API_HOST'), true);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;

        $result = $client->call(
            'Send',
            array(
                'username'=>env('SMS_USERNAME'),
                'password'=>env('SMS_PASSWORD'),
                'srcNumber'=>env('SMS_PROVIDER_NUMBER'),
                'body' =>$input['text'],
                'destNo' => $input['number'], //Array is Requird
                'flash' => env('SMS_FLASH')
            )
        );
        
        if(array_key_exists('Mobile', $result[0])) {
            return true;
        }
            return $this->error('Wrong! Error Code: '.$result[0]['ID']);
        
    }
}
