<?php

namespace Infrastructure\Console\Commands;

use Closure;
use Illuminate\Console\Command;
use Api\Sales\Models\Survey;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\DB;
// use \nusoap_client;


class SurveyConvertCommand extends Command
{

    protected $signature = 'convert:survey';

    protected $description = 'Convert requests table.4';

    protected $headers = ['Method', 'Uses', 'Insert'];
    
    protected $result;

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    public function handle()
    {
        DB::table('eshterak')->distinct('F3')->orderBy('F3')->chunk(
            100, function ($surveys) {
                foreach ($surveys as $survey) {
                    // $eshterak = DB::table('eshterak')->select()->where('F3', '=', $request->F5)->first();
                    // if($eshterak->F4) {
                    //     continue;
                    // }
                    // $discounts = DB::table('eshterak4')->select('F1', 'F5', 'F6', 'F7', 'F8', 'F9')->where('F5', '=', $request->F3)->get();
                    // $land = DB::table('sale_lands')->select('id', 'scope')->where('file_number', '=', $request->F3)->first();
                    // $disArray = [];
                    // if(!is_null($discounts)) {
                    //     foreach ($discounts as $discount) {
                    //         if($discount->F6 == 1) {
                    //             $disArray []= ['discount' => ['id'=>$discount->F8,
                    //             'title' => DB::table('ing_discounts')->select('title')->find($discount->F8)->title
                    //             ],
                    //             'national_code'=> $discount->F1,
                    //             'exemption_code'=> 111111
                    //             ];
                    //         }
                    //         if($discount->F7 == 1) {
                    //             $disArray []= ['discount' => ['id'=>$discount->F9,
                    //             'title' => DB::table('ing_discounts')->select('title')->find($discount->F9)->title
                    //             ],
                    //             'national_code'=> $discount->F1,
                    //             'exemption_code'=> 111111
                    //             ];
                    //         }
                    //         $disArray = [$disArray];
                    //     }
                    //
                    // }
                    if(empty($survey->F6)) {
                        continue;
                    }
                    // dd($disArray);
                    $request = DB::table('sale_requests')->select('id', 'request_number')->where('file_number', '=', $survey->F3)->first();
                    $discounts = DB::table('eshterak4')->select('F1', 'F5', 'F6', 'F7', 'F8', 'F9')->where('F5', '=', $survey->F3)->get();
                    $disArray=[];
                    if(!is_null($discounts)) {
                        foreach ($discounts as $discount) {
                            if($discount->F6 == 1) {
                                $disArray []= ['discount' => [
                                    'id'=>$discount->F8,
                                    'title' => DB::table('ing_discounts')->select('title')->find($discount->F8)->title
                                ],
                                'national_code'=> $discount->F1,
                                'exemption_code'=> 111111,
                                'count' => 1
                                ];
                            }
                            if($discount->F7 == 1) {
                                $disArray []= ['discount' => ['id'=>$discount->F9,
                                'title' => DB::table('ing_discounts')->select('title')->find($discount->F9)->title
                                ],
                                'national_code'=> $discount->F1,
                                'exemption_code'=> 111111,
                                'count' => 1
                                ];
                            }
                            $disArray = [$disArray];
                        }
                        
                    }
                    // dd($disArray);
                    // $idIngUsage = DB::table('map_ing_usages')->select('id_ing_usages')->where('id_usages_sys_old', '=', $survey->F16)->first()->id_ing_usages;
                    
                    $land = DB::table('sale_lands')->select('id', 'usage_id', 'scope', 'scope_city')->where('file_number', '=', $survey->F3)->first();
                    
                    $usage = DB::table('ing_usages')->select('code', 'id', 'title')->where('id', '=', $land->usage_id)->first();
                    
                    $coding = DB::table('ing_usages')->select('code', 'id', 'title')->where('code', '=', $survey->F18)->first();
                    dd("'".DB::table('map_no_nasb')->select('id_no_nasb')->where('id_no_nasb_sys_old', '=', $survey->F25)->first()->id_no_nasb."'");
                    Survey::create(
                        [
                            
                            'survey_date' => '2015-12-23 00:00:00',
                            'file_number' => $survey->F3,
                            'subscription_number' => $survey->F4,
                            'coordination_date' => '2015-12-23 00:00:00',
                            'request_id' => $request->id,
                            'request_number' => $request->request_number,
                            'user_id' => 1,
                            'instruction_id' => 1,
                            'usage_id' => $land->usage_id,
                            // 'survey_detail',
                            'number_of_units' =>
                                [[
                                'residential_units_wa' => $survey->F19,
                                'residential_units_wa_subsidiary' => $survey->F20,
                                'residential_units_wa_consumption' => $survey->F21,
                                'residential_units_sw' => $survey->F31,
                                'residential_units_sw_subsidiary' => $survey->F32,
                                'residential_units_sw_consumption' => $survey->F33,
                                'residential_surface_units_sw' => 0,
                                'residential_surface_units_sw_consumption' => 0,
                                'discounts' => $disArray,
                                // [[
                                //     'discount'=> ['id'=>'','title'=>''],
                                //     'national_code'=>'',
                                //     'exemption_code'=>'',
                                //     'count'=>''
                                // ]],
                                
                                'usage' =>
                                    ['id'=>$usage->id,'code'=>$usage->code,'title'=>$usage->title]
                                ,
                                'coding' =>
                                    ['id' => $coding->id, 'title' => $coding->title, 'code'=>$coding->code]
                                ,
                                'usage_pattern' =>
                                    ['id'=>'2', 'title'=>'111', 'value'=>2222]
                                ,
                                'usage_pattern_value' => 2222
                                ]]
                            ,
                            'is_split' => 1,
                            'g_siphon_id' => ((empty($survey->F34))? null: DB::table('set_gdata_details_view')->select('id')->where('gdata_slug', '=', 'siphon')->where('value', '=', $survey->F34)->first()->id),
                            'g_branch_diameter_id' => DB::table('set_gdata_details_view')->select('id')->where('gdata_slug', '=', 'branch_diameter')->where('value', '=', $survey->F23)->first()->id,
                            'g_type_installation_id' =>
                            DB::table('set_gdata_details_view')->select('id')->where('gdata_slug', '=', 'type_installation')->where(
                                'value', '=', "'".DB::table('map_no_nasb')->select('id_no_nasb')->where('id_no_nasb_sys_old', '=', $survey->F25)->first()->id_no_nasb."'"
                            )->first()->id,
                            
                            'consumption_instruction' => $survey->F21,
                            'g_service_type_id' => 0,
                            'scope_city' => $land->scope_city,
                            'scope' => $land->scope,
                            'status' => 0,
                                
                            // 'file_number' => $request->F3,
                            // 'subscription_number' => $request->F4,
                            // 'request_number' => $i+=1,
                            // 'land_id' => $land->id,
                            // 'request_type' => 2,
                            // 'g_service_type_id' => 0,
                            // 'branch_type' => $survey->F40,
                            'discount_id' => $disArray,
                            // 'person_id' => DB::table('sale_persons')->select('id')->where('identity_number', '=', $survey->F6)->first()->id,
                            // 'status' => 0,
                            // 'scope' => $land->scope
                        ]
                    );
                }
            }
        );
        
    }
}
