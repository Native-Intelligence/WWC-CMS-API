<?php

namespace Infrastructure\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'Infrastructure\Console\Commands\RoleCommand',
        'Infrastructure\Console\Commands\GdataCommand',
        'Infrastructure\Console\Commands\SendSmsCommand',
        'Infrastructure\Console\Commands\PersonConvertCommand',
        'Infrastructure\Console\Commands\LandConvertCommand',
        'Infrastructure\Console\Commands\RequestConvertCommand',
        'Infrastructure\Console\Commands\SurveyConvertCommand',
        'Infrastructure\Console\Commands\CalcConvertCommand',
        'Infrastructure\Console\Commands\FileConvertCommand',
        'Infrastructure\Console\Commands\InstallmentConvertCommand',
        '\Mlntn\Console\Commands\Serve'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
