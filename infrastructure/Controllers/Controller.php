<?php

namespace Infrastructure\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{

    protected $service;
    
    protected $request;
    
    protected $defaults = [];

    public function getAll()
    {
        return $this->response(
            app()->helper::jGenerate(
                $this->service->getAll()
            ),
            201
        );
    }

    public function getByParams()
    {
        $this->request->readValidation();
        
        return $this->response(
            app()->helper::jGenerate(
                $this->service->getByParams($this->request)
            ),
            201
        );
    }

    public function create()
    {
        $this->request->createValidation();

        return $this->response(
            app()->helper::jGenerate(
                $this->service->create($this->request)
            ),
            201
        );
    }

    public function update()
    {
        $this->request->updateValidation();
        
        return $this->response(
            app()->helper::jGenerate(
                $this->service->update($this->request)
            ),
            201
        );
    }

    public function destroy()
    {
        $this->request->deleteValidation();
        
        return $this->response(
            app()->helper::jGenerate(
                $this->service->destroy($this->request)
            ),
            201
        );
    }
    
    /**
     * Create a json response
     *
     * @param  mixed   $data
     * @param  integer $statusCode
     * @param  array   $headers
     * @return Illuminate\Http\JsonResponse
     */
    protected function response($data, $statusCode = 200, array $headers = [], $jsonNumericCheck = 0/* JSON_NUMERIC_CHECK*/)
    {
        if ($data instanceof Arrayable && !$data instanceof JsonSerializable) {
            $data = $data->toArray();
        }

        return new JsonResponse($data, $statusCode, $headers, $jsonNumericCheck);
    }

    /**
     * Parse data using larchitect
     *
     * @param  mixed $data
     * @param  array  $options
     * @param  string $key
     * @return mixed
     */
    // protected function parseData($data, array $options, $key = null)
    // {
    //     $larchitect = new Architect;
    //
    //     return $larchitect->parseData($data, $options['modes'], $key);
    // }
    //
    // protected function parseSort(array $sort)
    // {
    //     return array_map(function ($sort) {
    //         if (!isset($sort['direction'])) {
    //             $sort['direction'] = 'asc';
    //         }
    //
    //         return $sort;
    //     }, $sort);
    // }

    /**
     * Parse include strings into resource and modes
     *
     * @param  array  $includes
     * @return array The parsed resources and their respective modes
     */
    // protected function parseIncludes(array $includes)
    // {
    //     $return = [
    //         'includes' => [],
    //         'modes' => []
    //     ];
    //
    //     foreach ($includes as $include) {
    //         $explode = explode(':', $include);
    //
    //         if (!isset($explode[1])) {
    //             $explode[1] = $this->defaults['mode'];
    //         }
    //
    //         $return['includes'][] = $explode[0];
    //         $return['modes'][$explode[0]] = $explode[1];
    //     }
    //
    //     return $return;
    // }

    /**
     * Parse filter group strings into filters
     * Filters are formatted as key:operator(value)
     * Example: name:eq(esben)
     *
     * @param  array  $filters
     * @return array
     */
    // protected function parseFilterGroups(array $filter_groups)
    // {
    //     $return = [];
    //
    //     foreach ($filter_groups as $group) {
    //         if (!array_key_exists('filters', $group)) {
    //             throw new InvalidArgumentException('Filter group does not have the \'filters\' key.');
    //         }
    //
    //         $filters = array_map(function ($filter) {
    //             if (!isset($filter['not'])) {
    //                 $filter['not'] = false;
    //             }
    //
    //             return $filter;
    //         }, $group['filters']);
    //
    //         $return[] = [
    //             'filters' => $filters,
    //             'or' => isset($group['or']) ? $group['or'] : false
    //         ];
    //     }
    //
    //     return $return;
    // }

    /**
     * Parse GET parameters into resource options
     *
     * @return array
     */
    // protected function parseResourceOptions()
    // {
    //     $request = app()->make(Router::class)->getCurrentRequest();
    //
    //     $this->defaults = array_merge([
    //         'includes' => [],
    //         'sort' => [],
    //         'limit' => null,
    //         'page' => null,
    //         'mode' => 'embed',
    //         'filter_groups' => []
    //     ], $this->defaults);
    //
    //     $includes = $this->parseIncludes($request->get('includes', $this->defaults['includes']));
    //     $sort = $this->parseSort($request->get('sort', $this->defaults['sort']));
    //     $limit = $request->get('limit', $this->defaults['limit']);
    //     $page = $request->get('page', $this->defaults['page']);
    //     $filter_groups = $this->parseFilterGroups($request->get('filter_groups', $this->defaults['filter_groups']));
    //
    //     if ($page !== null && $limit === null) {
    //         throw new InvalidArgumentException('Cannot use page option without limit option');
    //     }
    //
    //     return [
    //         'includes' => $includes['includes'],
    //         'modes' => $includes['modes'],
    //         'sort' => $sort,
    //         'limit' => $limit,
    //         'page' => $page,
    //         'filter_groups' => $filter_groups
    //     ];
    // }
}
