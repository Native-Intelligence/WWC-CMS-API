<?php

namespace Infrastructure\Database\Eloquent;

class LogModel extends Model
{

    protected $table = 'log_model';

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
