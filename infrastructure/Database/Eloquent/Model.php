<?php

namespace Infrastructure\Database\Eloquent;

use Illuminate\Database\Eloquent\Model as BaseModel;

abstract class Model extends BaseModel
{

    protected $view;

    public function getView()
    {
        return $this->view;
    }
    
    public function getScopeAttribute($value)
    {
        return (int) $value;
    }

    public function getScopeCityAttribute($value)
    {
        return (int) $value;
    }

    public function getScopeMaxAttribute($value)
    {
        return (int) $value;

    }
    public function getDateFormat()
    {
        return 'Y-m-d H:i:s.u';
    }

    public function fromDateTime($value)
    {
        return substr(parent::fromDateTime($value), 0, -3);
    }

}
