<?php

namespace Infrastructure\Database\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Infrastructure\Database\Criterias\AuthCriteria;
use Prettus\Repository\Events\RepositoryEntityUpdated;

abstract class Repository extends BaseRepository
{
    public function fixData($data)
    {
        return array_map(
            function ($value) {
                return is_string($value) ? trim($value) : $value;
            },
            $data
        );
    }
    
    public function set($data, array $attributes, array $path)
    {
        if (!empty($path)) {
            if (empty($path[1])) {
                $data->{$path[0]} = $attributes;
                return $data;
            } else {
                $nextPath = $path;
                array_shift($nextPath);
                return $data->{$path[0]} = $this->set($data->{$path[0]}, $attributes, $nextPath);
            }
        }
    }
    
    public function create(array $attributes)
    {
        // fix data
        $attributes = $this->fixData($attributes);
        
        return  parent::create($attributes);
    }
    
    public function update(array $attributes, $id)
    {
        // fix data
        $attributes = $this->fixData($attributes);
        
        $this->applyScope();
        
        if (!is_null($this->validator)) {
            $this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }
        
        $_skipPresenter = $this->skipPresenter;
        
        $this->skipPresenter(true);
        
        $model = $this->model->findOrFail($id);
        
        foreach ($attributes as $k => $v) {
            // dd($model->{"access->permissions"});
            $path = explode('->', $k);
            if (!empty($path[1])) {
                // dd($path);
                // $temp = $model->{$path[0]};
                // dd(gettype($temp));
                // array_shift($path);
                // foreach ($path as $val) {
                //     $temp1[$val] = $temp->{$val};
                // }
                // dd($this->set($model, $attributes[$k], $path));
                $attributes[$path[0]] = $this->set($model, $attributes[$k], $path);
            }
            // $field = $this->wrapValue(array_shift($path));
            //
            // $accessor = '"$.'.implode('.', $path).'"';
            
            // return "{$field} = json_set({$field}, {$accessor}, {$value->getValue()})";
        }
        // $model = $this->model->where('id', $id);
        // dd($attributes);
        // $model->update(['access->permissions' => json_encode(['aaa', 'bbb'])]);
        // $model->update($attributes);
        $model->fill($attributes);
        // $model->fill($jsonAttributes);
        // dd($model->access->permissions= ["aaa"=>[1,2,3]]);
        $model->save();
        
        $this->skipPresenter($_skipPresenter);
        $this->resetModel();
        
        // $model = $this->model->findOrFail($id);
        
        event(new RepositoryEntityUpdated($this, $model));
        
        return $this->parserResult($model);
    }
    
    public function count()
    {
        return $this->model->count();
    }
    
    public function getTabale()
    {
        $model = $this->makeModel();
        return $model->getTable();
    }
    
    public function getQueryField($query = null)
    {
        return Schema::getColumnListing($this->getTabale());
    }
    
    public function makeModel()
    {
        parent::makeModel();
        $this->boot();
        return $this->model;
    }
    
    public function boot()
    {
        if (!empty($this->model->getView())) {
            $this->model->setTable($this->model->getView());
        }
    }
}
