<?php
namespace Infrastructure\Exceptions;

use Exception as BaseException;

class Exception extends BaseException
{

    private $_error;

    private $_messages;

    public function __construct(int $error, array $messages = [])
    {
        $this->_error = $error;
        $this->_messages = $messages;

        if (empty($this->_messages)) {
            // dd($messages);
            // $lowerRule = Str::snake($rule);
            $key = 'exception.'.$error;
            if ($key != ($value = trans($key))) {
                // $message = str_replace(array_keys($messages['error']), $messages['error'], $value);
                $this->_messages = $value;
            }
        }
        if (array_key_exists('error', $this->_messages)) {
            // dd($messages);
            // $lowerRule = Str::snake($rule);
            $key = 'exception.'.$error;
            if ($key != ($value = trans($key))) {
                $message = str_replace(array_keys($messages['error']), $messages['error'], $value);
                $this->_messages = $message;
            }
        }
    }

    public function getError()
    {
        return $this->_error;
    }
    
    public function getUser()
    {
        if (!is_null(app()->userHelper::$currentToken)) {
            return app()->userHelper::$currentUser;
            app()->userHelper::$currentToken = null;
        }

        return null;
    }
    
    public function getMessages()
    {
        return $this->_messages;
    }
}
