<?php
namespace Infrastructure\Exceptions;

use Exception as BaseException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Infrastructure\Exceptions\Exception;

class Handler extends ExceptionHandler
{

    protected $dontReport = [
                             AuthorizationException::class,
                             HttpException::class,
                             ModelNotFoundException::class,
                             ValidationException::class,
                            ];

    public function report(BaseException $e)
    {
        parent::report($e);
    }

    public function render($request, BaseException $e)
    {
        if ($e instanceof Exception && $e->getError()) {
            return response()->json(
                [
                 'status' => false,
                 'error' => $e->getError(),
                 'messages' => $e->getMessages(),
                 'user' => $e->getUser()
                ],
                substr($e->getError(), 0, 3)
            );
        }

        $rendered = parent::render($request, $e);

            return response()->json(
                [
                 'status' => false,
                 'error' => $rendered->getStatusCode(),
                 'messages' => ['default' => $e->getMessage()]
                ],
                500
            );
    }
}
