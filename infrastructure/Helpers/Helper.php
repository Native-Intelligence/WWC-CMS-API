<?php
namespace Infrastructure\Helpers;

class Helper
{

    public static $statusDestroy = ['status' => 9];
    
    public static $blockStatus = ['inactive' => 0, 'destroy' => 9];
    
    public static $autoIncrement = 10;
    
    public static $plusId = 1;
    
    public static $scopeDefault = 1;

    public static function jGenerate($result, bool $status = true)
    {
        $user = null;
        
        if (!is_null(app()->userHelper::$currentToken)) {
            $user = app()->userHelper::$currentToken;
            app()->userHelper::$currentToken = null;
        }
        
        return [
                'status' => $status,
                'result' => $result,
                'user' => $user
                ];
    }
    
    public static function fixPersianChars(string $str)
    {
        return str_replace(['ي', 'ك', '\u0643\u200C', '\u064A'], ['ی', 'ک', '\u06A9', '\u06CC'], $str);
    }
}
