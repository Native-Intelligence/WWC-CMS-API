<?php
namespace Infrastructure\Helpers;

use Illuminate\Support\Facades\Hash;

class UserHelper
{

    public static $currentUser;
    
    public static $currentToken;
    
    public static function createUniqueToken(string $prefix)
    {
        return [
                'access_token'       => Hash::make(uniqid($prefix)),
                'refresh_token'      => Hash::make(uniqid($prefix)),
                'access_expires_at'  => new \DateTime('+1 hours'),
                'refresh_expires_at' => new \DateTime('+1 day'),
               ];
    }
}
