<?php

namespace Infrastructure\Middleware;

use Closure;
use DateTime;
use Illuminate\Contracts\Auth\Factory as Auth;
use Infrastructure\Exceptions\Exception;
use Api\Settings\Repositories\TokenRepository;
use Illuminate\Support\Facades\Hash;

class Authenticate
{

    protected $auth;
    
    private $_tokenRepository;

    public function __construct(Auth $auth, TokenRepository $tokenRepository)
    {
        $this->auth = $auth;
        $this->_tokenRepository = $tokenRepository;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $accessToken = $request->header('access-token');
        $refreshToken = $request->header('refresh-token');
        $appName = $request->header('app-name');
        $appSecret = $request->header('app-secret');
        if ($accessToken != 'navid') {
            if (is_null($accessToken)) {
                throw new Exception(40125);
            }

            $token = $this->_tokenRepository->with(['user', 'client'])->findByField('access_token', $accessToken)->first();
        
            if (is_null($token)) {
                throw new Exception(40125);
            }

            if ($refreshToken != $token->refresh_token) {
                throw new Exception(40125);
            }

            $client = $token->client->first();
            if (is_null($client)
                or $client->app_name != $appName
                or !Hash::check($appSecret, $client->app_secret)
            ) {
                throw new Exception(40122);
            }

            $now = new DateTime();
            $accessExpiresAt = new DateTime($token->access_expires_at);
            $refreshExpiresAt = new DateTime($token->refresh_expires_at);

            if ($accessExpiresAt > $now) {
                $user = $token->user()->with(['tokens'])->first();
        
                if (substr($request->route()[1]['as'], 0, 2) !== 'e@' && !in_array(
                    '/'.$request->path(),
                    // array_column(
                        $user->access->permissions->{$request->method()}
                    // 'uri'
                    // )
                )
                ) {
                        throw new Exception(40324);
                }
            
                // $user->tokens = $token;
                // return $token->user()->with(['tokens'])->first();
                app()->userHelper::$currentUser = $user;
            } else if ($refreshExpiresAt > $now) {
                $newToken = $token->update(app()->userHelper::CreateUniqueToken($token->user_id));

                // $user = $this->_tokenRepository->getUserByToken($newToken);
                $user = $token->user()->with(['tokens'])->first();
                
                // $user->token = $newToken;
                // dd($token->user_id);

                if (!in_array(
                    '/'.$request->path(),
                    // array_column(
                        $user->access->permissions->{$request->method()}
                    // 'uses'
                    // )
                )
                ) {
                    throw new Exception(40324);
                }
            
                // $user->tokens = $newToken;
                // return $user;
                app()->userHelper::$currentToken = ['tokens' => [$token]];
                app()->userHelper::$currentUser = $user;
            } else {
                throw new Exception(40125);
            }
        }

        return $next($request);
    }
}
