<?php
namespace Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Infrastructure\Exceptions\Exception;

class ApiServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('helper', function () {
            return new \Infrastructure\Helpers\Helper;
        });
        
        $this->app->bind('userHelper', function () {
            return new \Infrastructure\Helpers\UserHelper;
        });
        
        // if(empty(app()->userHelper::$currentUser))
        // {
        //     throw new Exception(40121);
        // }
    
        Validator::extend('scope', function ($field, $value, $parameters) {
            if (!is_array($value)) {
                return false;
            }
            foreach ($value as $k => $v) {
                if (!is_array($v)) {
                    $v = [$v];
                }
                
                $v = array_diff($v, array(0,1));

                if (!empty($v)) {
                    if (!empty(
                        array_diff(
                            (array) app()->userHelper::$currentUser->access->scope->$k,
                            array(0, 1)
                            // array(1 => 'null', 0 => 'not null!')
                        )
                    )
                    ) {
                        $diff  = array_diff(
                            $v,
                            // array_keys((array) app()->userHelper::$currentUser->access->scope->$k)
                            (array) app()->userHelper::$currentUser->access->scope->$k
                        );

                        if (!empty($diff)) {
                            // dd($diff);
                            return false;
                        }
                    }
                } else if (!empty(
                    array_diff(
                        (array) app()->userHelper::$currentUser->access->scope->$k,
                        array(0, 1)
                        // array(1 => 'null', 0 => 'not null!')
                    )
                )
                ) {
                    return false;
                }
            }

            return true;
        });
    }
}
