<?php

namespace Infrastructure\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Api\Sales\Events\CartableChangeEvent' => [
            'Api\Sales\Listeners\CartableChangeListener',
        ],
        'Api\Sales\Events\SurveyCreatedEvent' => [
            'Api\Sales\Listeners\SurveyCreatedListener',
        ],
        'Api\Settings\Events\UserCreatedEvent' => [
            'Api\Settings\Listeners\UserCreatedListener',
        ],
        'Api\Ass\Events\SurveyCreatedEvent' => [
            'Api\Ass\Listeners\SurveyCreatedListener',
        ],
        'Api\Acc\Events\PaymentCreatedEvent' => [
            'Api\Acc\Listeners\PaymentCreatedListener',
        ],
        'Api\Acc\Events\FtpCreatedEvent' => [
            'Api\Acc\Listeners\FtpCreatedListener',
        ],
        'Api\Acc\Events\TransactionCreatedEvent' => [
            'Api\Acc\Listeners\TransactionCreatedListener',
        ],
    ];

    /**
    * Register any other events for your application.
    *
    * @return void
    */
    // public function boot()
    // {
    //     parent::boot();
    //
    //     Event::listen('event.name', function ($foo, $bar) {
    //         //
    //     });
    // }
}
