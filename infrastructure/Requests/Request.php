<?php

namespace Infrastructure\Requests;

use Infrastructure\Helpers\Helper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Validator;
use Illuminate\Http\Request as BaseRequest;
use Infrastructure\Exceptions\Exception;

class Request extends BaseRequest
{
    
    protected $data;
    
    protected $rules;
    
    protected $key;
    
    public function json($key = null, $default = null)
    {
        if (!isset($this->json)) {
            $this->json = new ParameterBag(
                (array)json_decode(Helper::fixPersianChars($this->getContent()), true)
            );
        }
        
        if (is_null($key)) {
            return $this->json;
        }
        
        return data_get($this->json->all(), $key, $default);
    }
    
    public function createValidation()
    {
        throw new Exception(40030);
    }
    
    public function readValidation()
    {
        $this->rules = [
            'id'        => 'integer',
            'condition' => 'array',
            'scope'     => 'integer',
            'sort'      => 'array',
            'columns'   => 'array'
        ];
        $this->validate($this->data, $this->rules);
    }
    
    public function updateValidation()
    {
        throw new Exception(40030);
    }
    
    public function deleteValidation()
    {
        $this->rules = ['id' => 'required|integer'];
        $this->validate($this->data, $this->rules);
    }
    
    protected function validate($data, $rules)
    {
        if (is_null($data) or is_null($rules)) {
            throw new Exception(40031);
        }
        
        $diff = array_diff_key($data, $rules);
        
        if (!empty($diff)) {
            throw new Exception(40030);
        }
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->fails()) {
            throw new Exception(40030, $validator->errors()->messages());
        }
    }
}
