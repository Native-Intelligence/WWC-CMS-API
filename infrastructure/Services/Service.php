<?php

namespace Infrastructure\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Infrastructure\Exceptions\Exception;
use Infrastructure\Database\Eloquent\LogModel;

class Service
{
    
    protected $data;
    
    protected $repository;
    
    protected $exist;
    
    protected $key;
    
    protected $userCheck = false;
    
    protected $uniqueKey;
    
    protected $perPage;
    
    protected $event = [
        'created' => ['Api\Sales\Events\CartableChangeEvent'],
        // 'updated' => ['Api\Sales\Events\CartableChangeEvent'],
        // 'deleted' => ['Api\Sales\Events\CartableChangeEvent']
    ];
    
    public function getData($data)
    {
        switch (gettype($data)) {
            case 'object':
                return $this->data = (object)$data->json($this->key);
                break;
            case 'array':
                return $this->data = (object)$data;
                break;
            case 'integer':
                return $this->data = (object)['id' => $data];
                break;
        }
    }
    
    public function archiveEvent($event, $model)
    {
        $log = new LogModel;
        $log->event = $event;
        $log->new_values = (is_callable([$model, 'toJson'])) ? $model->toJson() : json_encode($model);
        $log->user_id = app()->userHelper::$currentUser->id;
        $log->table = (is_callable([$model, 'getTable'])) ? $model->getTable() : 'action';
        
        if ($event == 'update') {
            $log->old_values = json_encode($model->getOriginal());
        }
        $log->save();
    }
    
    public function getAll()
    {
        $this->perPage = (Input::get('limit') ? Input::get('limit') : '10');
        
        return $this->getAllWithoutPaginate()->paginate($this->perPage);
    }
    
    public function scopeCheckQuery($query)
    {
        $query = $query->where('status', '!=', '9')->where(
            function ($query) {
                $query->orWhere(['scope' => app()->helper::$scopeDefault]);
                foreach ((array)app()->userHelper::$currentUser->access->scope as $v) {
                    $query->orWhereBetween('scope', $v);
                    $query->orWhereBetween('scope', [1, 1]);
                }
            }
        );
        
        if ($this->userCheck) {
            $query = $query->where('user_id', '=', app()->userHelper::$currentUser->id);
        }
        
        return $query;
    }
    
    public function getAllWithoutPaginate()
    {
        // dd(\DB::table('ing_areas')->find(11));
        return $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        );
    }
    
    // public function getGroupByScope()
    // {
    //     return $this->repository->scopeQuery(
    //         function ($query) {
    //             $q = $query->where('status', '!=', '9')->Where(
    //                 function ($query) {
    //                     foreach ($this->data->scope as $v) {
    //                         $query->orWhereBetween('scope', $v);
    //                     }
    //                 }
    //             );
    //             return $q;
    //         }
    //     )->all();
    // }
    
    public function find($id)
    {
        return $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->find($id);
    }
    
    public function getById($id)
    {
        return $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->find($id);
    }
    
    public function getByParams($data)
    {
        $this->data = (object)$data->json($this->key);
        // list($condition, $sort, $val) = (array)$this->data;
        
        // dd($condition);
        (isset($this->data->condition)) ? $condition = $this->data->condition : $condition = [];
        (isset($this->data->sort)) ? $sort = $this->data->sort : $sort = [];
        
        if (isset($this->data->condition) or isset($this->data->sort)) {
            if (isset($this->data->columns)) {
                $columns = $this->data->columns['only'];
            } else {
                $columns = ['*'];
            }
            
            return $this->getAllByField($condition, $sort, $columns);
        } elseif (isset($this->data->id)) {
            return $this->getById($this->data->id);
        }
        //  else if (isset($this->data->scope)) {
        //     return $this->getGroupByScope();
        // }
    }
    
    public function getFirstItem($direction = 'asc')
    {
        return $this->repository->scopeQuery(
            function ($query) use ($direction) {
                $query = $this->scopeCheckQuery($query);
                
                return $query->orderBy('created_at', $direction);
            }
        )->first();
    }
    
    public function getLastItem()
    {
        return $this->getFirstItem('desc');
    }
    
    public function getByField(array $condition)
    {
        return $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->findWhere($condition)->first();
    }
    
    public function getByFieldWithRelations(array $condition, array $relation)
    {
        return $this->repository->with($relation)->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->findWhere($condition)->first();
    }
    
    public function filter(array $condition, $query, string $method = 'where')
    {
        $query->{$method}(function ($query) use ($condition) {
            $is_and = 'where';
            foreach ($condition as $value) {
                if (is_array($value)) {
                    if (is_array($value[0])) {
                        $query = $this->filter($value, $query, $is_and);
                        $is_and = 'where';
                    } else {
                        list($field, $operation, $val) = $value;
                        
                        // for sqlsrv json
                        if (DB::getDriverName() === 'sqlsrv' && stripos($field, '->') !== false) {
                            $jsonColumns = explode('->', $field);
                            $query->whereRaw(
                                "JSON_VALUE(" . $jsonColumns[0] . ", '$." . $jsonColumns[1] . "') "
                                . $operation . ' '
                                . (is_string($val) ? "'" . $val . "'" : (is_array($val) ? "(" . implode(',', $val) . ")" : $val))
                            );
                        } else {
                            $query->{$is_and}(function ($query) use ($field, $val, $operation) {
                                switch ($operation) {
                                    case 'IN':
                                        $query->whereIn($field, $val);
                                        break;
                                    case 'NOT IN':
                                        $query->whereNotIn($field, $val);
                                        break;
                                    case 'BETWEEN':
                                        $query->whereBetween($field, $val);
                                        break;
                                    case 'NOT BETWEEN':
                                        $query->whereNotBetween($field, $val);
                                        break;
                                    case 'IS NULL':
                                        $query->whereNull($field);
                                        break;
                                    case 'IS NOT NULL':
                                        $query->whereNotNull($field);
                                        break;
                                    default:
                                        $query->where($field, $operation, $val);
                                        break;
                                }
                            });
                        }
                    }
                } else {
                    $is_and = ($value === 'and') ? 'where' : 'orWhere';
                }
            }
        });
        // dd($query->toSql());
        return $query;
    }
    
    public function getAllByField(array $condition = [], array $sort = [], array $columns = ['*'])
    {
        // $condition = (array) $data->condition;
        $this->perPage = (Input::get('limit') ? Input::get('limit') : '10');
        
        return $this->repository->scopeQuery(
            function ($query) use (&$condition, $sort) {
                // filter
                $query = $this->filter(
                    $condition,
                    $this->scopeCheckQuery($query)
                );
                
                // sort
                foreach ($sort as $column => $direction) {
                    $query->orderBy($column, $direction);
                }
                
                return $query;
            }
        )->paginate($this->perPage, $columns);
    }
    
    public function checkUniqueKeyExist($data)
    {
        $this->data = (object)$this->getData($data);
        // var_dump($this->data);
        if (!empty($this->uniqueKey)) {
            
            $uniqueKeys = explode('&', $this->uniqueKey);
            foreach ($uniqueKeys as $value) {
                $keys[$value] = $this->data->{$value};
            }
            $keys[] = ['status', '!=', '9'];
            $this->exist = $this->repository->findWhere($keys)->first();
        }
        if (!is_null($this->exist)) {
            throw new Exception(40640);
        }
    }
    
    public function create($data)
    {
        $this->checkUniqueKeyExist($data);
        
        $result = $this->repository->create((array)$this->data);
        
        $this->archiveEvent('create', $result);
        
        if (isset($this->event['created'])) {
            foreach ($this->event['created'] as $val) {
                event(new $val($result));
            }
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $this->data = (object)$this->getData($data);
        
        $result = $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->update((array)$this->data, $this->data->id);
        
        $this->archiveEvent('update', $result);
        
        if (isset($this->event['updated'])) {
            foreach ($this->event['updated'] as $val) {
                event(new $val($result));
            }
        }
        
        return $result;
    }
    
    public function destroy($data)
    {
        // $this->data = (object) $data->json($this->key);
        $this->data = (object)$this->getData($data);
        // dd($this->data);
        // dd($this->data, "asassa");
        $result = $this->repository->scopeQuery(
            function ($query) {
                return $this->scopeCheckQuery($query);
            }
        )->update(app()->helper::$statusDestroy, $this->data->id);
        
        $this->archiveEvent('soft_delete', $result);
        
        if (isset($this->event['deleted'])) {
            foreach ($this->event['deleted'] as $val) {
                event(new $val($result));
            }
        }
        
        return $result;
    }
}
