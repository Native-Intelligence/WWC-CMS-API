<?php

return [
    '40620' => ['default' => 'نام کاربری تکراری است.'],
    '40121' => ['default' => 'نام کاربری یا رمز عبور اشتباه است.'],
    '40122' => ['default' => 'شما از کلاینت غیر معتبری کانکت شده اید.'],
    '40123' => ['default' => 'کد امنیتی اشتباه است .'],

    '40323' => ['default' => 'دسترسی شما محدود شده است لطفا با ما تماس بگیرید.'],
    '40324' => ['default' => 'دسترسی محدود است و شما به این بخش دسترسی ندارید.'],
    '40125' => ['default' => 'کلید ورود شما نامعتبر است.'],
    '40030' => ['default' => 'اطلاعات ارسالی از سوی شما نامعتبر است.'],
    '40031' => ['default' => 'اطلاعات ثبت شده توسط شما به سیستم ارسال نشد.'],
    '40640' => ['default' => 'پیش تر ثبت شده است.'],
    
    '40040' => ['default' => 'دستورالعملی برای این درخواست وجود ندارد.'],
    '40041' => ['default' => 'تعرفه ای برای این درخواست وجود ندارد.'],
    
    '50010' => ['default' => '(:formula) این فرمول را نمیتوان پردازش کرد لطفا اطلاعات نرخ پایه در دستورالعمل را بررسی کنید. [:vars]'],
];
// return [
//     '40620' => ['default' => 'Username has been taken.'],
//     '40121' => ['default' => 'Could not authenticate you.'],
//     '40122' => ['default' => 'Could not authenticate your client.'],
//     '40323' => ['default' => 'Your account is block contact us.'],
//     '40324' => ['default' => 'Permission denied.'],
//     '40125' => ['default' => 'Token not valid.'],
//     '40030' => ['default' => 'Bad input.'],
//     '40031' => ['default' => 'Input is null'],
//     '40640' => ['default' => 'Has been taken.'],
//
//     '40040' => ['default' => 'Instrucion is null.'],
//     '40041' => ['default' => 'Invoice is null.'],
//
//     '50010' => ['default' => 'This formula (:formula) can\'t be parse please check baseRate value.'],
// ];
